

#####____________________interpret.py____________________#####
#####____________________IPP PROJECT_____________________#####
#####_____________KAROLINA_RADACOVSKA_xradac00___________#####



import argparse
import re
import xml.etree.ElementTree as ET
import sys

from classes import ArgTypeCountCheck, Holder, Instruction, Handle

#________________PARSING_CMD_ARGUMENTS_________________
def parse_cmd():
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument("--source", nargs=1, help="Vstupní soubor s XML reprezentací zdrojového kódu")
    argument_parser.add_argument("--input", nargs=1, help="Soubor se vstupy pro samotnou interpretaci zadaného zdrojového kódu")
    arguments = vars(argument_parser.parse_args())

    if arguments['source']:
        source_file = arguments['source'][0]
    else:
        source_file = input()

    if arguments['input']:
        input_file = arguments['input'][0]
    else:
        input_file = None

    if sys.argv[1] == '--help':
        argument_parser.print_help()

    return source_file, input_file

#________________LOADING_FILES________________
def load_source_file(file):
    try: 
        tree = ET.parse(file)
    except: 
        exit(31)  # could not load xml file
    return tree

def load_input_file(file_name):
    with open(file_name, "r") as fn:
        x = fn.read()
        try:
            x = x.split('\n')
        except: 
            pass
        if len(x[-1]) == 0: # avoiding one empty string at the end of an array 
            x = x[:-1]
        return x
    

#________________SORTING_&_CHECKING_DUPLICITIES________________
def root_sort(root):
    # sorting by instruction order
    root[:] = sorted(root, key=lambda child: (int(child.get('order'))))
    for instr in root:
        # sorting by argument order
        instr[:] = sorted(instr, key=lambda child: (child.tag))
    return root

def duplicities_check(root):
    previous = 0    # order will never start with 0
    for instr in root:
        order = instr.attrib['order']
        if previous == order:   # checking order duplicities
            exit(32)
        previous = order
        arg_duplicities = set()
        for arg in instr: 
            if arg.tag not in arg_duplicities: 
                arg_duplicities.add(arg.tag)
        if len(arg_duplicities) != len(instr):
            exit(32)

#________________XML_REPRESENTATION_CHECK________________
def xml_check(tree):
    root = tree.getroot()
    # root.tag  -> program
    # root.atrb -> language : IPPcode22
    if root.tag != 'program':
        exit(32)
    if not re.match("^IPPcode22$", root.attrib['language'], re.IGNORECASE):
        exit(32)

    for instruction in root:
        # instruction.atrb  -> order:[order], opcode:[name]
        if instruction.tag != 'instruction':
            exit(32)
        if 'order' not in instruction.attrib:
            exit(32)
        if 'opcode' not in instruction.attrib:
            exit(32)

        order_of_instruction = instruction.attrib['order']
        if re.match("^(?!0+$)[0-9]*$", order_of_instruction) == None: exit(32)

        # checknig missing arguments
        was_arg_1 = False
        was_arg_2 = False
        was_arg_3 = False
        for arg in instruction:
            if re.match("arg1|arg2|arg3", arg.tag) == None:
                exit(32)
            if arg.tag == 'arg1':
                was_arg_1 = True
            if arg.tag == 'arg2':
                was_arg_2 = True
            if arg.tag == 'arg3':
                was_arg_3 = True
            if not 'type' in arg.attrib:
                exit(32)
        if was_arg_2:        
            if not was_arg_1:            
                exit(32)
        if was_arg_3:
            if (not was_arg_1) or (not was_arg_2):
                exit(32)


#####____________________MAIN_PROGRAM____________________#####
if __name__ == "__main__":

    #___PARSING_CMD,_LOADIND_FILES,_CHECKING_XML___
    s_file_name, i_file_name = parse_cmd()
    xml_tree = load_source_file(s_file_name)

    if i_file_name != None:
        input_lines = load_input_file(i_file_name)
        input_lines_iter = iter(input_lines)

    xml_check(xml_tree)

    root = root_sort(xml_tree.getroot())
    duplicities_check(root)

    #___ADDING_LABELS_AND_THEIR_POSITION_TO_STACK___
    #___CHECKING_COUNT_&_TYPES_OF_INSTRUCTIONS___
    root = xml_tree.getroot()
    holder = Holder()
    checks = ArgTypeCountCheck()
    instruction_index: int = 0
    read_instr_counter = 0
    for i in root:
        cur_instr = Instruction(i.attrib['opcode'], i.attrib['order'])
        for arg in i:
            cur_instr.add_arg(arg.attrib['type'], arg.text)
            if cur_instr.get_name() == 'LABEL':
                holder.add_label(arg.text, instruction_index)
            if cur_instr.get_name() == 'READ':
                read_instr_counter += 1

        checks.count_check(cur_instr.get_name(), len(cur_instr.get_args()))
        checks.instruction_type_check(cur_instr.get_name(), cur_instr.get_args())             
        checks.regex_type_check(cur_instr.get_args())

        instruction_index += 1

    #________PROCESSING_ALL_INSTRUCTIONS________
    was_frame_created: bool = False     
    was_call: bool = False              
    instruction_index = 0               
    instruction = Handle()              
    was_type_instr:bool = False

    while instruction_index < len(root):

        name = root[instruction_index].attrib['opcode'].upper()
        order = root[instruction_index].attrib['order']
        current_instr = Instruction(name, order)
        
        for arg in root[instruction_index]:
            # adding arguments of intructions to array
            current_instr.add_arg(arg.attrib['type'], arg.text) 

        #________INSTRUCTIONS:_FRAMES,_CALLS________
        if name == 'CREATEFRAME':
            instruction.Createframe()
            was_frame_created = True
        elif name == 'PUSHFRAME':
            instruction.Pushframe(was_frame_created)
            instruction.local_frames_stack[0]
        elif name == 'POPFRAME':
            instruction.Popframe()
        elif name == 'MOVE':
            instruction.var_symb(current_instr.get_args(), name)
        elif name == 'DEFVAR':
            instruction.var(current_instr.get_args(), name)
        elif name == 'CALL':
            was_call = True
            instruction_index = instruction.label(current_instr.get_args(), name, instruction_index)
            continue
        elif name == 'RETURN':
            if not was_call:
                instruction_index+=1
                continue
            instruction_index = instruction.Return()
            continue

        #________INSTRUCTIONS:_DATA_STACK________
        elif name == 'PUSHS':
            instruction.symb(current_instr.get_args(), name)
        elif name == 'POPS':
            instruction.var(current_instr.get_args(), name)

        #________INSTRUCTIONS:_ARITHMETIC,_RELATIONAL,_BOOLEAN,_CONVERSAL________
        elif name=='ADD' or name=='SUB' or name=='MUL' or name=='IDIV' or name=='LT' or name=='GT' or name=='EQ' or name=='AND' or name=='OR' or name=='STRI2INT':
            instruction.var_symb_symb(current_instr.get_args(), name)
        elif name=='NOT' or name=='INT2CHAR':
            instruction.var_symb(current_instr.get_args(), name)

        #________INSTRUCTIONS:_INPUT,_OUTPUT_______
        elif name == 'READ':
            if i_file_name == None:
                input_lines = input()
            try: item = next(input_lines_iter)
            except: item = None
            instruction.var_type(current_instr.get_args(), item)
        elif name == 'WRITE':
            instruction.symb(current_instr.get_args(), name)

        #________INSTRUCTIONS:_STRINGS________
        elif name == 'CONCAT' or name =='GETCHAR' or name=='SETCHAR':
            instruction.var_symb_symb(current_instr.get_args(), name)
        elif name == 'STRLEN':
            instruction.var_symb(current_instr.get_args(), name)
        
        #________INSTRUCTIONS:_TYPE________
        elif name == 'TYPE':
            was_type_instr = True
            instruction.var_symb(current_instr.get_args(), name)
        
        #________INSTRUCTIONS:_PROGRAM_RUN_______
        elif name == 'LABEL':
            instruction_index += 1
            continue
        elif name == 'JUMP':
            instruction_index = instruction.label(current_instr.get_args(), name, instruction_index)
            continue
        elif name == 'JUMPIFEQ' or name == 'JUMPIFNEQ':
            index_to_jump = instruction.label_symb_symb(current_instr.get_args(), name)
            if index_to_jump == None: # symbols were not equal
                instruction_index += 1
                continue
            # symbols were equal -> jumping    
            instruction_index = index_to_jump
            continue
        elif name == 'EXIT':
            instruction.symb(current_instr.get_args(), name)

        #________INSTRUCTIONS:_DEBUG________
        elif name == 'DPRINT' or name == 'BREAK':
            pass
        else:
            exit(32) # uknown opcode

        instruction_index += 1

    exit(0)
    #________END_OF_CODE________