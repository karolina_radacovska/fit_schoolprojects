<?php



/*_____________________test.php_____________________*/
/*___________________IPP PROJECT____________________*/
/*___________KAROLINA_RADACOVSKA_xradac00___________*/




ini_set("display_errors", "stderr");

//_______________GLOBAL_VARIABLES_______________
global $argc;
global $html;
global $results;        $results = array();
global $tests;          $tests = array(); 
global $test_report;    $test_report = [];

global $directories;    $directories = [];
global $directory;      $directory = getcwd().'/';
global $recursive;      $recursive =  false;
global $interpret;      $interpret = 'interpret.py';
global $parser;         $parser = 'parse.php';
global $parse_only;     $parse_only = false;
global $int_only;       $int_only = false;
global $jexampath;      $jexampath = 'jexamxml/jexamxml.jar';       
// global $jexampath;      $jexampath = '/pub/courses/ipp/jexamxml/jexamxml.jar';
global $noclean;        $noclean = false;

//_______________RECURSIVE_DIRECTORY_SCAN_______________
function recursive_scan($dir, $all_files) {
    $tree = glob(rtrim($dir, '/') . '/*');
    if (is_array($tree)) {
        foreach($tree as $file) {
            if (is_dir($file))
                $all_files = recursive_scan($file, $all_files);
            elseif (is_file($file)){
                array_push($all_files, $file);
            }
        }
    }
    return $all_files;
}

//_______________DIRECTORY_SCAN_______________
function dir_scan($dir){
    $all_files = [];
    $tree = glob(rtrim($dir, '/') . '/*');
    if (is_array($tree)) {
        foreach($tree as $file) {
            if (is_file($file))
                array_push($all_files, $file);
        }
    }
    return $all_files;
}

//_______________PARSING_CMD_ARGUMENTS_______________
function parse_stdin(){
    global $argc, $directories, $directory, $recursive, $parse, $parse_only, $interpret, $int_only, $jexampath, $noclean;

    $longopts  = array(
        "help",            
        "directory:",       
        "recursive",        
        "parse-script:",    
        "int-script:",
        "parse-only", 
        "int-only",
        "jexampath:",
        "noclean"
    );
    $options = getopt("", $longopts);

    if(array_key_exists('help', $options)){
        if($argc != 2)
            exit(8);
        echo "Tester for parser and interpret scripts for IPPcode22.\n";
        echo "Usage: test.php [options]\n";
        echo "    [options] with no arguments:\n";
        echo "        --help              : Prints help.\n";
        echo "        --recursive         : Searches for tests in directory recursively.\n";
        echo "        --parse-only        : Tests only the parse script.\n";
        echo "        --int-only          : Tests only the interpret script.\n";
        echo "        --noclean           : The intermediate data will not be removed.\n";
        echo "    [options] with arguments:\n";
        echo "        --directory         : Searches for tests in given path to folder [--directory=path]\n";
        echo "        --parse-script      : Path to script parse.php [--parse-script=file]\n";       
        echo "        --int-script        : Path to script interpret.py [--int-script=file]\n";       
        echo "        --jexampath         : Path to file for xml comparing [--jexampath=path]\n";       
        exit(0);
    }

    // if missing, scanning actual adresar
    if(array_key_exists('directory', $options)){
        if(!is_readable($options['directory']))
            exit(41);

        $directory = $options['directory'];

        $len = strlen($directory);
        if($directory[$len-1] != '/')
            $directory .= "/";
    }
    // adding a directory to an array in case of recursion 
    array_push($directories, $directory);
    
    if(array_key_exists('recursive', $options))
        $recursive = true;

    if(array_key_exists('parse-script', $options)){
        if(!is_readable($options['parse-script']))
            exit(41);
        
        $parser = $options['parse-script'];
    } 

    if(array_key_exists('int-script', $options)){
        if(!is_readable($options['int-script']))
            exit(41);
        
        $interpret = $options['int-script'];
    }
    
    if(array_key_exists('parse-only', $options))
        $parse_only = true;

    if(array_key_exists('int-only', $options))
        $int_only = true;

    if(array_key_exists('jexampath', $options)){
        if(!is_readable($options['jexampath']))
            exit(41);

        $jexampath = $options['jexampath'];
        $len = strlen($jexampath);
        if($jexampath[$len-1] != '/')
            $jexampath .= "/jexamxml.jar";
        else{
            $jexampath .= "jexamxml.jar";
        }
    }

    if(array_key_exists('noclean', $options))
        $noclean = true;

    // parameters that cannot be combined        
    if(($parse_only && $int_only)||($parse_only && array_key_exists('int-script',$options)))
        exit(41);
    if(($int_only && $parse_only)||($int_only && array_key_exists('parse-script',$options))||($int_only && array_key_exists('jexampath', $options)))
        exit(41);
}

//_______________SCANNING_&_GROUPING_TESTS_______________
function division_and_execution(){
    global $tests, $results, $directories, $directory, $recursive;

    if($recursive){
        $directories = recursive_scan($directory, $directories);
    } else {
        $directories = dir_scan($directory);
    }
    foreach($directories as $src){
        if(str_contains($src, ".src")){ 

            $basename = basename($src, ".src"); // return basename of a file
            $dirname = dirname($src); // return dirname of a file
            
            $tests[$dirname][$basename] = array();
            $results[$basename] = ['-', '-'];

            $in = $dirname."/".$basename.".in";           
            $out = $dirname."/".$basename.".out";         
            $rc = $dirname."/".$basename.".rc";           

            if (!file_exists($in))  $in = write_to_spec_file($in, "");
            if (!file_exists($out)) $out = write_to_spec_file($out, "");
            if (!file_exists($rc))  $rc = write_to_spec_file($rc, "0");
            
            $test_result = test($basename, $src, $in, $out, $rc);
            if($test_result == true){
                array_push($tests[$dirname][$basename], "passed");
                array_push($tests[$dirname][$basename], $results[$basename][0]); // rc of script
                array_push($tests[$dirname][$basename], $results[$basename][1]); // expected rc
            }
            else{
                array_push($tests[$dirname][$basename], "failed");
                array_push($tests[$dirname][$basename], $results[$basename][0]); // rc of script
                array_push($tests[$dirname][$basename], $results[$basename][1]); // expected rc
            }
        }
    }
    return $tests;
}

//_______________CHECKING_RETURNED_CODE_&_OUTPUT_______________
function rc_check($basename, $expected, $mine){
    global $results;
    $file = fopen($expected, "r");
    $rc_origin = fgets($file);
    $results[$basename] = [$mine, $rc_origin];
    if ($rc_origin == $mine){
        return true;
    }else return false;
}

function diff($out, $output){
    global $noclean;

    $temp = write_to_file($output);
    $diff_output = shell_exec("diff ".$out ." ". $temp);
    if(!$noclean)
        exec("rm temp_file_name_*");

    if($diff_output == "")
        return true;
    return false;
}

//_______________WRTTING_TO_FILES_______________
function write_to_spec_file($name_of_file, $message){
    $my_temp_file_handle = fopen($name_of_file, "w");
    fwrite($my_temp_file_handle, $message);
    fclose($my_temp_file_handle);
    return $name_of_file;
}

function write_to_file($message){
    $temp_file_name = tempnam(".", "temp_file_name_");
    $my_temp_file_handle = fopen($temp_file_name, "w");
    if($message == null){
        $message = "";
    }
    fwrite($my_temp_file_handle, $message);
    fclose($my_temp_file_handle);
    return $temp_file_name;
}


//_______________EXECUTING_SCRIPTS_&_TESTING_OUTPUTS_______________
function test($basename, $src, $in, $out, $rc){
    global $results, $argc, $directories, $directory, $recursive, $parser, $parse_only, $interpret, $int_only, $jexampath, $noclean;
   
     // --parse-only parameter tests only parse.php script 
    if($parse_only){
        exec("php8.1 ".$parser." < ".$src, $output, $return_code);
        $output = shell_exec("php8.1 ".$parser." < ".$src);

        $temp_file_name = write_to_file($output);
        $jexa_result = shell_exec("java -jar ".$jexampath." ".$temp_file_name." ".$out);
        if(!$noclean)
            exec("rm temp_file_name_*");

        if(rc_check($basename, $rc, $return_code)){
            if($return_code == 0){
                if(str_contains($jexa_result, "Two files are identical"))   
                    return true;
                else return false;
            }
            return true;
        }else return false;
    }
    // --int-only parameter tests only intepret.py script
    else if($int_only){
        exec("python3.8 " . $interpret . " --source=".$src." --input=".$in, $output, $return_code);
        $output = shell_exec("python3.8 " . $interpret . " --source=".$src." --input=".$in);

        if(rc_check($basename, $rc, $return_code)){
            if($return_code == 0){
                if(diff($out, $output)) // the error code is 0 -> the output exists and is needed to check the difference
                    return true;
                return false;
            }
            return true;
        }else return false;        
    // both -> parser output is used as an intepret input
    }else{
        exec("php8.1 $parser < $src > out.xml", $parse_output, $return_code);
        $parse_output = shell_exec("php8.1 $parser < $src"); // making output string 

        $temp_file_name = write_to_file($parse_output);

        exec("python3.8 " . $interpret . " --source=".$temp_file_name." --input=".$in, $int_output, $return_code);
        $int_output = shell_exec("python3.8 " . $interpret . " --source=".$temp_file_name." --input=".$in);

        if(!$noclean){
            exec("rm temp_file_name_*");
            exec("rm ./out.xml");
        }

        if(rc_check($basename, $rc, $return_code)){
            if($return_code == 0){
                if(diff($out, $int_output)) // the error code is 0 -> the output exists and is needed to check the difference
                    return true;
                return false;
            }
            return true;
        }else return false;       
    }
}

//_______________CALCULATING_TESTS_RESULTS_______________
function calculate_test_evaluation(){
    global $test_report, $tests;
    foreach($tests as $dir_name=>$test_name_arr){
        $passed = 0;
        $failed = 0;
        foreach($test_name_arr as $test_name=>$tests_results){
            if($tests_results[0] == 'passed')
                $passed++;
            if($tests_results[0] == 'failed')
                $failed++;
        }
        $single_dir = new stdClass();
        $single_dir->dir_name = $dir_name;
        $single_dir->passed = $passed;
        $single_dir->failed = $failed;
        $json = json_encode($single_dir);

        array_push($test_report, $json);
    }
    return $test_report;
}

//_______________GETTING_PASSED/FAILED_TESTS_______________
function get_passed_tests($dir_name){
    global $test_report;
    foreach($test_report as $dir){
        $set = json_decode($dir);
        if($set->dir_name == $dir_name)
            return $set->passed;
    }
    return 0;
}

function get_failed_tests($dir_name){
    global $test_report;
    foreach($test_report as $dir){
        $set = json_decode($dir);
        if($set->dir_name == $dir_name)
            return $set->failed;
    }
    return 0;
}

//_______________HTML_CODE_GENERATOR_______________
function generate_html($tests){
    global $html, $test_report;
    $html = '<!DOCTYPE html>
    <html lang="en">
    
    <head>
        <meta charset="utf-8"/>
        <title>IPPcode22 TEST REPORT</title>
    </head>

    <body>
        <div id="title_and_author">
            <h1>Principles of Programming Languages</h1>
            <h1>IPPcode22 test results</h1>
            <h4>Author: Karolína Radačovská - xradac00</h4>
        </div>
        <div>
            <table id="head" align="center">
                    <tr id="table_head">
                        <th class="cell">TEST</th>
                        <th class="cell">Passed/Failed</th>
                        <th class="cell">Result</th>
                        <th class="cell">Expected result</th>
                    </tr>
            </table>
        </div>
        <div>';
        
        $total_passed = 0;
        $total_failed = 0;
        foreach($tests as $dir_name=>$test_names_arr){
            $passed = get_passed_tests($dir_name);
            $total_passed += $passed;
            $failed = get_failed_tests($dir_name);
            $total_failed += $failed;
            $total = $failed + $passed;
            $html .= '
            <p>
                <span>Test directory: '.$dir_name.'/</span>
                <span style="float:right;">Passed:'.$passed.', Failed:'.$failed.', Total:'.$total.'</span>
            </p>
            <table id="body" align="center">';
        foreach($test_names_arr as $test_name=>$test_results){
            if($test_results[0] == 'passed'){
                $html .= '
                    <tr>    
                    <td class="cell_passed">'.$test_name.'</td> 
                    <td class="cell_passed">'.$test_results[0].'</td> 
                    <td class="cell_passed">'.$test_results[1].'</td> 
                    <td class="cell_passed">'.$test_results[2].'</td>
                    </tr>';
            }
            else{
                $html .= '
                    <tr>    
                    <td class="cell_failed">'.$test_name.'</td> 
                    <td class="cell_failed">'.$test_results[0].'</td> 
                    <td class="cell_failed">'.$test_results[1].'</td> 
                    <td class="cell_failed">'.$test_results[2].'</td>
                    </tr>';
            }
        }
        $html .= '</table>';
    }
    $total = $total_failed+$total_passed;
    $html .= '
        </div>
        <p><span style="float:right; padding-bottom: 30px; font-size: large;">Total passed: '.$total_passed.', Total failed: '.$total_failed.', Total: '.$total.'</span></p>
    </body>

    <style>
    *{  
        background-color: white;
        padding-left: 30px;
        padding-right: 30px;
    }
    #title_and_author{
        text-align:center;
        margin-bottom: 50px;
    }
    table{
        padding: 0;
        width: 100%;
        justify-self: center;
        border: 1px solid black;
    }
    #head{
        font-size: larger;
        margin-bottom: 50px;
    }
    thead{
        background-color: bisque;
    }
    th, td{
        text-align: center;
        width: 25%;    
    }
    .cell{
        background-color: bisque;
        padding: 5px;
    }
    .cell_passed{
        background-color: #7ffed4;
    }
    .cell_failed{
        background-color: #ff726f;
    }
    </style>

    </html>';

    echo $html;
}

////////////////////////////////////////////////////////////////////////////
//__________________________________MAIN__________________________________//
if($argc < 2)
    echo "Error: Less parameters than excepted.";

parse_stdin();
$tests = division_and_execution();
calculate_test_evaluation();
generate_html($tests);

?>