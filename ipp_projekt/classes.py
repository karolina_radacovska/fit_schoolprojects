import re

#____________________CLASS:_INSTRUCTION_INFORMATION____________________#
class Instruction:
    name: str
    order: int
    args: list

    def __init__(self, instr_name, instr_order):
        self.name = instr_name
        self.order = instr_order
        self.args = []

    def get_name(self):
        return self.name

    def get_order(self):
        return self.order

    def add_arg(self, arg_type: str, value:str):
        tmp = Argument(arg_type, value)
        self.args.append(tmp)

    def get_args(self):
        return self.args

    def print(self):
        print(f"Instruction: opcode:{self.name} order:{self.order}")
        for single_arg in self.args:
            single_arg.print()


#____________________CLASS:_ARGUMENT_INFORMATION____________________#
class Argument:
    typ: list       # 'type' is key word, so I use 'typ'...
    value: str

    def __init__(self, arg_type: str, arg_value):
        self.typ = []
        self.typ.append(arg_type)
        self.value = arg_value

    def get_value(self):
        return self.value

    def get_type(self):
        return self.typ

    def print(self):
        print(f"Argument: type: '{self.typ}', value: '{self.value}'")


#______________________CLASS:_ARGUMENT_CHECKER_____________________#
class ArgTypeCountCheck():
    instructions_arg_types: dict

    def __init__(self):
        # DEFINED ALL INSTRUCTIONS WITH THEIR TYPES
        self.instructions_arg_types = {
        'CREATEFRAME':[], 'POPFRAME':[], 'PUSHFRAME':[], 'RETURN':[], 'BREAK':[],     

        'DEFVAR':['var'], 'POPS':['var'],

        'PUSHS':['symb'], 'WRITE':['symb'], 'EXIT':['symb'], 'DPRINT':['symb'],

        'CALL':['label'], 'LABEL':['label'], 'JUMP':['label'],

        'MOVE':['var','symb'], 'INT2CHAR':['var','symb'], 'STRLEN':['var','symb'], 'TYPE':['var','symb'], 'NOT':['var','symb'],

        'READ':['var', 'type'],

        'JUMPIFEQ':['label','symb','symb'], 'JUMPIFNEQ':['label','symb','symb'],
        
        'ADD': ['var','symb','symb'], 'SUB': ['var','symb','symb'], 'MUL': ['var','symb','symb'], 'IDIV':['var','symb','symb'],
        'LT':['var','symb','symb'], 'GT':['var','symb','symb'], 'EQ':['var','symb','symb'], 'AND':['var','symb','symb'],
        'OR':['var','symb','symb'], 'STRI2INT':['var','symb','symb'], 
        'CONCAT':['var','symb','symb'], 'GETCHAR':['var','symb','symb'], 'SETCHAR':['var','symb','symb'] 
        }
        # SYMBOL TYPES
        self.symbols = ['string', 'int', 'bool', 'nil', 'var']

    #__________LOADING_TYPES_&_VALUES__________
    def get_types_and_values(self, args):
        arg_value_list = []
        arg_type_list = []
        for current_arg in args:
            arg_value_list.append(current_arg.get_value())
            arg_type_list.append(current_arg.get_type()[0])
            
        return arg_type_list, arg_value_list

    #__________FUNCTIONS_FOR_CHECKING_ARGUMENTS__________
    def count_check(self, name, actual_count):
        if name in self.instructions_arg_types:
            expected_count = len(self.instructions_arg_types[name])
            if expected_count != actual_count:
                exit(32) # Instruction has wrong number of arguments!

    def get_type_list_of_instr(self, args: list):
        actual_types = []
        for current_arg in args:
            actual_types.append(current_arg.get_type()[0])
        return actual_types

    def instruction_type_check(self, name, args: list):
        actual_types = self.get_type_list_of_instr(args)

        if name in self.instructions_arg_types:
            expected_types = self.instructions_arg_types[name]

            # types are parsed on the base of their actual count 
            if len(actual_types) == 1:
                # symb case is special
                if expected_types[0] == 'symb':
                    if not actual_types[0] in self.symbols:
                        exit(53)
                # other cases of types -> var, type, label
                elif(actual_types[0] != expected_types[0]):
                    exit(53)

            elif len(actual_types) == 2:
                # symb case is special, in instructions with two arguments, symb is at 2nd place
                if expected_types[1] == 'symb':
                    if not actual_types[1] in self.symbols:
                        exit(53)
                # other cases of types -> var, type, label
                elif(actual_types[0]!=expected_types[0]) and (actual_types[1]!=expected_types[1]):
                    exit(53)

            elif len(actual_types) == 3:
                 # symb case is special, in instructions with three arguments, symbs are at 2nd and 3rd place
                if(expected_types[1] == 'symb') and (expected_types[2] == 'symb'):
                    if (not actual_types[1] in self.symbols) and (not actual_types[2] in self.symbols):
                        exit(53)
                # handling the first argument -> only var/label can appear here
                if actual_types[0] != expected_types[0]:
                    exit(53)

    def regex_type_check(self, args: list):
        types, values = self.get_types_and_values(args)
        i = 0
        while i < len(types):

            if 'var' == types[i]:
                if not re.match("^(LF|GF|TF)@[a-zA-Z$&%*!?_-][a-zA-Z0-9$&%*!?_-]*$", values[i]):
                    exit(32)
            if 'label' == types[i]:
                if not re.match("^[a-zA-Z_$&%*!?-][a-zA-Z0-9_$&%*!?-]*$", values[i]):
                    exit(32)
            if 'string' == types[i]:
                if values[i] == None: # empty string check
                    i += 1
                    continue 
                if not re.match("^(\\\\[0-9]{3}|[^\s\\\\#])*$", values[i]):
                    exit(32)
            if 'int' == types[i]:
                if not re.match("[-]?[0-9]+$", values[i]):
                    exit(32)
            if 'bool' == types[i]:
                if not re.match("^(true|false)$", values[i]):
                    exit(32)
            if 'nil' == types[i]:
                if not re.match("^nil$", values[i]):
                    exit(32)
            if 'type' == types[i]:
                if not re.match("(string|int|bool)$", values[i]):
                    exit(32)

            i += 1


#____________________CLASS:_HANDLING_ALL_INSTRUCTIONS____________________#
class Handle():

    def __init__(self):
        # FRAMES:
        self.frame : dict = None
        self.global_frame: dict = {}
        self.local_frames_stack: list = []
        # DATA STACK:
        self.stack_array: list = []
        # HELP:
        self.was_frame_created = False
        self.checks = ArgTypeCountCheck()
        self.holder = Holder()  
   
    #__________FUNCTION_FOR_FINDING_RIGHT_FRAME__________
    def looking_for_frame(self, frame_str: str, name_value: str):
        if frame_str == 'TF':
            if self.was_frame_created == False:
                exit(55)
            return self.frame
            
        if frame_str == 'LF':
            if bool(self.local_frames_stack)==False:
                exit(55)
            for frame in reversed(self.local_frames_stack):
                for variable in frame:
                    if name_value in variable:
                        return frame
                exit(54)

        if frame_str == 'GF':
            return self.global_frame

    #__________FUNCTIONS_FOR_GETTING_TYPES_&_VALUES_FROM_INSTRUCTION_ARGUMENTS__________
    # var is in all instruction cases at the first position
    def get_variable(self, values: list):
        list = values[0].split('@') 
        return list[0], list[1] # frame, value

    def get_symbol(self, values:list, name):
        if name in self.checks.instructions_arg_types:
            types = self.checks.instructions_arg_types[name]
            count_types = len(types)

        # symb cases
        if count_types == 1:
            list = values[0].split('@')
            if len(list) == 1:
                return list[0] # name
            return list[0], list[1]

        # var_symb cases
        elif count_types == 2:
            list = values[1].split('@')
            if len(list) == 1:
                return list[0]
            return list[1] # symbol value, frame or type is not important now

        # var_symb_symb or label_symb_symb cases
        elif count_types == 3:
            symb1 = values[1].split('@')
            symb2 = values[2].split('@')
            if(len(symb1)==1):
                symb1 = symb1[0]
            if(len(symb2) == 1):
                symb2 = symb2[0]
            return symb1, symb2
            
    def get_label(self, values: list):
        list = values[0].split('@')
        return list[0]


    #__________WRTITING_TO_FRAME__________
    def write_to_frame(self, frame_str, name, type_to_write, value):
        if frame_str == 'TF':
            frame = self.looking_for_frame(frame_str, name)
            if name not in frame:
                exit(54) #Trying to reach variable that does not exist!
            frame[name] = {'type': type_to_write, 'value': value}

        if frame_str == 'LF':
            frame = self.looking_for_frame(frame_str, name)
            if frame == None:
                exit(55) #Trying to reach variable that does not exist!
            for variable in frame:
                if variable == name:
                    frame[variable]['type'] = type_to_write
                    frame[variable]['value'] = value

        if frame_str == 'GF':
            frame = self.looking_for_frame(frame_str, name)
            if name not in frame:
                exit(54) #Trying to reach variable that does not exist!
            frame[name] = {'type':type_to_write, 'value':value}

       
    #__________ADDING_VARIABLE_TO_FRAME__________
    def add_to_frame(self, variable_frame_string, variable_name):
        if variable_frame_string == 'LF':
            if bool(self.local_frames_stack) == False: exit(55)
            frame = self.local_frames_stack[-1]
        else:
            frame = self.looking_for_frame(variable_frame_string, variable_name)
       
        if variable_name not in frame:
            frame[variable_name] = {'type': "", 'value': ""}
        else: exit(52) # Trying to define already defined variable


    #__________GETTING_VARIABLES_AND_TYPES_FROM_FRAMES__________
    def get_value_from_frame(self, symbol, frame_str):
        frame = self.looking_for_frame(frame_str, symbol)
        i = 0
        while i < len(frame):
            if symbol in frame:
                return frame[symbol]['value']
            i += 1
        exit(54)

    def get_type_from_frame(self, symbol, frame_str):
        frame = self.looking_for_frame(frame_str, symbol)
        i = 0
        while i < len(frame):
            if symbol in frame:
                return frame[symbol]['type']
            i += 1
        exit(54)


     #__________CONVERTING_UNICODE_TO_ES__________
    def replace_unicode(self, match):
        return chr(int(match.group(1)))

    def check_and_convert_if_needed(self, typ, value):
        if typ == 'string' and re.match("([^\s\\\\#]|(\\\\[0-9]{3}))*", value):
            regex = re.compile(r"\\(\d{1,3})")
            value = regex.sub(self.replace_unicode, value)
        return value

    #____________________[INSTRUCTION]_<>___________________
    # creates a new temporary frame
    def Createframe(self):
        self.frame = {}
        self.was_frame_created = True 

    # moves temporary frame to stack and makes it local frame
    def Pushframe(self, was_frame_created: bool):
        if was_frame_created == True:
            self.local_frames_stack.append(self.frame)
            self.frame = {}
        else: exit(55)
        self.was_frame_created = False # after instruction is called, there's no tmp frame

    # moves the top local frame from stack and makes it temporary
    def Popframe(self):
        count = len(self.local_frames_stack)
        if count > 0:
            self.frame = self.local_frames_stack.pop()
            self.was_frame_created = True  # tmp frame exists now
        else: exit(55)

    # returns to a position where call appeared
    def Return(self):
        return self.holder.where_to_continue()

    #____________________[INSTRUCTION]_<var>____________________
    def var(self, args: list, name):
        types, values = self.checks.get_types_and_values(args)
        variable_frame_string, variable_name = self.get_variable(values)

        # pops value from data stack into a variable
        if name == 'POPS':
            if len(self.stack_array) <= 0: exit(56)    
            popped_item = self.stack_array.pop() # [typ, value]
            self.write_to_frame(variable_frame_string, variable_name, popped_item[0], popped_item[1])

        # defines a new variable to proper frame
        if name == 'DEFVAR':
            self.add_to_frame(variable_frame_string, variable_name)

    #____________________[INSTRUCTION]_<label>____________________
    def label(self, args:list, name, order):
        types, values = self.checks.get_types_and_values(args)
        label = self.get_label(values)

        # jumps to defined label
        if name == 'JUMP':
            return self.holder.where_to_jump(label)

        # jumps to defined label and stores the next position in code into label stack
        if name == 'CALL':
            self.holder.call_stack_push(int(order)+1)
            return self.holder.where_to_jump(label)
    
    #____________________[INSTRUCTION]_<symb>____________________
    def symb(self, args:list, name):
        types, values = self.checks.get_types_and_values(args)

        typ = types[0]
        value = values[0]

        if typ == 'var':
            frame_and_value = value.split('@')
            value = self.get_value_from_frame(frame_and_value[1], frame_and_value[0])
            typ = self.get_type_from_frame(frame_and_value[1], frame_and_value[0])
        
        if typ=="" and value=="": exit(56) # missing value

        # writes symbol to stdout
        if name == 'WRITE':
            # bool case:
            if value == 'True':  
                value = 'true'
            if value == 'False':  
                value = 'false'
            # nil case:
            if typ == 'nil':
                print("", end='')
                return
            # string case:
            if typ == 'string':
                value = self.check_and_convert_if_needed(typ, value)
            # int case and printing out:
            print(value, end='')

        # exits the program 
        if name == 'EXIT':
            # only int or string can be handled as exit code
            if typ == 'int':   
                if int(value)<0 or int(value)>49:
                    exit(57)
                exit(int(value))
            if typ == 'string':
                if not re.match("[0-9]+", value):
                    exit(53)
                if int(value)<0 or int(value)>49:
                    exit(57) 
                exit(int(value))
            exit(53)

        # pushes symbol to data stack
        if name == 'PUSHS':
            arr = [typ, value]
            self.stack_array.append(arr)

    def string_to_bool(self, string):
        if string=='true': return True
        if string=='false': return False
        return None

    #____________________[INSTRUCTION]_<var>_<symb>_<symb>____________________
    def var_symb_symb(self, args:list, name):
        types, values = self.checks.get_types_and_values(args)
        variable_frame_str, variable_name = self.get_variable(values)

        # values[1] -> 1.value, types[1] -> 1.type
        value_1 = values[1]
        type_1 = types[1]
        # values[2] -> 2.value. types[2] -> 2.type
        type_2 = types[2]
        value_2 = values[2]

        # getting value which is stored into a variable
        if type_1 == 'var':
            symb1_value, symb2_value = self.get_symbol(values, name)
            value_1 = self.get_value_from_frame(symb1_value[1], symb1_value[0])
            type_1 = self.get_type_from_frame(symb1_value[1], symb1_value[0])
            if type_1=="" and value_1=="":
                exit(56)
        if type_2 == 'var':
            symb1_value, symb2_value = self.get_symbol(values, name)
            value_2 =  self.get_value_from_frame(symb2_value[1], symb2_value[0])
            type_2 = self.get_type_from_frame(symb2_value[1], symb2_value[0])
            if type_2=="" and value_2=="":
                exit(56)

        # converstion NoneType to an empty string
        if value_1 == None: value_1=""
        if value_2 == None: value_2=""

        if type_1 == 'string' and '\\' in value_1: 
            value_1 = self.check_and_convert_if_needed(type_1, value_1)
        if type_2 == 'string' and '\\' in value_2: 
            value_2 = self.check_and_convert_if_needed(type_2, value_2)

        # adds two symbols and stores the result into a variable
        if name == 'ADD':
            if type_1 != 'int' or type_2 != 'int': exit(53)
            self.write_to_frame(variable_frame_str, variable_name, 'int', int(value_1) + int(value_2))
        
        # subtracts the second symbol from the first and stores the result into a variable
        if name == 'SUB':
            if type_1 != 'int' or type_2 != 'int': exit(53)
            self.write_to_frame(variable_frame_str, variable_name, 'int', int(value_1) - int(value_2))
        
        # multiplies two symbols and stores the result into a variable
        if name == 'MUL':
            if type_1 != 'int' or type_2 != 'int': exit(53)
            self.write_to_frame(variable_frame_str, variable_name, 'int', int(value_1)*int(value_2))

        # divides the first symbol by the second and stores the result into a variable
        if name == 'IDIV':
            if type_1 != 'int' or type_2 != 'int': exit(53)
            if int(value_2) == 0: exit(57)
            self.write_to_frame(variable_frame_str, variable_name, 'int', int(value_1) // int(value_2))

        # compares two symbols, stores true into a variable, if equal, otherwise stores false
        if name == 'EQ':   
            if (type_1 == type_2) or type_1=='nil' or type_2=='nil':
                if value_1 == value_2:
                    self.write_to_frame(variable_frame_str, variable_name, 'bool', 'true')
                else:
                    self.write_to_frame(variable_frame_str, variable_name, 'bool', 'false')
            else: exit(53)
        
        # compares two symbols, stores true into a variable, if the first is smaller than the other, otherwise stores false
        if name == 'LT':
            if value_1=='nil' or value_2=='nil': exit(53)
            if type_1 == type_2:
                if type_1 == 'int': 
                    value_1 = int(value_1)
                    value_2 = int(value_2)
                if value_1 < value_2:
                    self.write_to_frame(variable_frame_str, variable_name, 'bool', 'true')
                else:
                    self.write_to_frame(variable_frame_str, variable_name, 'bool', 'false')
            else: exit(53)
        
        # compares two symbols, stores true into a variable, if the first is greater than the other, otherwise stores false
        if name == 'GT':
            if value_1=='nil' or value_2=='nil': exit(53)
            if type_1 == type_2:
                if type_1 == 'int': 
                    value_1 = int(value_1)
                    value_2 = int(value_2)
                if value_1 > value_2:
                    self.write_to_frame(variable_frame_str, variable_name, 'bool', 'true')
                else:
                    self.write_to_frame(variable_frame_str, variable_name, 'bool', 'false')
            else: exit(53)

        # applies conjunction of two symbols and stores the bool result into a variable
        if name == 'AND':
            if type_1 != 'bool' or type_2 != 'bool': exit(53) # Types of AND instruction are not bools!
            if self.string_to_bool(value_1) and self.string_to_bool(value_2) == True:
                self.write_to_frame(variable_frame_str, variable_name, 'bool', 'true')
            else:
                self.write_to_frame(variable_frame_str, variable_name, 'bool', 'false')
        
        # applies disjunction of two symbols and stores the bool result into a variable
        if name == 'OR':
            if type_1 != 'bool' or type_2 != 'bool': exit(53)
            if self.string_to_bool(value_1) or self.string_to_bool(value_2) == True:
                self.write_to_frame(variable_frame_str, variable_name, 'bool', 'true')
            else:
                self.write_to_frame(variable_frame_str, variable_name, 'bool', 'false')

        # stores ordinal value of symbol1 with position of symbol2 into a variable
        if name == 'STRI2INT':
            if type_1 != 'string' or type_2 != 'int': exit(53)
            if int(value_2) < 0: exit(58) # index out of range
            if int(value_2) >= len(value_1): exit(58) # index out of range
            self.write_to_frame(variable_frame_str, variable_name, 'int', ord(value_1[int(value_2)]))

        # concatenates two symbols and stores the result into a variable
        if name == 'CONCAT':
            if type_1 != 'string' or type_2 != 'string': exit(53) #Trying to concatenate no string type variable!
            self.write_to_frame(variable_frame_str, variable_name, 'string', value_1+value_2)

        # stores one char of symbol1 with position of symbol2 into a variable
        if name == 'GETCHAR':
            if type_1 != 'string' or type_2 != 'int': exit(53)
            if int(value_2) >= len(value_1): exit(58) # index out of range
            if int(value_2) < 0: exit(58) # index out of range
            self.write_to_frame(variable_frame_str, variable_name, 'string', value_1[int(value_2)])

        # sets char into symbol1 on position symbol2 into a variable
        if name == 'SETCHAR':
            type_of_var = self.get_type_from_frame(variable_name, variable_frame_str)
            to_change = self.get_value_from_frame(variable_name, variable_frame_str)
            if type_of_var=="" and to_change=="": exit(56)

            if type_of_var != 'string': exit(53)
            if type_1 != 'int' or type_2 != 'string': exit(53)
            if int(value_1) < 0: exit(58) # index out of range
            if value_2 == "": exit(58)
            if int(value_1) >= len(to_change): exit(58) # index out of range

            to_change = list(to_change)
            to_change[int(value_1)] = value_2[0]
            to_change = "".join(to_change)
            self.write_to_frame(variable_frame_str, variable_name, 'string', to_change)


    #____________________[INSTRUCTION]_<var>_<symb>____________________
    def var_symb(self, args: list, name):
        types, values = self.checks.get_types_and_values(args)
        variable_frame_string, variable_name = self.get_variable(values)
        
        # values[1] -> wanted value, types[1] -> wanted type
        typ = types[1]
        value = values[1]

        # getting value which is stored into a variable
        if typ == 'var': 
            frame_and_value = value.split('@')
            value = self.get_value_from_frame(frame_and_value[1], frame_and_value[0])
            typ = self.get_type_from_frame(frame_and_value[1], frame_and_value[0])

        # moves symbol from defined frame to variable
        if name == 'MOVE':
            if typ=="" and value=="": exit(56)
            self.write_to_frame(variable_frame_string, variable_name, typ, value)

        # negates boolean value
        if name == 'NOT':
            if typ=="" and value=="": exit(56)
            if typ != 'bool': exit(53)
            if value == 'true': what_value = 'false'
            else: what_value = 'true'
            self.write_to_frame(variable_frame_string, variable_name, 'bool', what_value)
        
        # changes an integer to unicode symbol
        if name == 'INT2CHAR':
            if typ=="" and value=="": exit(56)
            if typ != 'int':
                exit(53)
            if int(value) > 1114111 or int(value)<0:
                exit(58)
            self.write_to_frame(variable_frame_string, variable_name, 'string', chr(int(value)))

        # counts length of a string
        if name == 'STRLEN':
            if typ=="" and value=="": exit(56)
            if typ != 'string': exit(53)
            if value==None: value=""
            self.write_to_frame(variable_frame_string, variable_name, 'int', len(value))

        # finds out type of variable and write it to another one
        if name == 'TYPE':
            self.write_to_frame(variable_frame_string, variable_name, 'string', typ)


    #____________________[INSTRUCTION]_<var>_<type>____________________
    # 'READ' case - reads the input, stores it into a variable
    def var_type(self, args: list, input_file):
        types, values = self.checks.get_types_and_values(args)
        variable_frame_str, variable_name = self.get_variable(values)
        typ = values[1]

        # empty string
        if input_file == None:
            self.write_to_frame(variable_frame_str, variable_name, 'nil', 'nil')
            return

        # handling types to check if the input had correct type
        try:
            if typ == 'int':
                if re.match("[0-9]*", input_file):
                    try:
                        self.write_to_frame(variable_frame_str, variable_name, typ, int(input_file))
                    except: 
                        self.write_to_frame(variable_frame_str, variable_name, 'nil', 'nil')
                else: exit(57)
            elif typ == 'bool':
                if re.match("true", input_file, re.IGNORECASE):
                    self.write_to_frame(variable_frame_str, variable_name, typ, 'true')
                else:
                    self.write_to_frame(variable_frame_str, variable_name, 'bool', 'false')
            else: # string case
                self.write_to_frame(variable_frame_str, variable_name, typ, input_file)
        except:
            self.write_to_frame(variable_frame_str, variable_name, 'nil', 'nil')


    #____________________[INSTRUCTION]_<label>_<symb>_<symb>____________________
    def label_symb_symb(self, args: list, name):
        types, values = self.checks.get_types_and_values(args)
        label = self.get_label(values)
        if label not in self.holder.label_dict: exit(52)

        # values[1] -> 1. value, types[1] -> 1. type
        value_1 = values[1]
        type_1 = types[1]
        # values[2] -> 2. value, types[2] -> 2. type
        type_2 = types[2]
        value_2 = values[2]

        # getting value which is stored into a variable
        if type_1 == 'var':
            symb1_value, symb2_value = self.get_symbol(values, name)
            value_1 = self.get_value_from_frame(symb1_value[1], symb1_value[0])
            type_1 = self.get_type_from_frame(symb1_value[1], symb1_value[0])
            if type_1=="" and value_1=="":
                exit(56)
        if type_2 == 'var':
            symb1_value, symb2_value = self.get_symbol(values, name)
            value_2 =  self.get_value_from_frame(symb2_value[1], symb2_value[0])
            type_2 = self.get_type_from_frame(symb2_value[1], symb2_value[0])
            if type_2=="" and value_2=="":
                exit(56)    

        # conversion of NoneType to an empty string
        if value_1 == None: value_1=""
        if value_2 == None: value_2=""

        # converting unicode to escape sequence character
        if type_1 == 'string' and '\\' in value_1: 
            value_1 = self.check_and_convert_if_needed(type_1, value_1)
        if type_2 == 'string' and '\\' in value_2: 
            value_2 = self.check_and_convert_if_needed(type_2, value_2)

        # jumps to defined label if equal
        if name == 'JUMPIFEQ':
            if (type_1 == type_2) or (type_1=='nil') or (type_2=='nil'):
                if type_1 == 'int' and type_2=='int':
                    value_1 = int(value_1)
                    value_2 = int(value_2)
                if value_1 == value_2:
                    return self.holder.where_to_jump(label)
            else: exit(53)
            return None

        # jumps to defined label if not equal
        if name == 'JUMPIFNEQ':
            if (type_1==type_2) or (type_1=='nil') or (type_2=='nil'):
                if type_1 == 'int' and type_2=='int':
                    value_1 = int(value_1)
                    value_2 = int(value_2)
                if value_1 != value_2:
                    return self.holder.where_to_jump(label)
            else: exit(53)
            return None


#__________________CLASS:_OPERATIONS_WITH_LABELS:__________________#
class Holder():
    call_stack = []     # storing the position of next instruction after CALL instruction
    label_dict = {}     # { label : position } 

    def __init__(self) -> None:
        pass

    def call_stack_push(self, position):
        self.call_stack.append(position)

    # adding label with its position to dictionary 
    def add_label(self, label, position): 
        if label not in self.label_dict:
            self.label_dict[label] = position
        else:
            exit(52) # Label name already exist

    # searching for position of given label
    def where_to_jump(self, label):
        if label in self.label_dict:
            return self.label_dict[label]
        else:
            exit(52) # Trying to reach label which does not exist

    # searching for stored position of next instruction after CALL instruction    
    def where_to_continue(self):
        count = len(self.call_stack)
        if count > 0:
            return self.call_stack.pop()
        exit(56) # Trying to reach label which does not exist!
