<?php

    /*____________________parse.php____________________*/
    /*___________________IPP PROJECT___________________*/
    /*___________KAROLINA_RADACOVSKA_xradac00__________*/


    ini_set('display_error', 'stderr');

    //________CLASS:_ARGUMENT_TYPES_OF_INSTRUCTIONS:________
    abstract class arg_types{
        const variable = 2;
        const symb = 3;
        const label = 4;
        const type = 5; 
    };

    // global variable for concatenating the result xml program
    $xml_out = "";

    //________CHECKING_COUNT_OF_INSTRUCTIONS_ARGUMENTS_________
    function check_arg_count($count, $name){
        switch($name){
            // zero arguments expected 
            case'CREATEFRAME': case'PUSHFRAME': case'POPFRAME': case'RETURN': case'BREAK':
                if($count == 0) return;
                else exit(23); // syntax error - invalid number of arguments of instruction
            
            // one argument expected
            case'DEFVAR'; case'CALL': case'PUSHS': case'POPS': case'WRITE': case'LABEL': case'JUMP': case'EXIT': case'DPRINT':
                if($count == 1) return;
                else exit(23);
            
            // two arguments expected
            case 'MOVE': case 'INT2CHAR': case 'READ': case 'STRLEN': case 'TYPE': case'NOT':
                if($count == 2) return;
                else exit(23);         
            
            // three arguments expected
            case'ADD': case'SUB': case'MUL': case'IDIV': case'LT': case'GT': case'EQ':
            case'AND': case'OR':  case'STRI2INT': case'CONCAT': case'GETCHAR':
            case'SETCHAR': case'JUMPIFEQ': case'JUMPIGNEQ':
                if($count == 3) return;
                else exit(23);
        }
    }

    //________CHECKING_TYPES_OF_INSTRUCTIONS_ARGUMENTS________
    function check_arg_type($splitted_line, $type, $arg_order){
        global $xml_out;

        switch($type){
            case(arg_types::variable):
                // LF|GF|TF + @ + alphabet + alphabet/number + $
                if( preg_match("/(LF|GF|TF)@[a-zA-Z$&%*!?_-][a-zA-Z0-9$&%*!?_-]*$/", $splitted_line[$arg_order]) ){
                    $xml_out .= "<arg$arg_order type=\"var\">$splitted_line[$arg_order]</arg$arg_order>";
                    break;
                }
                else exit(23); // syntax error - invalid type 'var'

            case(arg_types::label):
                if( preg_match("/^[a-zA-Z_$&%*!?-][a-zA-Z0-9_$&%*!?-]*$/", $splitted_line[$arg_order]) ){
                    $xml_out .= "<arg$arg_order type=\"label\">$splitted_line[$arg_order]</arg$arg_order>";
                    break;
                }
                else exit(23); // syntax error - invalid type 'label'

            case(arg_types::symb):
                // skipping the 'smth@' part of arguments
                $length = strlen(strstr($splitted_line[$arg_order], '@')); // length of @[smth]
                $argument = substr($splitted_line[$arg_order], strpos($splitted_line[$arg_order], "@")+1, $length);

                // frame + @ + sequence + $; /u for unicode chars occurance (hexa number)
                if(preg_match("/(LF|GF|TF)@[a-zA-Z$&%*!?_-]*$/u", $splitted_line[$arg_order])){
                    $xml_out .= "<arg$arg_order type=\"var\">$splitted_line[$arg_order]</arg$arg_order>";
                    break;
                }
                // string + @ + (sequence of chars except for es,#,\) + \[0-9]{3}(3 digits required)* + $
                if(preg_match("/string@([^\s\\\\#]|(\\\\[0-9]{3}))*$/u", $splitted_line[$arg_order])){
                    $argument = str_replace("&", "&amp;", $argument);
                    $argument = str_replace("<", "&lt;", $argument);
                    $argument = str_replace(">", "&gt;", $argument);
                    $argument = str_replace("\"", "&quot;", $argument);
                    $argument = str_replace("'", "&apos;", $argument);
                    $xml_out .= "<arg$arg_order type=\"string\">$argument</arg$arg_order>";
                    break;
                }
                // bool + @ + (true or false) + $ 
                if(preg_match("/bool@(true|false)$/u", $splitted_line[$arg_order])){
                    $xml_out .= "<arg$arg_order type=\"bool\">$argument</arg$arg_order>";
                    break;
                }
                // int + @ + (+ or -)(once or never) + (0-9)(more than once bcs sign without any number makes no sense) + $
                if(preg_match("/int@([+-]?[0-9]+)$/u", $splitted_line[$arg_order])){
                    $xml_out .= "<arg$arg_order type=\"int\">$argument</arg$arg_order>";
                    break;
                }
                // nil + @ + nil + $
                if(preg_match("/^nil@nil$/u", $splitted_line[$arg_order])){
                    $xml_out .= "<arg$arg_order type=\"nil\">$argument</arg$arg_order>";
                    break;
                }
                exit(23); // syntax error - invalid type 'symb'

            case(arg_types::type):
                // only 3 data types:string, int, bool + 1 special type:nil
                if(preg_match("/^(string|int|bool|nil)$/", $splitted_line[$arg_order])){
                    $xml_out .= "<arg$arg_order type=\"type\">$splitted_line[$arg_order]</arg$arg_order>";
                    break;
                }
                else
                    exit(23); // invalid type 'type'

            default:
                exit(23);
        }
    }

    //________INSTRUCTION_CONVERT_WITH_CHECKS________
    function instruction($splitted_line, $name, $count, $types, $instr_order){
        global $xml_out;
        $xml_out .= "<instruction order=\"$instr_order\" opcode=\"$name\">";

        //________ARGUMENTS_COUNT_CHECK________
        check_arg_count($count, $name);
        
        //________ARGUMENTS_TYPES_CHECK________
        if($types[0] && !$types[1] && !$types[2]){
            check_arg_type($splitted_line, $types[0], 1);
        }
        if($types[0] && $types[1] && !$types[2]){
            check_arg_type($splitted_line, $types[0], 1);
            check_arg_type($splitted_line, $types[1], 2);
        }
        if($types[0] && $types[1] && $types[2]){
            check_arg_type($splitted_line, $types[0], 1);
            check_arg_type($splitted_line, $types[1], 2);
            check_arg_type($splitted_line, $types[2], 3);
        }

        $xml_out .= "</instruction>";
    }

    //________INSTRUCTION_PARSER________
    function parse($splitted_line, $i){
        $count = count($splitted_line)-1; // instruction name is not part of arguments
        $name = strtoupper($splitted_line[0]); // strtoupper bcs case insensitive 
        $types = array(
            null, null, null,
        );

        switch($name){ 
            //________INSTRUCTIONS_WITH_ZERO_ARGUMENTS:________
            case "CREATEFRAME":
            case 'PUSHFRAME':
            case 'POPFRAME':
            case 'RETURN':
            case 'BREAK':
                instruction($splitted_line, $name, $count, $types, $i);
                break;

            //________INSTRUCTIONS_WITH_ONE_ARGUMENT:________
            case 'DEFVAR':  
            case 'POPS':    
                $types[0] = arg_types::variable; 
                instruction($splitted_line, $name, $count, $types, $i);
                break;
            case 'LABEL':   
            case 'JUMP':    
            case 'CALL':    
                $types[0] = arg_types::label;
                instruction($splitted_line, $name, $count, $types, $i);
                break;
            case 'PUSHS':   
            case 'WRITE':   
            case 'EXIT':    
            case 'DPRINT':  
                $types[0] = arg_types::symb;
                instruction($splitted_line, $name, $count, $types, $i);
                break;

            //________INSTRUCTIONS_WITH_TWO_ARGUMENTS:________
            case 'READ':
                $types[0] = arg_types::variable; 
                $types[1] = arg_types::type;
                instruction($splitted_line, $name, $count, $types, $i);
                break;
            case 'MOVE': 
            case 'INT2CHAR':
            case 'STRLEN':
            case 'TYPE':
            case 'NOT':
                $types[0] = arg_types::variable; 
                $types[1] = arg_types::symb;
                instruction($splitted_line, $name, $count, $types, $i);
                break;
                
            //________INSTRUCTIONS_WITH_THREE_ARGUMENTS:________
            case 'ADD':
            case 'SUB':
            case 'MUL':
            case 'IDIV':
            case 'LT':
            case 'GT':
            case 'EQ':
            case 'AND':
            case 'OR':
            case 'STRI2INT':    
            case 'CONCAT':
            case 'GETCHAR':
            case 'SETCHAR':
                $types[0] = arg_types::variable; 
                $types[1] = arg_types::symb; 
                $types[2] = arg_types::symb;
                instruction($splitted_line, $name, $count, $types, $i);
                break;
            case 'JUMPIFEQ':
            case 'JUMPIFNEQ':
                $types[0] = arg_types::label; 
                $types[1] = arg_types::symb; 
                $types[2] = arg_types::symb;
                instruction($splitted_line, $name, $count, $types, $i);
                break;

            default:
                echo "Unknown or wrong instruction!\n";
                exit(22); // unknown/invalid instruction error code             
        }
    }

    //________MAIN_PROGRAM________
    if($argc == 2){ // --help in cmd line
        if($argv[1] == '--help'){
            echo "This script parses the input source file into XML representation of code.\n";
            echo "Usage: parse.php [options] < <input file>\n";
            echo "    [options]: no options\n";
            exit(0);
        } else {
            echo "Wrong combination of parameters. Use 'parse.php --help' for more information.\n";
            exit(23);
        }
    }
    if($argc > 2){ // checking correct number of params on cmdl
        echo "More parameters than expected. Use 'parse.php --help' for more information.\n";
        exit(23);
    }    

    $head = false; // flag for .IPPcode22
    $i = 0; // opcode counter

    //________HANDLING_STDIN________
    while($line = fgets(STDIN)){ 
        if($line[0] == "#" || $line == "\n")
            continue;

        // skipping comment lines
        if(strpos($line, "#") != null)
        $line = substr($line, 0, strpos($line, '#')); // $line is non-commented substring of commented line    
            
        // trimming and splitting line into single strings
        $trimmed_line = trim($line, "\n");
        $trimmed_line = trim($trimmed_line);  

        if(!$head){     
            if(preg_match("/^.IPPcode22$/", $trimmed_line)){
                $xml_out = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; // version and encoding xml tag
                $xml_out .= "<program language=\"IPPcode22\">\n"; // .IPPcode22 xml tag
                $head = true;
                $i++;
                continue;
            }
            exit(21); // missing or invalid head error code
        }
        $splitted_line = preg_split("/\s+/", $trimmed_line);  
        
        // handling instructions of program
        parse($splitted_line, $i);
        $i++;
    }
       
    // active head allows closing tag of program
    if($head == true)
        $xml_out .= "</program>\n";

    //________XML_OUTPUT________
    echo "$xml_out";
?>
