/*  ppm.c
*   Řešení IJC-DU1, příklad b), 25.3.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*   Funkcia ppm_read otvori subor .ppm a nacita ho do danej struktury
*   Funkcia ppm_free uvolni pamat dynamicky alokovanu vo funkcii ppm_read
*/

#include "ppm.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "error.h"

struct ppm * ppm_read(const char * filename)
{
    FILE *file = fopen(filename, "rb");
    if(file == NULL)
    {
        warning_msg("Cannot open file.\n");
        fclose(file);
        return NULL;
    }

    char image_format[5];
    if(fgets(image_format, 4, file) == NULL) 
    {
        warning_msg("Missing image format.\n");
        fclose(file);
        return NULL;
    }

    //fprintf(stderr, "ppm.c: image format = '%s'\n", image_format);
    if( strcmp("P6\n" , image_format) != 0 )
    {
        warning_msg("Unsupported file format.\n");
        fclose(file);
        return NULL;
    }

    unsigned long image_x_size, image_y_size;
    int image_colour;
    if(fscanf(file, "%lu %lu\n%d\n", &image_x_size, &image_y_size, &image_colour) != 3)
    {
        warning_msg("Wrong file format.\n");
        fclose(file);
        return NULL;
    }
   // fprintf(stderr, "x: %lu, y:%lu, color:%d\n", image_x_size, image_y_size, image_colour);

    unsigned max_size_of_image = IMAGE_LIMIT;
    unsigned actual_size_of_image = 3*image_x_size*image_y_size;

    if(actual_size_of_image >= max_size_of_image)
    {
        warning_msg("Image size exceeded maximum.\n");
        fclose(file);
        return NULL;
    }

    struct ppm *image = calloc(1, 3*image_y_size*image_x_size*sizeof(char) + sizeof(struct ppm));
    if(image == NULL)
    {
        warning_msg("Memmory allocation failed.\n");
        fclose(file);
        return NULL;
    }

    image->xsize = image_x_size;
    image->ysize = image_y_size;

    int c;
    for(unsigned int i = 0; ( (c=fgetc(file)) != EOF ) && (i < actual_size_of_image) ; i++)
    {
        if(c != '\n')
        {
            image->data[i] = c;
        }
    }

    
    fclose(file);
    return image;
}

void ppm_free(struct ppm *p)
{
    free(p);
}