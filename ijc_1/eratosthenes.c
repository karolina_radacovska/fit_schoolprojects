/*  eratosthenes.c
*   Řešení IJC-DU1, příklad a), 25.3.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*   Funkcia Eratosthenes vyselektuje cisla v bitovom poli podla prvocisel    
*/

#include "eratosthenes.h"
#include <math.h>
#include <stdio.h>

void Eratosthenes(bitset_t pole)
{
    // Nulujeme bitové pole  p  o rozměru N, pole[0]=1, pole[1]=1 -> 0 a 1 nejsou prvočísla
    bitset_setbit(pole, 0, 1);
    bitset_setbit(pole, 1, 1);

    // podla definicie zadania zacinam od i=2 az po sqrt rozmeru
    for(unsigned long i = 2; i <= ceil(sqrt(bitset_size(pole))); i++) 
    {
        // pri najdeni prveho i-teho bitu rovneho 0 -> mam prvocislo
        // pre vsetky zvysne hodnoty nastavim 1 
        if((bitset_getbit(pole, i) == 0)) 
        {
            for(unsigned long j = (i+i); j < (bitset_size(pole)); j += i)
            {
		        bitset_setbit(pole, j, 1);
            }
        } 
    }

}