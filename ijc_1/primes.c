/*  primes.c
*   Řešení IJC-DU1, příklad a), 25.3.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*   Subor primes.c vyselektuje volanim funkcie Eratothenes cisla, ktore nie su prvocislami a poslednych 10 vypise
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "bitset.h"
#include "eratosthenes.h"
#include "error.h"

#define ARRAY_SIZE 200000000


int main()
{
    clock_t start = clock();

    bitset_alloc(b_array, ARRAY_SIZE);
    if( bitset_size(b_array) < 2 )
    {
        error_exit("(bitset_size:) There is no prime number in range of 0-%lu.", bitset_size(b_array));
    }

    Eratosthenes(b_array);
    
    unsigned long primes[10]; // 10 poslednych prvocisel

    int count = 0;
    unsigned long i = ARRAY_SIZE-1;
    while(i > 1)
    {
        // ak premenna i je prvocislo, jeho bit je nastaveny na 0
        if(bitset_getbit(b_array, i) == 0)
        {
            primes[count] = i;
            count++;
            if(count == 10)
                break;
        }
        i--;
    }

    // 10 poslednych prvocisel => indexy 9-0
    for(int k = 9; k >=0; k--)
    {   
        printf("%ld\n", primes[k]);
    }

    fprintf(stderr, "Time=%.3g\n", (double)(clock()-start)/CLOCKS_PER_SEC);
    free(b_array);
    return 0;
}
