/*  bit_array.h
*   Řešení IJC-DU1, příklad a), 25.3.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*   Hlavickovy subor definuje makra a inline funkcie   
*/

#ifndef BIT_ARRAY_H
#define BIT_ARRAY_H

#include <limits.h>
#include "error.h"
#include <assert.h>
#include <stdlib.h>

// typ bitoveho pola
typedef unsigned long *bitset_t;
// typ indexu do bitoveho pola
typedef unsigned long bitset_index_t;

//macro for number of bits in unsigned long int
#define UNSIGNED_BITS (sizeof(unsigned long) * CHAR_BIT)

//macro for rounding up if the remainder of division is greater than 0
#define DIV_ROUND_UP(a, b) (((a) + (b) - 1)/(b))

// definuje a -nuluje- premennu jmeno_pole
#define bitset_create(jmeno_pole,velikost) \
unsigned long jmeno_pole[DIV_ROUND_UP(velikost, UNSIGNED_BITS) + 1] = {velikost, 0}; \
static_assert(velikost > 0, "Size exceeded.\n");

// definuje premennu jmeno_pole tak, aby bola kompatibilna s polom vytvorenym pomocou bitset_create
// pole bude alokovane dynamicky
#define bitset_alloc(jmeno_pole, velikost) assert((velikost) > 0 && (velikost) <= (200000000)); \
bitset_t jmeno_pole = calloc((((velikost-1)/(8*sizeof(unsigned long)))+2), sizeof(unsigned long)); \
if(jmeno_pole == NULL) error_exit("(bitset_alloc:) Memmory not allocated.\n"); \
jmeno_pole[0] = (velikost);

#define bitset_free(jmeno_pole) free(jmeno_pole)

#ifndef USE_INLINE

    #define bitset_size(jmeno_pole) jmeno_pole[0] 

    #define bitset_setbit(jmeno_pole, index, vyraz) \
    (((bitset_index_t)(index) < (bitset_size(jmeno_pole))) ? \
	((vyraz == 0) ? \
	( jmeno_pole[ (index)/(sizeof(bitset_index_t)*CHAR_BIT)+1 ] &= ~((bitset_index_t)1 << ( (index)%(sizeof(bitset_index_t)*CHAR_BIT)) )) : \
	( jmeno_pole[ (index)/(sizeof(bitset_index_t)*CHAR_BIT)+1 ] |= ((bitset_index_t)1 << ( (index)%(sizeof(bitset_index_t)*CHAR_BIT))) )) \
	: (error_exit("(bitset_setbit:) Index %lu is not in range of 0-%lu\n", index, bitset_size(jmeno_pole)-1),0))


    #define bitset_getbit(jmeno_pole, index)\
    ((index >= bitset_size(jmeno_pole)) ?\
        error_exit("bitset_getbit: Index %lu mimo rozsah 0..%lu",(unsigned long)index, bitset_size(jmeno_pole)), 0\
    :\
    (bitset_index_t)((jmeno_pole[(index/(sizeof(bitset_index_t)*__CHAR_BIT__)+1)] & \
    ((bitset_index_t)1 << (index%(sizeof(bitset_index_t)*__CHAR_BIT__)))) > 0)) \
    
#endif

#ifdef USE_INLINE 
    static inline unsigned long bitset_size(bitset_t jmeno_pole) 
    { return jmeno_pole[0]; }

    static inline void bitset_setbit(bitset_t jmeno_pole, bitset_index_t index, bitset_index_t vyraz)
    {
        if(index >= bitset_size(jmeno_pole))
        {
            error_exit("(bitset_setbit:) Index %lu is not in range of 0-%lu.", (unsigned long)index, bitset_size(jmeno_pole));
        }
        if(vyraz)
        {
            jmeno_pole[ (index/CHAR_BIT*sizeof(bitset_index_t)) +1] |= ( (bitset_index_t)1 << (index % CHAR_BIT*sizeof(bitset_index_t)) );
        }
        else
        {
            jmeno_pole[ (index/CHAR_BIT*sizeof(bitset_index_t)) +1] &= ~( (bitset_index_t)1 << (index % CHAR_BIT*sizeof(bitset_index_t)) );
        }
    }

    static inline int bitset_getbit(bitset_t jmeno_pole, bitset_index_t index) {
        unsigned long position;
        unsigned long tmp;
        if(index >= jmeno_pole[0])
        {
            error_exit("bitset_getbit: Index %lu mimo rozsah 0..%lu.", (unsigned long)index, bitset_size(jmeno_pole));
            return -1;
        }
        position = 1;
        position <<= index % (sizeof(unsigned long) * CHAR_BIT);
        tmp = jmeno_pole[(index /(sizeof(unsigned long) * CHAR_BIT)) + 1];
        tmp &= position;
        if(tmp != 0)
        {
            return 1;
        }
        else
            return 0;
    }

#endif

#endif