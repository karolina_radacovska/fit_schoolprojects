/*  error.h
*   Řešení IJC-DU1, příklad b), 25.3.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*   Hlavickovy subor pre subor error.c
*/

#ifndef ERROR_H
#define ERROR_H

#include <stdarg.h>

void warning_msg(const char *fmt, ...);
void error_exit(const char *fmt, ...);

#endif 