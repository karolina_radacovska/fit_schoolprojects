/*  error.c
*   Řešení IJC-DU1, příklad b), 25.3.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*   Subor definuje funkcie warning_msg a error_exit
*/

#include "error.h"
#include <stdio.h>
#include <stdlib.h>

void warning_msg(const char *fmt, ...)
{
    fprintf(stderr, "CHYBA: "); 
    va_list args; 
    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    va_end(args);
}

void error_exit(const char *fmt, ...)
{
    warning_msg(fmt);
    exit(1);
}