/*  ppm.h
*   Řešení IJC-DU1, příklad b), 25.3.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*   Hlavickovy subor pre subor ppm.c
*/

#ifndef PPM_H
#define PPM_H

#define IMAGE_LIMIT 190000000

struct ppm {
    unsigned xsize;
    unsigned ysize;
    char data[];    
};

struct ppm * ppm_read(const char * filename);
void ppm_free(struct ppm *p);

#endif 