/*  bit_array.c
*   Řešení IJC-DU1, příklad a), 25.3.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*/

#include "bitset.h"

#ifdef USE_INLINE

unsigned long bitset_size(bitset_t jmeno_pole);
void bitset_setbit(bitset_t jmeno_pole, unsigned long index, bitset_index_t vyraz);
int bitset_getbit(bitset_t jmeno_pole, unsigned long index);

#endif