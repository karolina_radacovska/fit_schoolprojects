/*  eratothenes.h
*   Řešení IJC-DU1, příklad a), 25.3.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*   Hlavickovy subor pre subor eratosthenes.c
*/

#ifndef ERATOSTHENES_H
#define ERATOSTHENES_H

#include "bitset.h"

void Eratosthenes(bitset_t b_array);

#endif