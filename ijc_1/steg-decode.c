/*  steg-decode.c
*   Řešení IJC-DU1, příklad b), 25.3.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*	Subor vo funkcii main dekoduje obrazok a desifruje tajnu spravu
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bitset.h"
#include "error.h"
#include "ppm.h"
#include "eratosthenes.h"
#include <limits.h>

int main(int argc, char *argv[])
{
    if(argc != 2 )
    {
        error_exit("Wrong number of arguments.\n");
    }

	// premenna pre nazov suboru
	char image_name[30];
	strcpy(image_name, argv[1]);

	// nacitanie obrazku do struktury
    struct ppm *image;
    image = ppm_read(image_name);
    if(image == NULL)
    {
        error_exit("Image could not be loaded.\n");
    }
    
	// data z obrazky image su ulozene do Eratosthenovho pola
    unsigned long image_size = 3 * image->xsize * image->ysize;
	bitset_alloc(primes_array, image_size);
    Eratosthenes(primes_array);

	// premenna pre ulozenie spravy
    bitset_create(message, CHAR_BIT);
    unsigned long message_iterator = 0;

	// podla definicie v zadani kontrolujem byty od pozicie 23
    unsigned long i = 23; 
	while(i < image_size)
	{
		// kontrola bitov, ktore su 0 -> prvocisla
		if(bitset_getbit(primes_array, i) == 0)
		{	
			// image->data[i] & 1 =>  na najnizsich bitoch vybranych bitov je ulozena sprava
            bitset_setbit( message, message_iterator, (image->data[i] & 1) );
			message_iterator++;

			if(message_iterator == CHAR_BIT) // nacitana cela sprava
			{
				// sprava konci dosiahnutim '\0'
				if(message[1] == '\0')
				{
					printf("\n");
					bitset_free(primes_array);
					ppm_free(image);
					return 0;
				}
				else 
				{
					// "prime number winner"
					printf("%c", (char)message[1]);
				}
				message_iterator = 0; // zacinam novu spravu
			}
		}
		i++;
	}
	
	// uvolnim alokovanu pamat
	bitset_free(primes_array);
	ppm_free(image);
	
	// ak sa dostal program sem -> nenasiel koniec spravy = '\0'
	error_exit("Message is not ended with '\0'.\n");
}