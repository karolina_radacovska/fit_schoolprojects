/*  htab_clear.c
*   Řešení IJC-DU2, příklad b), 26.4.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*   Funkcia rusi vsetky zaznamy a uvolnuje pamat
*/


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "htab.h"
#include "htab_structures.h"

// ruší všechny záznamy
void htab_clear(htab_t * t)         
{
    // kontrola prazdnej tabulky
    if(t == NULL)
        return;

    int i;
    for(i = 0; i < t->arr_size; i++)
    {
        // vytvorenie docasneho ptr, ktory iteruje cez zoznamy
        htab_item_t *current = t->ptr[i];
        while( current !=  NULL ) // stop podmienka
        {   
            htab_item_t *tmp = current->next;
            // uvolnujem pamat postupne
            free((char*)current->item_pair->key);
            free(current->item_pair);
            free(current);    
            current = tmp;
        }
        t->ptr[i] = NULL;
    }
    // nulujem premenne
    t->arr_size = 0;
    t->size = 0;
}