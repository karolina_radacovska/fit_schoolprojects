/*
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "htab.h"
#include "htab_structures.h"

void fprintf_table(htab_t *tab)
{
    fprintf(stderr, "\n..fprintf table content..\n");

    if (tab == NULL)
    {
        fprintf(stderr, "..tab is NULL\n");
        return ;
    }

    fprintf(stderr, "..size:%d arr_size:%d\n", tab->size, tab->arr_size);
    fprintf(stderr, "..ptr content:\n");
    for(int i=0; i<tab->arr_size; i++)
    {
        fprintf(stderr, "..ptr[%d] (%p):\n", i, (void*)tab->ptr[i]);
        htab_item_t *current = tab->ptr[i];
        while (current != NULL)
        {
            fprintf(stderr, "....(%p):(%s,%d), next:(%p)\n", 
                (void*)current->item_pair ,current->item_pair->key, current->item_pair->value,
                (void *)current->next);
            current = current->next;
        }
    }
    fprintf(stderr, "..ptr done.\n");
    return ;
}

int main()
{
    htab_t *tab;
    tab = htab_init(10);
    //fprintf(stderr, "(%d)  size: %d   arr_size: %d\n", __LINE__, tab->size, tab->arr_size);
    
    char string[128] = "mnau";
    char *pstr = calloc(strlen(string)+1, sizeof(char));
    strcpy(pstr, string);
    htab_lookup_add(tab, pstr);
    //fprintf_table(tab);
    
    char string_2[128] = "hau";
    pstr = calloc(strlen(string_2)+1, sizeof(char));
    strcpy(pstr, string_2);
    htab_lookup_add(tab, pstr);
    //fprintf_table(tab);
    
    char string_3[128]= "zajo";
    pstr = calloc(strlen(string_3)+1, sizeof(char));
    strcpy(pstr, string_3);
    htab_lookup_add(tab, pstr);
    //fprintf_table(tab);

    char string_4[128] = "mnau";
    pstr = calloc(strlen(string_4)+1, sizeof(char));
    strcpy(pstr, string_4);
    htab_lookup_add(tab, pstr);
    fprintf_table(tab);

    htab_t *new_tab = htab_move(tab->arr_size, tab);
    
    fprintf(stderr, "TAB:\n");
    fprintf_table(tab);
    fprintf(stderr, "NEW_TAB:\n");
    fprintf_table(new_tab);

    //htab_lookup_add(new_tab, "kvik");
    //fprintf_table(tab);

    //fprintf(stderr, "(%d)  size: %d   arr_size: %d\n", __LINE__, tab->size, tab->arr_size);
    //htab_erase(tab, "hau");
    //fprintf_table(tab);
    //htab_erase(tab, "mnau");
    //fprintf_table(tab);
    //htab_erase(tab, "mnau");
    //fprintf_table(tab);

    htab_clear(tab);
    htab_free(tab);

    htab_clear(new_tab);
    htab_free(new_tab);
    return 0;
}
