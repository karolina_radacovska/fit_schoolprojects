/*  htab_init.c
*   Řešení IJC-DU2, příklad b), 26.4.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*   Funkcia inicializuje tabulku   
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "htab.h"
#include "htab_structures.h"

// konstruktor tabulky
htab_t *htab_init(size_t n)
{
    // alokacia pamate pre strukturu tabulky + miesto podla poctu arr_size
    htab_t *table = calloc(1, sizeof(htab_t) + n*(sizeof(htab_item_t)));
    // kontrola prazdnej tabulky    
    if(table == NULL)
    {
        fprintf(stderr, "Error: Table allocation failed.\n");
        return NULL;
    }

    // inicializacia tabulky
    table->arr_size = n;
    table->size = 0;

    for(size_t i=0; i < n ; i++ )
        table->ptr[i] = NULL; 

    return table;
}        