/*  htab_erase.c
*   Řešení IJC-DU2, příklad b), 26.4.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*   Funkcia rusi zadany zaznam key z tabulky t
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "htab.h"
#include "htab_structures.h"

// ruší zadaný záznam
bool htab_erase(htab_t * t, htab_key_t key)
{
    // najde zadany key
    htab_pair_t *pair_to_erase = htab_find(t, key);
    if(pair_to_erase == NULL)
        return false;

    // zistim index ptr array
    int index = (htab_hash_function(key) % (t->arr_size));

    // overim, ci hladany item_pair je prvy
    if( t->ptr[index]->item_pair == pair_to_erase)
    {
        // v pripade mazania key, ktory uz existuje znizim pocet vyskytov
        if(t->ptr[index]->item_pair->value > 1)
        {
            t->ptr[index]->item_pair->value--;
            return true;
        }

        // vytvorim docasnu premennu pre mazanie    
        htab_item_t *to_be_deleted = t->ptr[index];
        t->ptr[index] = t->ptr[index]->next;

        // postupne uvolnujem pamat
        free((char*)to_be_deleted->item_pair->key);
        free(to_be_deleted->item_pair);
        free(to_be_deleted);
        t->size--;

        return true;
    }

    // hladany prvok urcite nie je prvy -> prechadzam cely linked list
    htab_item_t *current = t->ptr[index]; // hlavicka linked list
    while(current != NULL)
    {
        // hladam, ktory zoznam ukazuje na moj hladany -> potrebny na vymazanie
        if( current->next->item_pair == pair_to_erase )
        {
            if(current->next->item_pair->value > 1)
            {
                current->next->item_pair->value--;
                break;
            }
            // slovo je v zozname len raz -> mazem cely item_pair
            htab_item_t *to_be_deleted = current->next;
            current->next = current->next->next;
            free(to_be_deleted->item_pair);
            free(to_be_deleted);
            t->size--;
            return true;
        }
        current = current->next; // posuvam sa dalej
    }
    return false;
}    