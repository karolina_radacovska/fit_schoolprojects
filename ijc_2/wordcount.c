/*  wordcount.c
*   Řešení IJC-DU2, příklad b), 26.4.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*   Implementacia vypoctu vyskytu znakov na vstupe
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "htab.h"
#include "htab_structures.h"
#include "io.h"

#define LIMIT 127
#define TAB_ARR_SIZE 10

void print_pair(htab_pair_t *data)
{
    //cout << mi.first << "\t" << mi.second << "\n";
    printf("%s\t%d\n", data->key, data->value);
}

int main()
{
    htab_t *tab = htab_init(TAB_ARR_SIZE);
    if(tab == NULL)
    {
        fprintf(stderr, "Error: Table allocation failed.\n");
        return EXIT_FAILURE;
    }

    char tmp_word[LIMIT] = ""; // tmp string
    int char_count=0; // tmp string length
    while ( (char_count = read_word(tmp_word, LIMIT, stdin)) != EOF )
    {
        // vytvorenie smernika pre key
        char *string = calloc(strlen(tmp_word)+1, sizeof(char));
        strcpy(string, tmp_word);
        
        // pridanie slova do tabulky
        htab_pair_t *pair = htab_lookup_add(tab, string);
        if(pair == NULL)
        {
            fprintf(stderr, "Error: Unable to create pair.\n");
            return EXIT_FAILURE;
        }
    }
    // vypis
    htab_for_each(tab, &print_pair);

// podmieneny preklad pre htab_move TEST    
#ifdef MOVETEST 
    htab_t *new_tab = htab_move(tab->arr_size, tab);
    
    if( new_tab == NULL )
    {
        fprintf(stderr, "Error: Table allocation failed.\n");
        return EXIT_FAILURE;
    }

    htab_for_each(new_tab, &print_pair);

    htab_clear(new_tab);
    htab_free(new_tab);

#endif

    // uvolnenie zdrojov
    htab_clear(tab);
    htab_free(tab);
    return EXIT_SUCCESS;
}