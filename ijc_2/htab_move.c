/*  htab_move.c
*   Řešení IJC-DU2, příklad b), 26.4.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*   Funkcia presuva data z tabulky from do novej tabulky
*   FUnkcia vrati novu tabulku, staru necha prazdnu a alokovanu
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "htab.h"
#include "htab_structures.h"

// přesun dat do nové tabulky
htab_t *htab_move(size_t n, htab_t *from)
{
    // alokacia novej tabulky
    htab_t *table_to;
    table_to = htab_init(n);
    table_to->size = from->size;

    // presun smernikov na data
    for(int i=0; i < from->arr_size; i++)
    {
        table_to->ptr[i] = from->ptr[i];
    }

    from->arr_size=0;
    from->size=0;

    return table_to;
}  