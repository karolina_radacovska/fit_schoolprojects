/*  htab_find.c
*   Řešení IJC-DU2, příklad b), 26.4.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*   Funkcia hlada zadany key v tabulke t
*   Funkcia vracia smernik na dvojicu item_pair ak zaznam najde, inak null
*/        

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "htab.h"
#include "htab_structures.h"

// hledání
htab_pair_t * htab_find(htab_t * t, htab_key_t key)
{
    // kontrola praznej tabulky
    if(t == NULL)
        return NULL;

    for (int i = 0; i < t->arr_size; i++) // prechadzam cez cele ptr
    {
        // do current ptr vlozim hlavicku linkovaneho zoznamu
        htab_item_t *current_ptr = t->ptr[i];
        while( current_ptr != NULL )
        {
            // ak najdem zhodu aktualneho key s mojim hladanym, vratim cely par
            if( !strncmp(current_ptr->item_pair->key, key, strlen(key)) )
                return ( current_ptr->item_pair );

            current_ptr = current_ptr->next;
        }
    }
    return NULL;      
}