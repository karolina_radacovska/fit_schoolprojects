/*  tail.c
*   Řešení IJC-DU2, příklad a), 26.4.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*   Implementacia vypisu riadkov zo suboru na stdout
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

#define LINE_CHAR_LIMIT 511

#define ERROR_MISSING_PARAMETER -1
#define ERROR_UNKNOWN_PARAMETER -2
#define ALLOCATION_ERROR -3
#define ERROR_FILE_DOES_NOT_EXIST -4

typedef char* string_t;
typedef struct {
    string_t* rows;
    int n_rows;
} lines_t;

typedef struct {
    bool n_parameter_provided; // poskytnuty parameter 'n' na cmd
    char n_sign; // znamienko
    int n_parameter; // 'n'
    bool filenames_prodived; // poskytnuty filename na cmd
    int filename_first_occurance_index; // index prveho filename na cmd
} tail_parameters;

// inicializacia parametrov z cmd
int init_cmd_parameters(tail_parameters *cmd_paramters)
{
    cmd_paramters->n_parameter_provided=false;
    cmd_paramters->n_sign='\0';
    cmd_paramters->n_parameter=10;          // default hodnota

    cmd_paramters->filenames_prodived=false;
    cmd_paramters->filename_first_occurance_index=0;

    return 0;
}

// funkcia vrati index znamienka pred parametrom
int find_digit_or_sign_in_string(const string_t string)
{
    // hladam znamienko -> na prvej pozicii je znak pre parameter, nie jeho hodnotu
    for(size_t i=1; i<strlen(string); i++)
        if((string[i]=='-') || (string[i]=='+'))
            return i;

    // ak najdem cislo, tu zacina moj parameter
    for(size_t i=0; i<strlen(string); i++)
        if(isdigit(string[i]))
            return i;
    
    return 0;
}

// funkcia prida jeden c nakoniec retazca string
int append_char(string_t *string, int c)
{
    // osetrenie prazdneho retazca
	int i=0;
	if (*string == NULL)
	{
		*string = calloc(2, sizeof(char)); // alokujem 1char + '\0' -> 2
        if(string==NULL)
        {
            fprintf(stderr, "%s: Allocation failed\n", __FILE__);
            return ALLOCATION_ERROR;
        }
		i=0;
	} 
	else // string obsahuje znaky
	{
		// realokujem pamat pre char c a pre '\0' -> 2
		*string = realloc(*string, (strlen(*string) + 2) * sizeof(char));
        if(string==NULL)
        {
            fprintf(stderr, "%s: Allocation failed\n", __FILE__);
            return ALLOCATION_ERROR;
        }
		i = strlen(*string); // dlzka retazca
	}
    
    // doplnenie stringu + '\0' na konci
	(*string)[i] = c;
	(*string)[i+1] = '\0';
    
	return 0;
}

// funkcia overi existenciu parametra -n na cmd
int check_n_lines_argument(int argc, char *argv[], tail_parameters *cmd_parameters, int *arg_index)
{
    // pred parametrom musi byt '-'
    if (argv[*arg_index][0]=='-')
    {
        // skontrolujem, ci bol zadany aj prepinac, alebo len znak '-' -> bez 'n'
        if(strlen(argv[*arg_index])==1)
        {
            fprintf(stderr, "%s: Wrong parameter\n", __FILE__);
            return ERROR_UNKNOWN_PARAMETER;
        }

        // hladam znak 'n' v stringu, pre rozpoznanie zadaneho parametra
        if(strstr(argv[*arg_index], "n"))
        {
            // bud je cislo sucastou tohto retazca alebo to bude dalsi retazec na cmd
            // overim, ci cislo je sucastou tohto retazca

            // hladam zaciatok cisla v retazci
            string_t n_string = NULL;   // NUMBER hodnota parametra (-n [NUMBER]) ako retazec (pre konverziu)
            int i_from=find_digit_or_sign_in_string(argv[*arg_index]); // parameter zacina znamienkom alebo cislicou
            for(size_t i=i_from; i<strlen(argv[*arg_index]); i++)
            {
                append_char(&n_string, (argv[*arg_index][i]));
            }

            // kontrola konverzie
            int int_parameter;
            char *rest=NULL;
            int_parameter=strtol(n_string, &rest, 10);
            if(!strcmp(rest, "")) // ak je zvysok po konverzii prazdny -> je to ok
            {
                // aktualizujem strukturu cmd parametrov:
                cmd_parameters->n_parameter_provided=true;
                cmd_parameters->n_parameter=abs(int_parameter);

                if(int_parameter<0)
                    cmd_parameters->n_sign='-';
                
                if(n_string[0]=='+')
                    cmd_parameters->n_sign='+';
         
                if(n_string!=NULL)
                {
                    free(n_string); n_string=NULL;
                }

                (*arg_index)++;
            }
            else // zvysok po konverzii nie je prazdny
            {
                // NUMBER pre parameter '-n' nebol sucastou toho isteho retazca

                // overim, ci je cislo dalsi retazec
                (*arg_index)++; // posuvam sa na dalsi argument na cmd
                if(*arg_index>=argc) // existuje dalsi argument?
                {
                    if(n_string!=NULL)
                    {
                        free(n_string); 
                        n_string=NULL;
                    }
                    return ERROR_MISSING_PARAMETER;
                }
            
                int_parameter=strtol(argv[*arg_index], &rest, 10);
                if(!strcmp(rest, ""))
                {
                    // aktualizujem strukturu cmd parametrov:
                    cmd_parameters->n_parameter_provided=true;
                    cmd_parameters->n_parameter=abs(int_parameter);

                    if(int_parameter<0)
                        cmd_parameters->n_sign='-';
                    
                    if(argv[*arg_index][0]=='+')
                        cmd_parameters->n_sign='+';

                    // posuniem index o poziciu, kedze som tuto uz sprocesovala
                    (*arg_index)++; // v pripade, ze je dalsi cmd arg filename -> musim vediet

                    if(n_string!=NULL)
                    {
                        free(n_string); n_string=NULL;
                    }
                    return 0;
                }
                else
                {
                    // tu bola ocakavana hodnota pre '-n' (ako hned nasledujuci parameter po -n)
                    // kedze konverzia zlyhala, vraciam chybovu hlasku
                    (*arg_index)++;
                    return ERROR_MISSING_PARAMETER;
                }
            }
        }
    }
    return ERROR_MISSING_PARAMETER;
}

// kontrola argumentov na cmd
int check_cmd_parameters(int argc, char *argv[], tail_parameters *cmd_parameters)
{
    // nemam ziadny parameter na cmd
    if (argc == 1) 
    {
        return ERROR_MISSING_PARAMETER;
    }
    
    // argc > 1
    // tzn. bud mam pritomny prepinac '-n' alebo zacinaju nazvy suborov
    int arg_index=1;

    // kontrolujem, ci mam pritomny rozlisovac prepinaca '-'
    switch(check_n_lines_argument(argc, argv, cmd_parameters, &arg_index))
    {
        case ERROR_UNKNOWN_PARAMETER:
            return ERROR_UNKNOWN_PARAMETER;
        case ALLOCATION_ERROR:
            return ALLOCATION_ERROR;
    }

    // ak sa parameter '-n' na cmd nenachadza, arg_index ostal nezmeneny
    // a teda ocakavam iba vstupne subory
    if (arg_index<=argc-1)
    {
        cmd_parameters->filename_first_occurance_index=arg_index;
        cmd_parameters->filenames_prodived=true;
    }

    return 0;
}

// tvorba celeho textu
// funkcia prida jeden riadok k uz existujucemu textu
int append_line(lines_t *lines, string_t string)
{
    int current_n_rows = lines->n_rows; // sucasny pocet riadkov

    // ak su riadky prazdne => text este nie je nacitany
    if(lines->rows == NULL)
    {
        // vytvorim smernik pre 1.riadok
        lines->rows = calloc(strlen(string)+1, sizeof(string_t));
        if(lines->rows==NULL)
        {
            fprintf(stderr, "%s: Allocation failed\n", __FILE__);
            return ALLOCATION_ERROR;
        }
    }
    else // riadky uz existuju -> obsahuju text
    {
        // smernik pre 1 dalsi riadok
        lines->rows = realloc(lines->rows, (current_n_rows + 1)*sizeof(string_t)); // +1 pre '\0'
        if(lines->rows==NULL)
        {
            fprintf(stderr, "%s: Allocation failed\n", __FILE__);
            return ALLOCATION_ERROR;
        }
    }

    // miesto pre novy riadok - string
    lines->rows[current_n_rows] = calloc(strlen(string)+1, sizeof(char));
    if(lines->rows[current_n_rows]==NULL)
    {
        fprintf(stderr, "%s: Allocation failed\n", __FILE__);
        return ALLOCATION_ERROR;
    }
    // miesto pre novy riadok je vytvorene -> kopirujem tam text
    strcpy((lines)->rows[current_n_rows], string);

    lines->n_rows++; // zvysim pocet riadkov
    return 0;
}

// funkcia nacita text z fp na filename z cmd a jeho obsah vlozi do all_lines
int load_text_from_file(lines_t *all_lines, FILE *fp)
{
    // init
    all_lines->n_rows=0;
    all_lines->rows=NULL;

    int c;
    if(fp==NULL)
    {
        return ERROR_FILE_DOES_NOT_EXIST;
    }

    string_t single_line = NULL;
    int char_count=0; // counter znakov v riadku
    while ( (c=fgetc(fp)) != EOF) 
    {
        char_count++;

        // kontrola, ci nenacitavam viac ako LINE_CHAR_LIMIT znakov
        if(char_count > LINE_CHAR_LIMIT)
        {
            // ignorovanie zvysku riadku az po znak '\n' alebo EOF
            while( (c=getc(fp)) != '\n')
            {
                if(c==EOF)
                {
                    break;
                }
            } 
        }

        append_char(&single_line, c);

        if( (c == '\n') || (c == EOF) )
        {
            append_line(all_lines, single_line);
            char_count = 0;

            if(single_line != NULL)
            {
                free(single_line);
                single_line = NULL;
            }
        }
    }

    if(single_line != NULL)
    {
        // single_line este obsahuje data z posledneho riadku, ktore neboli pridane 
        append_line(all_lines, single_line);

        free(single_line);
        single_line = NULL;
    }

    return 0;
}

// funkcia nacita text zo stdin do all_lines
int load_text_from_stdin(lines_t *all_lines)
{
    all_lines->n_rows=0;
    all_lines->rows=NULL;

    int c;

    string_t single_line = NULL;
    int char_count=0;
    while ( (c=getchar()) != EOF)
    {
        char_count++;

        // kontrola, ci nenacitavam viac ako LINE_CHAR_LIMIT znakov
        if(char_count > LINE_CHAR_LIMIT)
        {
            fprintf(stderr, "Error: Limit size of line num.%d exceeded, the rest of line is ignored (max:%d)\n", all_lines->n_rows, LINE_CHAR_LIMIT);
            
            // vycistenie riadku az po znak \n
            while( (c=getchar()) != '\n' )
            {
                if(c==EOF)
                {
                    break;
                }
            }    
        }

        append_char(&single_line, c);

        if( (c == '\n') || (c==EOF) )
        {
            append_line(all_lines, single_line);
            char_count = 0;

            if(single_line != NULL)
            {
                free(single_line);
                single_line = NULL;
            }
        }
    }

    if(single_line != NULL)
    {
        // ak single_line nie je NULL, stale drzi udaje z posledneho riadku
        append_line(all_lines, single_line);

        free(single_line);
        single_line = NULL;
    }

    return 0;
}

// pomocna funkcia
/*
void fprinf_all_lines(lines_t *text)
{
    if(text == NULL)
        return;

    fprintf(stderr, ".printing all lines (%d):\n", text->n_rows);
    for(int i=0; i < text->n_rows; i++)
    {
        fprintf(stderr, "%d: ", i);
        fprintf(stderr, "'%s' , len=%lu\n", text->rows[i], strlen(text->rows[i]));
    }
}
*/

// funkcia uvolni pamat
int clear_and_free_lines(lines_t *text)
{
    // neexistuje ziaden riadok
    if(text->rows==NULL)
    {
        text->n_rows=0;
        return 0;
    }

    for(int i=0; i < text->n_rows; i++)
    {
        free(text->rows[i]);
        text->rows[i]=NULL;
    }
    free(text->rows);
    text->rows=NULL;
    return 0;
}

// funkcia vytlaci riadky na stdout
int print_last_lines(lines_t text, int n_last_lines, char plus_sign)
{
    // riadok, od ktoreho sa ma zacat vypisovat
    int line_from = text.n_rows - n_last_lines;

    // znamienko '+' meni logiku -> tlacim od indexu n_last_lines - 1 az dokonca
    if(plus_sign == '+')
    {
        line_from = n_last_lines - 1;
    }

    // kontrola, ze parameter na cmd nie je vacsi ako skutocny pocet riadkov v subore
    if(line_from < 0)
    {
        line_from = 0;
    }

    for(int i=line_from; i<text.n_rows; i++)
    {
        for(int j=0; text.rows[i][j] != '\0'; j++)
        {
            // kontrola "tlacitelneho" znaku
            if (isprint(text.rows[i][j]))
            {
                printf("%c", text.rows[i][j]);
            }
            else
            {
                switch (text.rows[i][j])
                {
                case '\n':
                    printf("\n");
                    break;
                
                default:
                    // ak nepoznam charakter, vytlacim prazdny retazec (napr EOF)
                    printf("%s", "");
                    break;
                }
            }
        }
    }
    return 0;
}

int main(int argc, char **argv)
{
    tail_parameters cmd_paramters; // parametre na cmd
    init_cmd_parameters(&cmd_paramters); 
    
    // vyhodnocovanie vstupnych parametrov
    check_cmd_parameters(argc, argv, &cmd_paramters);

    // ak existuju na cmd filenames
    if(cmd_paramters.filenames_prodived)
    {
        // prechadzam cez vsetky filenames poskytnutych na cmd
        for(int index_arg = cmd_paramters.filename_first_occurance_index; index_arg<argc; index_arg++)
        {
            // ak je na cmd viac filenames, vypisem aj nazov kazdeho z nich
            if(cmd_paramters.filename_first_occurance_index < argc-1)
            {
                // pred kazdym dalsim spracuvanym suborom je prazdny riadok
                if(index_arg != cmd_paramters.filename_first_occurance_index)    
                    printf("\n");
                
                printf("==> %s <==\n", argv[index_arg]); // podla tailu
            }

            FILE *fp = fopen (argv[index_arg], "r");        
            if (fp==NULL)
            {
                fprintf(stderr, "Error: File %s cannot be opened.\n", argv[index_arg]);
                continue;
            }

            lines_t text;
            // nacitam subor do line_t *
            if(load_text_from_file(&text, fp))
            {
                fprintf(stderr, "Error: File %s cannot be opened.\n",argv[index_arg]);
                continue;
            }
            // zatvorenie suboru
            if (fp != NULL)
            {
                fclose(fp);
            }

            print_last_lines(text, cmd_paramters.n_parameter, cmd_paramters.n_sign);
            if (text.rows != NULL)
            {
                clear_and_free_lines(&text);
            }
        }
    }
    else // na cmd nie su ziadne filenames -> nacitavam zo stdin
    {
        lines_t text;
        //fprinf_all_lines(&text);
        load_text_from_stdin(&text);
        print_last_lines(text, cmd_paramters.n_parameter, cmd_paramters.n_sign);
        if (text.rows != NULL)
        {
            clear_and_free_lines(&text);
        }
    }
    
    return 0;
}