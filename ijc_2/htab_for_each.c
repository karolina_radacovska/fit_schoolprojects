/*  htab_for_each.c
*   Řešení IJC-DU2, příklad b), 26.4.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*   Funkcia prejde zaznamy tabulky t a zavola na ne funkciu f  
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "htab.h"
#include "htab_structures.h"

// for_each: projde všechny záznamy a zavolá na ně funkci f
void htab_for_each(const htab_t * t, void (*f)(htab_pair_t *data))
{
    // kontrola prazdnej tabulky
    if (t == NULL)
        return ;

    // prechadzanie ptr
    for(int i=0; i<t->arr_size; i++)
    {
        // do docasnej premennej urcenej na iteraciu vlozim hlavicku linkovaneho zoznamu
        htab_item_t *current = t->ptr[i];
        while (current != NULL)
        {
            // ako data vratim pair
            (*f)((void*)current->item_pair);
            current = current->next;
        }
    }
}