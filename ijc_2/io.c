/*  io.c
*   Řešení IJC-DU2, příklad b), 26.4.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*   Funkcia nacita slovo zo suboru f o maximalnej dlzke max
*   Funkcia vrati pocet znakov alebo EOF ak narazi na koniec suboru
*/

#include "io.h"

int read_word(char *s, int max, FILE *f)
{
    // kotrola prazdneho suboru
    if(f == NULL)
        return EOF;

    int c; // znak, do ktoreho priebezne nacitavam znaky zo suboru f
    int i=0; // iterator znakov
    s[0] = '\0';
    while( (c=fgetc(f)) != EOF )
    {
        // ak je na prvom (nultom) indexe white space
        if( (isspace(c)) && (i==0) )
        {
            continue;
        }
        if(isspace(c)) // koniec slova
        {
            break;
        }
        
        s[i] = c; // c je sucast stringu
        s[i+1] = '\0';
        i++;

        // kontrola max limit size
        if(i >= max)
        {
            // dosiahnuty limit na nacitanie slova
            fprintf(stderr, "Warning: Word max size limit reached.\n");      
            while ( ((c=fgetc(f)) != EOF) && isspace(c) ) 
            { 
                // Ignoring char c
            }
            break;  
        }
    }
    if(c == EOF)
        return EOF;
    
    return i; // pocet znakov

}