/*  htab_hash_function.c
*   Řešení IJC-DU2, příklad b), 26.4.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*   Funkcia vypocita hash pre dany key  
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "htab.h"
#include "htab_structures.h"

// Rozptylovací (hash) funkce (stejná pro všechny tabulky v programu)
// Pokud si v programu definujete stejnou funkci, použije se ta vaše.
size_t htab_hash_function(htab_key_t str)
{
#ifndef HASHTEST // TEST pre hashovaciu funkciu   

    uint32_t h=0;     // musí mít 32 bitů
    const unsigned char *p;
    for(p=(const unsigned char*)str; *p!='\0'; p++)
        h = 65599*h + *p;
    return h;

// djb2 implementacia hashovacej funkcie zo zdroja http://www.cse.yorku.ca/~oz/hash.html
#else 
    uint32_t hash = 5381;
    int c;
    while ( (c = (*str++)) )
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    return hash;
#endif
}