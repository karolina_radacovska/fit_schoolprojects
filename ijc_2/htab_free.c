/*  htab_free.c
*   Řešení IJC-DU2, příklad b), 26.4.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*   Funkcia vycisti tabulku t a uvolni zdroje  
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "htab.h"
#include "htab_structures.h"

// destruktor tabulky
void htab_free(htab_t * t)
{
    if(t != NULL)
    {
        if(t->ptr != NULL) // ak v tabulke su zaznamy...
        {
            htab_clear(t); // ...vycistim ju...
        }
        free(t); // ... a uvolnim pamat
    }
}  