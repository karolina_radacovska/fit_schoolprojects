/*  io.h
*   Řešení IJC-DU2, příklad b), 26.4.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*   Hlavickovy subor pre funkciu read_word
*/

#ifndef IO_H
#define IO_H

#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>

int read_word(char *s, int max, FILE *f);

#endif