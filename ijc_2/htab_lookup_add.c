/*  htab_lookup_add.c
*   Řešení IJC-DU2, příklad b), 26.4.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*   Funkcia prida key do tabulky t, ak existuje, zvysi jeho value
*   Funkcia vracia smernik na item_pair   
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "htab.h"
#include "htab_structures.h"

// vyhladavanie + pridanie
htab_pair_t * htab_lookup_add(htab_t * t, htab_key_t key)
{
    // najdem key, ktory chcem pridat do tabulky
    htab_pair_t *pair = htab_find(t, key);
    
    if(pair != NULL)    
    {
        // dany key uz v tabulke nachadza -> zvysim value
        (pair->value)++; 
        // dany key uz nepotrebujem 2x -> uvolnim pamat 
        free((char*)key);

        return pair;
    }

    // dany key sa v tabulke nenachadza

    // vypocitam na aky index mam nove slovo vlozit
    int index = (htab_hash_function(key) % (t->arr_size));
    
    // vytvaram pamat pre novy pair_item
    htab_item_t *new_item = calloc(1, sizeof(htab_item_t));
    new_item->item_pair = calloc(1, sizeof(htab_pair_t));
    
    new_item->item_pair->key = key;
    new_item->item_pair->value = 1; 
    new_item->next = NULL;

    // na index priradim dany pair (key+value)
    new_item->next = t->ptr[index];
    t->ptr[index] = new_item;

    (t->size)++;

    return (new_item->item_pair);
}