/*  htab_structures.h
*   Řešení IJC-DU2, příklad b), 26.4.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*   Hlavickovy subor pre tabulkove struktury
*/

#ifndef HTAB_STRUCTURES_H
#define HTAB_STRUCTURES_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct htab_item 
{
    // key
    // value
    htab_pair_t *item_pair;
    // next
    struct htab_item* next;
} htab_item_t;

struct htab
{
    int size; // counter zaznamov vo vsetkych ptr
    int arr_size; // counter ptr-ov
    htab_item_t *ptr[]; 
};

#endif //HTAB_STRUCTURES_H