/*  htab_bucket_count.c
*   Řešení IJC-DU2, příklad b), 26.4.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*   Funkcia vracia velkost pola smernikov
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "htab.h"
#include "htab_structures.h"

// velikost pole
size_t htab_bucket_count(const htab_t * t)
{
    return (t->arr_size);
} 