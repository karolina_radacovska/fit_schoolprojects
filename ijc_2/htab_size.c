/*  htab_size.c
*   Řešení IJC-DU2, příklad b), 26.4.2021
*   Autor: Karolina Radacovska, FIT
*   Přeloženo: gcc 9.3.0
*
*   Funkcia vrati pocet zaznamov  v tabulke
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "htab.h"
#include "htab_structures.h"

// počet záznamů v tabulce
size_t htab_size(const htab_t * t)
{
    return (t->size);
}