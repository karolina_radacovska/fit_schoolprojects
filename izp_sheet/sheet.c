/**************************************************/
/* * *               sheet.c                  * * */
/* * *                                        * * */
/* * *          semestralny projekt           * * */
/* * *                                        * * */
/* * *          Karolina Radacovska           * * */
/* * *               xradac00                 * * */
/* * *             November 2020              * * */
/**************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <math.h>

// own error return codes
#define ERROR_EARLIER_END_PROGRAM 1
#define ERROR_NO_DELIMINATOR_FOUND -1
#define ERROR_UKNOWN_COLUMN -2
#define ERROR_CONVERSION_FAILED -3
#define ERROR_ROWS_ARGUMENT_NOT_FOUND -4
#define ERROR_MISSING_ARGUMENT -5
#define MAX_SIZE_COL 100
#define MAX_SIZE_ROW  10240
#define NO_NUMBER_ARGUMENT -10
#define REQUESTED_LAST_ROW -12

/**
* Own structure for storing commnads and their parameters from command line
* If command is set, approriate variable is set true
*/
typedef struct _commands {
    bool irow;
	int irow_r;
    bool arow;
    bool drow;
	int drow_r;
    bool drows;
	int drows_n, drows_m;
    bool icol;
	int icol_c;
    bool acol;
    bool dcol;
	int dcol_c;
    bool dcols;
	int dcols_n, dcols_m;

    bool cset;
	int cset_c;
	char cset_str[MAX_SIZE_COL];
    bool tolower;
	int tolower_c;
    bool toupper;
	int toupper_c;
    bool round;
	int round_c;
    bool make_int;
	int make_int_c;
    bool copy;
	int copy_n, copy_m;
    bool swap;
	int swap_n, swap_m;
    bool move;
	int move_n, move_m;

	bool rows;
	int rows_n, rows_m;
	bool beginswith;
	int beginswith_c;
	char beginswith_str[MAX_SIZE_COL];
	bool contains;
	int contains_c;
	char contains_str[MAX_SIZE_COL];

} Commands;

// own functions for processing command line arguments
int get_value_from_argument(char *argv[], int i);
int get_value_from_argument_for_rows(char *argv[], int i, Commands *cmds);
int get_string_from_argument(char *argv[], int i, char string[]);
int parse_arguments(int argc, char *argv[], Commands *cmds);
int parse_deliminator(int argc, char *argv[], char delim[]);

// dedicated function for reading one single row from stdin
int read_one_row(char buffer[]);

// functions for data processing
void print_empty_row(char delim_char, int cols);
int insert_char_at_position(char buffer[], char delim_char, int position);
int insert_new_column_at_position(char buffer[], char delim[], int col_pos);
int delete_char_at_position(char char_array[], int pos);
int delete_column_at_position(char buffer[], char delim[], int column);
int delete_columns(char buffer[], char delim[], int n, int m0);
int insert_substring_in_string_at_position(char buffer[], char substr[], int position);
int get_total_columns(char buffer[], char delim[]);
int set_value_to_column(char buffer[], char delim[], int col, char col_str[]);
int get_value_from_column(char buffer[], char delim[], int col, char col_value[]);
int set_column_tolower(char buffer[], char delim[], int col);
int set_column_toupper(char buffer[], char delim[], int col);
float my_pow_10(int exponent);
int float_to_string(float f, char string[]);
float my_round(float f);
int set_column_round(char buffer[], char delim[], int col);
int set_column_int(char buffer[], char delim[], int col);
int process_data_in_row(char buffer[], char delim[], Commands cmds);

// functions for conditional selecting of rows
bool is_row_selected(int cur_row, int n, int m, bool flag_last_line    );
bool does_col_beginswith(char buffer[], char delim[], Commands cmds);
bool does_col_contains(char buffer[], char delim[], Commands cmds);

// main function ends with 0 if success and with 1 if error
int main(int argc, char *argv[])
{
    // all commands from command line
    Commands cmds;

    // array of chars for column separators
    char delim[MAX_SIZE_COL] = " ";

    // reading data from command line to set the structure up and delim
    parse_arguments(argc, argv, &cmds);
    parse_deliminator(argc, argv, delim);

    // buffer keeps current row from stdin
    // next_buffer keeps the very next line, next_buffer is for treating the last line from stdin
	char buffer[MAX_SIZE_ROW], next_buffer[MAX_SIZE_ROW];

    // total_cols, rows, length depends on each read buffer
    int total_cols = 0;
    int rows = 0;
    int length;
    bool flag_last_line = false;

    // infinite loop is to make sure we get throught all lines
    // for selecting rows we need to know row number (rows)
    // to figure out the last row we compare 2 rows
    // if next_buffer is EOF => buffer is the last row
    length = read_one_row(next_buffer);
    if(length == ERROR_EARLIER_END_PROGRAM)
    {
        return ERROR_EARLIER_END_PROGRAM;
    }

	while(true)
    {
        // buffer is our current row we are processing
        strcpy(buffer, next_buffer);
        total_cols = get_total_columns(buffer, delim);

        // next_buffer is our next row, to be checked for EOF
        length = read_one_row(next_buffer);
        if(length == ERROR_EARLIER_END_PROGRAM)
        {
            return ERROR_EARLIER_END_PROGRAM;
        }

        if(length == EOF)
            flag_last_line = true;

        // each new line will increment rows
        rows++;


        // based on command line arguments stored in cmds structure we process each line
        // in case of error returning value 1
		if(cmds.rows)
		{
            // handling wrong arguments errors and earlier programme ending
            if((cmds.rows_n == NO_NUMBER_ARGUMENT) || (cmds.rows_n == ERROR_MISSING_ARGUMENT))
            {
                fprintf(stderr, "rows n - wrong argument\n");
                return ERROR_EARLIER_END_PROGRAM;
            }
            if((cmds.rows_m == NO_NUMBER_ARGUMENT) || (cmds.rows_m == ERROR_MISSING_ARGUMENT))
            {
                fprintf(stderr, "rows m - wrong argument\n");
                return ERROR_EARLIER_END_PROGRAM;
            }

            // calling function is_row_selected to select the row to be processed
			if(is_row_selected(rows, cmds.rows_n ,cmds.rows_m, flag_last_line))
			{
                int error_code;
               	error_code = process_data_in_row(buffer, delim, cmds);
                if(error_code == NO_NUMBER_ARGUMENT)
                {
                    fprintf(stderr, "missing number argument\n");
                    return ERROR_EARLIER_END_PROGRAM;
                }
            }
		}

		if(cmds.beginswith)
		{
            if((cmds.beginswith_c == NO_NUMBER_ARGUMENT) || (cmds.beginswith_c < 1))
            {
                fprintf(stderr, "beginswith c - wrong argument\n");
                return ERROR_EARLIER_END_PROGRAM;
            }

            // calling function to check if column begins with required string stored in cmds structure
			if(does_col_beginswith(buffer, delim, cmds))
			{
				// begin with option
				process_data_in_row(buffer, delim, cmds);
			}
		}

        // handling cells which contains string
		if(cmds.contains)
		{
            if((cmds.contains_c == NO_NUMBER_ARGUMENT) || (cmds.contains_c < 1))
            {
                fprintf(stderr, "contains c - wrong argument\n");
                return ERROR_EARLIER_END_PROGRAM;
            }

			// checking for column
			if(does_col_contains(buffer, delim, cmds))
			{
				// row selected - col contains
				process_data_in_row(buffer, delim, cmds);
			}
		}

        // previous three options selected rows based on criteria
        // these options process rows if no such criteria does exist
        if ((!cmds.rows) && (!cmds.beginswith) && (!cmds.contains))
		{
			// error_code to control earlier program end
            int error_code;
           	error_code = process_data_in_row(buffer, delim, cmds);
            if(error_code == NO_NUMBER_ARGUMENT)
            {
                // missing number argument
                return ERROR_EARLIER_END_PROGRAM;
            }
		}

        // option for adding column at the end of line
        // => appending delim at the end of string
		if(cmds.acol)
		{
			insert_char_at_position(buffer, delim[0], strlen(buffer));
		}

        // treating inserting new column in each row
		if(cmds.icol)
		{
            if((cmds.icol_c == NO_NUMBER_ARGUMENT) || (cmds.icol_c < 1))
            {
                fprintf(stderr, "icol c - wrong argument\n");
                return ERROR_EARLIER_END_PROGRAM;
            }
			insert_new_column_at_position(buffer, delim, cmds.icol_c);
		}

		// options for deleting column(s)
        if(cmds.dcol)
        {
            if((cmds.dcol_c == NO_NUMBER_ARGUMENT) || (cmds.dcol_c < 1))
            {
                fprintf(stderr, "dcol c - wrong argument\n");
                return ERROR_EARLIER_END_PROGRAM;
            }
			delete_column_at_position(buffer, delim, cmds.dcol_c);
        }
		if(cmds.dcols)
		{
            if((cmds.dcols_n == NO_NUMBER_ARGUMENT) || (cmds.dcols_n < 1))
            {
                fprintf(stderr, "dcols n - wrong argument\n");
                return ERROR_EARLIER_END_PROGRAM;
            }
            if(cmds.dcols_m == NO_NUMBER_ARGUMENT   )
            {
                fprintf(stderr, "dcols m - wrong argument\n");
                return ERROR_EARLIER_END_PROGRAM;
            }

			delete_columns(buffer, delim, cmds.dcols_n, cmds.dcols_m);
		}

        // option for irow R
		if(cmds.irow)
		{
            if(cmds.irow_r == NO_NUMBER_ARGUMENT)
            {
                fprintf(stderr, "irow r - wrong argument\n");
                return ERROR_EARLIER_END_PROGRAM;
            }
			if(rows == cmds.irow_r)
			{
				// found a place for new row
                print_empty_row(delim[0], total_cols);
			}
		}

        // option for drow R and drows N M
		if(cmds.drow)
		{
            if(cmds.drow_r == NO_NUMBER_ARGUMENT)
            {
                fprintf(stderr, "drow r - wrong argument\n");
                return ERROR_EARLIER_END_PROGRAM;
            }
			if (rows == cmds.drow_r)
				continue;
		}
		if(cmds.drows)
		{
            if((cmds.drows_n = NO_NUMBER_ARGUMENT) || (cmds.drows_n < 1))
            {
                fprintf(stderr, "drows n - wrong argument\n");
                return ERROR_EARLIER_END_PROGRAM;
            }
            if(cmds.drows_m == NO_NUMBER_ARGUMENT)
            {
                fprintf(stderr, "drows m - wrong argument\n");
                return ERROR_EARLIER_END_PROGRAM;
            }
			if((cmds.drows_n <= rows) && (rows <= cmds.drows_m))
			{
				continue;
			}
		}

        // printing processed data in row to stdout
        printf("%s\n", buffer);

        // stop condition
        if(length == EOF)
            break;
    }

    // adding empty row at the end of processed data
	if(cmds.arow)
		print_empty_row(delim[0], total_cols);

    return 0;
}

// function returns integer number from argument argv[i]
int get_value_from_argument(char *argv[], int i)
{
	char *end; // the rest after strtod conversion
	int tmp = NO_NUMBER_ARGUMENT; // expecting there is no valid argument
	if(argv[i] != NULL)
	{
		tmp = strtod(argv[i], &end);
		if(strlen(end) == 0) // if string "end" is empty the whole string was converted
			return tmp;
	}
	return NO_NUMBER_ARGUMENT;
}

// function will copy argument  argv[i] into the string
int get_string_from_argument(char * argv[], int i, char string[])
{
    if(argv[i] == NULL)
    {
        return ERROR_MISSING_ARGUMENT;
    }

	strcpy(string, argv[i]);
	return 0;
}

// function parses arguments from comand line and storing details in structure "cmds"
int parse_arguments(int argc, char *argv[], Commands *cmds)
{
    // assuming there is no argument on command line
    // setting default values in all structure items
    cmds->irow=false;
	cmds->irow_r=NO_NUMBER_ARGUMENT;
    cmds->arow=false;
    cmds->drow=false;
	cmds->drow_r=NO_NUMBER_ARGUMENT;
    cmds->drows=false;
	cmds->drows_n=NO_NUMBER_ARGUMENT;
	cmds->drows_m=NO_NUMBER_ARGUMENT;
    cmds->icol=false;
	cmds->icol_c=NO_NUMBER_ARGUMENT;
    cmds->acol=false;
    cmds->dcol=false;
	cmds->dcol_c=NO_NUMBER_ARGUMENT;
    cmds->dcols=false;
	cmds->dcols_n=NO_NUMBER_ARGUMENT;
	cmds->dcols_m=NO_NUMBER_ARGUMENT;

    cmds->cset=false;
	cmds->cset_c=NO_NUMBER_ARGUMENT;
	strcpy(cmds->cset_str, "");
    cmds->tolower=false;
	cmds->tolower_c=NO_NUMBER_ARGUMENT;
    cmds->toupper=false;
	cmds->toupper_c=NO_NUMBER_ARGUMENT;
    cmds->round=false;
	cmds->round_c=NO_NUMBER_ARGUMENT;
    cmds->make_int=false;
	cmds->make_int_c=NO_NUMBER_ARGUMENT;
    cmds->copy=false;
	cmds->copy_n=NO_NUMBER_ARGUMENT;
	cmds->copy_m=NO_NUMBER_ARGUMENT;
    cmds->swap=false;
	cmds->swap_n=NO_NUMBER_ARGUMENT;
	cmds->swap_m=NO_NUMBER_ARGUMENT;
    cmds->move=false;
	cmds->move_n=NO_NUMBER_ARGUMENT;
	cmds->move_m=NO_NUMBER_ARGUMENT;

	cmds->rows=false;
	cmds->rows_n=NO_NUMBER_ARGUMENT;
	cmds->rows_m=NO_NUMBER_ARGUMENT;
	cmds->beginswith=false;
	cmds->beginswith_c=NO_NUMBER_ARGUMENT;
	strcpy(cmds->beginswith_str, "");
	cmds->contains=false;
	cmds->contains_c=NO_NUMBER_ARGUMENT;
	strcpy(cmds->contains_str, "");

    int i=1;
    // processing all commands from command line, starting from 1 (argv[0] is name of the program)
    while(i<argc)
    {
        if(strcmp(argv[i], "irow") == 0)
       {
            cmds->irow=true;
            i++;
			cmds->irow_r = get_value_from_argument(argv, i);
       }

		if(strcmp(argv[i], "arow") == 0)
			cmds->arow=true;

		if(strcmp(argv[i], "drow") == 0)
		{
			cmds->drow=true;
            i++;
			cmds->drow_r = get_value_from_argument(argv, i);
		}

		if(strcmp(argv[i], "drows") == 0)
		{
			cmds->drows=true;
            i++;
			cmds->drows_n=get_value_from_argument(argv, i);
            i++;
			cmds->drows_m=get_value_from_argument(argv, i);
		}

		if(strcmp(argv[i], "acol") == 0)
			cmds->acol=true;

		if(strcmp(argv[i], "icol") == 0)
		{
			cmds->icol=true;
            i++;
			cmds->icol_c = get_value_from_argument(argv, i);
		}

		if(strcmp(argv[i], "dcol") == 0)
		{
			cmds->dcol=true;
            i++;
			cmds->dcol_c=get_value_from_argument(argv, i);
		}

		if(strcmp(argv[i], "dcols") == 0)
		{
			cmds->dcols=true;
            i++;
			cmds->dcols_n=get_value_from_argument(argv, i);
            i++;
			cmds->dcols_m=get_value_from_argument(argv, i);
		}

		if(strcmp(argv[i], "cset") == 0)
		{
			cmds->cset=true;
			i++;
			cmds->cset_c=get_value_from_argument(argv, i);
			if(cmds->cset_c != NO_NUMBER_ARGUMENT)
			{
				i++;
				get_string_from_argument(argv, i, cmds->cset_str);
			}
		}

		if(strcmp(argv[i], "tolower") == 0)
		{
			cmds->tolower=true;
            i++;
			cmds->tolower_c=get_value_from_argument(argv, i);
		}

		if(strcmp(argv[i], "toupper") == 0)
		{
			cmds->toupper=true;
            i++;
			cmds->toupper_c=get_value_from_argument(argv, i);
		}

		if(strcmp(argv[i], "round") == 0)
		{
			cmds->round=true;
            i++;
			cmds->round_c=get_value_from_argument(argv, i);
		}

		if(strcmp(argv[i], "int") == 0)
		{
			cmds->make_int=true;
            i++;
			cmds->make_int_c=get_value_from_argument(argv, i);
		}

		if(strcmp(argv[i], "copy") == 0)
		{
			cmds->copy=true;
            i++;
			cmds->copy_n=get_value_from_argument(argv, i);
            i++;
			cmds->copy_m=get_value_from_argument(argv, i);
		}

		if(strcmp(argv[i], "swap") == 0)
		{
			cmds->swap=true;
            i++;
			cmds->swap_n=get_value_from_argument(argv, i);
            i++;
			cmds->swap_m=get_value_from_argument(argv, i);
		}

		if(strcmp(argv[i], "move") == 0)
		{
			cmds->move=true;
            i++;
			cmds->move_n=get_value_from_argument(argv, i);
            i++;
			cmds->move_m=get_value_from_argument(argv, i);
		}

		if(!strcmp(argv[i], "rows"))
		{
			cmds->rows=true;
			i++;
			cmds->rows_n=get_value_from_argument_for_rows(argv, i, cmds);
			if(cmds->rows_n == ERROR_MISSING_ARGUMENT)
			{
				cmds->rows_m = ERROR_MISSING_ARGUMENT;
				//no existing arguments for row selection => selection cancelled
				cmds->rows=false;
			}
			else
			{
				i++;
				cmds->rows_m=get_value_from_argument_for_rows(argv, i, cmds);
			}
			if(cmds->rows_m == ERROR_MISSING_ARGUMENT)
			{
				cmds->rows=false;
			}
		}
		if(!strcmp(argv[i], "beginswith"))
		{
			cmds->beginswith=true;
			i++;
			cmds->beginswith_c = get_value_from_argument(argv, i);
			if(cmds->beginswith_c != NO_NUMBER_ARGUMENT)
			{
				i++;
				get_string_from_argument(argv, i, cmds->beginswith_str);
			}
		}

		if(!strcmp(argv[i], "contains"))
		{
			cmds->contains=true;
			i++;
			cmds->contains_c = get_value_from_argument(argv, i);
			if(cmds->contains_c != NO_NUMBER_ARGUMENT)
			{
				i++;
				get_string_from_argument(argv, i, cmds->contains_str);
			}
		}

        i++;
    }
    return 0;
}

// function parses delim from command line
// if deliminator does exist, returning position in argv
// otherwise returning 0
// in case of missing valid arg, returing error
int parse_deliminator(int argc, char *argv[], char delim[])
{
        int i=1;
        while(i<argc)
        {
                // printf("Spracovany arg:'%s'\n", argv[i]);
                if(strcmp(argv[i], "-d") == 0)
                {
                        // checking if deliminator character exist
                        if(argv[i+1] != NULL)
                        {
                                strcpy(delim, argv[i+1]);
                                i++;
                        }
                        else
                        {
                                // deliminator does not exist on i+1 position
                                // returning error
                                return ERROR_NO_DELIMINATOR_FOUND;
                        }
                        return i;
                }
                i++;
        }
        //no "-d" found
        return 0;
}

// function reads just one line from stdin
// returns number of characters, in case of error returning error value, in case of EOF returning EOF
int read_one_row(char buffer[])
{
	int i=0;
	int c;
	while( ( c=getchar() ) )
	{
        if(i > MAX_SIZE_ROW)
        {
            fprintf(stderr, "Exceeded size of row\n");
            return ERROR_EARLIER_END_PROGRAM;
        }

		if(c == EOF)
		{
            buffer[i] = '\0';
			return EOF;
		}

		if(c == '\n')
		{
            buffer[i] = '\0';
			return i;
		}
		buffer[i] = c;
		i++;
	}
	return 0;
}

// as delim will be used first char in string delim
void print_empty_row(char delim_char, int cols)
{
	for(int i=1; i<cols ;i++)
	{
		printf("%c", delim_char);
	}
	printf("\n");
}

int insert_char_at_position(char buffer[], char delim_char, int position)
{
	int i;
	for(i=strlen(buffer)+1; i>position; i--)
	{
		buffer[i]=buffer[i-1];
	}
	buffer[i]=delim_char;
	buffer[strlen(buffer)]='\0';
	return 0;
}

// searching for 'one_char' in string delim
// returning true, if success
bool is_delim(char one_char, char delim[])
{
    if(delim == NULL)
        return false;

    for(int i=0; i < (int)strlen(delim); i++)
    {
        if(delim[i] == one_char)
            return true;
    }
    return false;
}

int insert_new_column_at_position(char buffer[], char delim[], int col_pos)
{
    // buffer iterator, current column
	int i,tmp_col=1;

    // if adding new column on position 1 => adding delim[0] at the beggining of buffer
	if(col_pos == 1)
	{
		insert_char_at_position(buffer, delim[0], 0);
	}

    // otherwise we need to iterate the buffer to find the right column
	for(i=0; buffer[i] != '\0'; i++)
	{
		if(is_delim(buffer[i], delim))
		{
			tmp_col++;
		}
		if(tmp_col == col_pos)
		{
            // as delim for new column will be used delim[0]
			insert_char_at_position(buffer, delim[0], i);
            // if column found and added we can skip the rest of buffer
			break;
		}
	}
	return 0;
}

// deletes one character in char_array at position pos
int delete_char_at_position(char char_array[], int pos)
{
    // copying a character at position i to the previous positon
    // new char_array will one character shorter
    // beggining of string remains the same so starting copying from pos position
	for(int i=pos; i < (int)strlen(char_array) ;i++)
	{
		char_array[i] = char_array[i+1];
	}
	return 0;
}

// function deletes all character in column
int delete_column_at_position(char buffer[], char delim[], int column)
{
	int i, j=0; // iterators for buffer and new_buffer (without column)
	int tmp_col=1; // iterator for column
    char new_buffer[MAX_SIZE_ROW];

    // new_buffer will contain all characters except those ones in "column" 
	for(i=0; buffer[i] != '\0'; i++)
	{
        // column iterator
		if(is_delim(buffer[i], delim))
		{
			tmp_col++;
		}

        // if "column" is found we will not copy the character into the new_buffer
		if(tmp_col == column)
		{
			continue;
		}
		new_buffer[j] = buffer[i];
		j++;
	}
	new_buffer[j] = '\0';

    // column 1 is a special case where delim must be treated separately
	if (column == 1)
    {
        delete_char_at_position(new_buffer, 0);
    }
    strcpy(buffer, new_buffer); // returning buffer as new_buffer is local variable
	return 0;
}

// function deletes columns from n to m
// using previous function for deleting one column
int delete_columns(char buffer[], char delim[], int n, int m)
{
	int tmp_col = n; // column iterator

    // while reaching m column, deleting
	while(tmp_col <= m)
	{
		delete_column_at_position(buffer, delim, tmp_col);
        // if column is deleted the total columns and m value is one less
		m--;
	}
	return 0;
}

// function insert substring in string at position
// new_buffer contains merged strings, this is done is 3 steps
int insert_substring_in_string_at_position(char buffer[], char substr[], int position)
{
	int length_substr = strlen(substr);
	int length_buffer = strlen(buffer);
	int i, j;
	char new_buffer[MAX_SIZE_ROW];

    // step 1: till "position" everything is the same, copying chars buffer to new_buffer
	for(i=0; i<position; i++)
	{
		new_buffer[i] = buffer[i];
	}

    // step 2: from "position" till end of substr, copying chars substring to new_buffer
	j = 0;
	for(i=position; substr[j] != '\0'; i++)
	{
		new_buffer[i] = substr[j];
		j++;
	}

    // step 3: new_buffer length is sum of both strings, copying the rest of buffer into new_buffer
    // iterator i for new_buffer remains from previous loop
	j = 0;
	while(i < length_substr+length_buffer)
	{
		new_buffer[i] = buffer[position + j];
		i++;
		j++;
	}
	new_buffer[length_buffer+length_substr]='\0';
	strcpy(buffer, new_buffer); // sending new_buffer to main function 
	return 0;
}

// function returns number of total columns in the row
// counting all columns: all delims in the row + 1, delim is just a separator
int get_total_columns(char buffer[], char delim[])
{
	int i_col = 1;
	int i;
	for(i=0; buffer[i] != '\0'; i++)
	{
        // if delim contains buffer[i]
		if(is_delim(buffer[i], delim))
		{
			i_col++;
		}
	}
	return i_col;
}

// function sets value to column in row
// this is done in 3 steps: deleting "col" column, inserting empty column and inserting string in created column
int set_value_to_column(char buffer[], char delim[], int col, char col_str[])
{
	int total_columns = get_total_columns(buffer, delim);

    // step 1: as we do not know the length of column we delete everything inside 
	delete_column_at_position(buffer, delim, col);

	// step 2: adding new column
    // adding of new column at the end is handled separately: we add delim[0] at the end of row
	if(col == total_columns)
		insert_char_at_position(buffer, delim[0], strlen(buffer));
	else
		insert_new_column_at_position(buffer, delim, col);

    // step 3: inserting substring into empty column "col"
	if(col == 1)
	{
        // delim is located at buffer[0]
		insert_substring_in_string_at_position(buffer, col_str, 0);
		return 0;
	}
	int i_col=1;
	int i;
	for(i=0; buffer[i] != '\0'; i++)
	{
		if(is_delim(buffer[i], delim)) // columns counting
		{
			i_col++;
		}

		if(i_col == col)
		{
			insert_substring_in_string_at_position(buffer, col_str, i+1);
			return 0;
		}
	}
	return 0;
}

// function copy value from "buffer" from column "col" into the "col_value"
// returns length of col_value or ERROR_UNKNOWN_COLUMN
int get_value_from_column(char buffer[], char delim[], int col, char col_value[])
{
	int i,j,k;
	int tmp_col; // column iterator

    // if "col" is 1: our col_value ends till delim or till end of row
	if(col == 1)
	{
		for(i=0; (!is_delim(buffer[i], delim)) && (buffer[i] != '\0'); i++)
		{
			col_value[i] = buffer[i];
		}
		col_value[i]='\0';
		return i;
	}

    // handling other "col" values:
	tmp_col=1;
	for(i=0; buffer[i] != '\0'; i++)
	{
		if(is_delim(buffer[i], delim)) // column counting
		{
			tmp_col++;
		}
		if(tmp_col == col)
		{
            // we found the rigth "col"
            // characters from next position (i+1) till next delim will be our col_value 
			k=0;
			for(j=i+1; (!is_delim(buffer[j], delim)) && (buffer[j] != '\0'); j++)
			{
				col_value[k] = buffer[j];
				k++;
			}
			col_value[k] = '\0';
			return j;
		}
	}
	return ERROR_UKNOWN_COLUMN;
}

// function will set all characters in column "col" to lower
int set_column_tolower(char buffer[], char delim[], int col)
{
	if(col > get_total_columns(buffer, delim))
	{
		return ERROR_UKNOWN_COLUMN;
	}

    // temporary variable to read string from buffer from column
	char col_value[MAX_SIZE_COL];
	get_value_from_column(buffer, delim, col, col_value);

	// lowering each character in col_value
	for(int i=0; col_value[i] != '\0'; i++)
	{
		col_value[i]=tolower(col_value[i]);
	}

    // updating buffer with new col_value
	set_value_to_column(buffer, delim, col, col_value);

	return 0;
}

// function will set all chars in column "col" to upper
int set_column_toupper(char buffer[], char delim[], int col)
{
	if(col > get_total_columns(buffer, delim))
	{
		return ERROR_UKNOWN_COLUMN;
	}

    // temporary variable to get value from column "col"
	char col_value[MAX_SIZE_COL];
	get_value_from_column(buffer, delim, col, col_value);

    // converting chars in col_value to upper
	for(int i=0; col_value[i] != '\0'; i++)
	{
		col_value[i]=toupper(col_value[i]);
	}

    // updating buffer with new col_value
	set_value_to_column(buffer, delim, col, col_value);
	return 0;
}

// function returns 10^exponent
float my_pow_10(int exponent)
{
    float exp=1.0;

    if (exponent > 0)
    {
        for(int i=1; i<=exponent; i++)
            exp *= 10;
    }
    else if (exponent < 0)
    {
        for(int i=-1; i>=exponent; i--)
            exp /= 10;
    }
    return exp;
}

// function converts float variable "f" to string "string"
// converting from 10^8 till 10^-4
int float_to_string(float f, char string[])
{
	int i=0;

    // treating negative float number: string[0] = '-'
	if(f<0)
	{
		string[i] = '-';
		f = -1*f;
		i++;
	}

    //
	float tmp = f;
	int exponent;
	int digit;

    // step 1: converting positive figures, starting 10^8 till 10^0
	for(exponent=8; exponent >= 0 ; exponent--)
	{
        // dividing with appropriate divider will get 1 single digit
		tmp = f/my_pow_10(exponent);

        // truncating  after decimal point and keep result in "digit"
		tmp = (float)((int)tmp);
		digit = (int)tmp;

        // after successful getting "digit", reducing f number for the size we already converted
        // already converted: digit*10^exponent
        // "f" will have lower exponent in the next round
		f = f-digit*my_pow_10(exponent);

        // making ASCII '0'
		string[i] = digit + '0';
		string[i+1] = '\0';
		i++;
	}

    // treating decimal point numbers
	if(f>0)
	{
		string[i] = '.';
		i++;
        string[i+1] = '\0';

        // converting from 10^-1 till 10^-4
		for(exponent=-1; exponent>=-4; exponent--)
		{
			tmp = f/my_pow_10(exponent);
			tmp = (float)((int)tmp);
			digit = (int)tmp;
			f= f - digit*my_pow_10(exponent);

			string[i] = digit + '0';
			string[i+1] = '\0';
			i++;
		}
	}

	// deleting '0' from string in front of the number
	// expecting, there are some (flag still_have_zero_to_delete)
	bool still_have_zero_to_delete = true;
	i=0;

	if(string[0] == '-') // negative numbers have '-' at string[0], zero deletion will start from string[1]
		i=1;

	while(string[i] != '\0')
	{
		if(!still_have_zero_to_delete) // stop condition
			break;

		if(string[i] == '0')
		{
			delete_char_at_position(string, i);
			continue;
		}
		else // we already deleted all 0, only other digits remains
		{
			still_have_zero_to_delete=false;
		}
		i++;
	}
	return 0;
}

// return rounded value
float my_round(float f)
{
    float tmp;
    tmp = (float)((int)f);

    // rounding is based on delta
    float delta = f - tmp;

    // rounding positive numbers
    if(f>0)
    {
        if(delta >= 0.5) // rounding up
            return tmp+1;
        return tmp; // rounding down => staying with tmp
    }

    // rounding negative numbers
    if(delta <= -0.5) // rounding down
        return tmp-1;
    return tmp; // rouding up
}

// function will round  column number value
int set_column_round(char buffer[], char delim[], int col)
{
	if(col > get_total_columns(buffer, delim))
	{
		return ERROR_UKNOWN_COLUMN;
	}
    if(col < 1)
    {
        return ERROR_UKNOWN_COLUMN;
    }

	char col_value[MAX_SIZE_COL]; // temporary variable for column value
	get_value_from_column(buffer, delim, col, col_value);

    // converting string to float
	char *end;
	float f;
	f = strtof(col_value, &end);

    // if unsuccessful conversion returning ERROR_CONVERSION_FAILED
	if(strlen(end) > 0)
		return ERROR_CONVERSION_FAILED;

	f = my_round(f);
	float_to_string(f, col_value);

    // updating column in buffer
	set_value_to_column(buffer, delim, col, col_value);
	return 0;
}

// function will make number without decimal numbers (nothing after decimal point)
int set_column_int(char buffer[], char delim[], int col)
{
	if(col> get_total_columns(buffer, delim))
		return ERROR_UKNOWN_COLUMN;

	char col_value[MAX_SIZE_COL]; // temporary variable for column value
	get_value_from_column(buffer, delim, col,  col_value);

    // converting string to float
	char *end;
	float f;
	f = strtof(col_value, &end);

    // if unsuccessful conversion returning ERROR
	if(strlen(end) > 0)
		return ERROR_CONVERSION_FAILED;

	f = (float)((int)f);
	float_to_string(f, col_value);

    // updating buffer
	set_value_to_column(buffer, delim, col, col_value);
	return 0;
}

// function updates cmds structure according to rows definition
// as this requires additional error handling, I created separated function (derived from get_value_from_argument)
int get_value_from_argument_for_rows(char *argv[], int i, Commands *cmds)
{
	if(!cmds->rows)
		return ERROR_ROWS_ARGUMENT_NOT_FOUND;

	if(argv[i] == NULL)
	{
		// argument does not exist
		return ERROR_MISSING_ARGUMENT;
	}

	if(!strcmp(argv[i], "-"))
	{
		// rows_arg is -
		return REQUESTED_LAST_ROW;
	}

	// argument is number
	return get_value_from_argument(argv, i);
}

// based on current row, the function returns true/false
// function is used for selecting rows
bool is_row_selected(int cur_row, int n, int m, bool flag_last_line)
{
    // if n is the last line we need to check the flag_last_line
	if(n == REQUESTED_LAST_ROW)
	{
        if(flag_last_line)
		    return true;
	}
    else
    {
        // n is not the last line
	    if(n <= cur_row)
	    {
		    if(cur_row <= m) // n <= cur_row <= m
			    return true;

		    if(m == REQUESTED_LAST_ROW) // if m is the last line, always true
			    return true;
	    }
    }
	return false;
}

// function returns true/false based on value in approriate column (stored in cmds)
bool does_col_beginswith(char buffer[], char delim[], Commands cmds)
{
    // getting value from buffer to col_value
	char col_value[MAX_SIZE_COL];
	get_value_from_column(buffer, delim, cmds.beginswith_c, col_value);

	// finding the first occurance of col_value in buffer
    char *ptr_pos = strstr(col_value, cmds.beginswith_str);
	if(ptr_pos) // if found occurance
	{
		if((ptr_pos - col_value) == 0) // match starts at position 0 => starts with 
			return true;
	}
	// substr does not start with
	return false;
}

// function returns true/false based on value in buffer column (details stored in cmds)
bool does_col_contains(char buffer[], char delim[], Commands cmds)
{
	char col_value[MAX_SIZE_COL]; // temporary variable for value in the column
	get_value_from_column(buffer, delim, cmds.contains_c, col_value);

    // finding occurance in the buffer
	char *ptr_pos = strstr(col_value, cmds.contains_str);
	if(ptr_pos) // col contains substring
		return true;

	return false;
}

// function proccesses data rows based on commands from command line
// commands are parsed and stored in "cmds"
// row is in "buffer"
int process_data_in_row(char buffer[], char delim[], Commands cmds)
{
	if(cmds.cset) // option for cset C STR
	{
        if(cmds.cset_c == NO_NUMBER_ARGUMENT)
        {
            fprintf(stderr, "cset c - wrong argument\n");
            return NO_NUMBER_ARGUMENT;
        }
		set_value_to_column(buffer, delim, cmds.cset_c, cmds.cset_str);
	}

	if(cmds.tolower) // option for tolower C
	{
        if(cmds.tolower_c == NO_NUMBER_ARGUMENT)
        {
            fprintf(stderr, "tolower c - wrong argument\n");
            return NO_NUMBER_ARGUMENT;
        }
		set_column_tolower(buffer, delim, cmds.tolower_c);
	}

	if(cmds.toupper) // option for toupper C
	{
        if(cmds.toupper_c == NO_NUMBER_ARGUMENT)
        {
            fprintf(stderr, "toupper c - wrong argument\n");
            return NO_NUMBER_ARGUMENT;
        }
		set_column_toupper(buffer, delim, cmds.toupper_c);
	}

	if(cmds.round) // option for round C
	{
        if(cmds.round_c == NO_NUMBER_ARGUMENT)
        {
            fprintf(stderr, "round c - wrong argument\n");
            return NO_NUMBER_ARGUMENT;
        }
		set_column_round(buffer, delim, cmds.round_c);
	}

	if(cmds.make_int) // option for int C
	{
        if(cmds.make_int_c == NO_NUMBER_ARGUMENT)
        {
            fprintf(stderr, "int c - wrong argument\n");
            return NO_NUMBER_ARGUMENT;
        }
		set_column_int(buffer, delim, cmds.make_int_c);
	}

	if(cmds.copy) // option for copy N M
	{
        if((cmds.copy_n == NO_NUMBER_ARGUMENT) || (cmds.copy_n < 1))
        {
            fprintf(stderr, "copy n - wrong argument\n");
            return NO_NUMBER_ARGUMENT;
        }
        if((cmds.copy_m == NO_NUMBER_ARGUMENT) || (cmds.copy_m < 1))
        {
            fprintf(stderr, "copy m - wrong argument\n");
            return NO_NUMBER_ARGUMENT;
        }

        // copying value from column N to temporary "col_value"
		char col_value[MAX_SIZE_COL];
		get_value_from_column(buffer, delim, cmds.copy_n, col_value);
        // setting "col_value" into the column M
		set_value_to_column(buffer, delim, cmds.copy_m, col_value);
	}

	if(cmds.swap) // option for swap N M
	{
        if((cmds.swap_n == NO_NUMBER_ARGUMENT) || (cmds.swap_n < 1))
        {
            fprintf(stderr, "swap n - wrong argument\n");
            return NO_NUMBER_ARGUMENT;
        }
        if((cmds.swap_m == NO_NUMBER_ARGUMENT) || (cmds.swap_m < 1))
        {
            fprintf(stderr, "swap m - wrong argument\n");
            return NO_NUMBER_ARGUMENT;
        }

        // getting values from column N (col_value_n) and column M (col_value_m)
		char col_value_n[MAX_SIZE_COL], col_value_m[MAX_SIZE_COL];
		get_value_from_column(buffer, delim, cmds.swap_n, col_value_n);
		get_value_from_column(buffer, delim, cmds.swap_m, col_value_m);
        // setting swaped values
		set_value_to_column(buffer, delim, cmds.swap_n, col_value_m);
		set_value_to_column(buffer, delim, cmds.swap_m, col_value_n);
	}

	if(cmds.move) // option for move N M
	{
        if((cmds.move_n == NO_NUMBER_ARGUMENT) || (cmds.move_n < 1))
        {
            fprintf(stderr, "move n  - wrong argument\n");
            return NO_NUMBER_ARGUMENT;
        }
        if((cmds.move_m == NO_NUMBER_ARGUMENT) || (cmds.move_m < 1))
        {
            fprintf(stderr, "move m - wrong argument\n");
            return NO_NUMBER_ARGUMENT;
        }

        // done in 3 steps
        // step 1: getting value from column to "col_value" , this will be moved to column M
		char col_value[MAX_SIZE_COL];
		get_value_from_column(buffer, delim, cmds.move_n, col_value);

        // step 2: creating new column with "col_value"
		insert_new_column_at_position(buffer, delim, cmds.move_m);
		set_value_to_column(buffer, delim, cmds.move_m, col_value);

        // now we have one more column
        // moving behind M means I should include M column position in N column counting
		if(cmds.move_n > cmds.move_m)
		{
			cmds.move_n = cmds.move_n+1;
		}
        // step 3: deleting moved column N
		delete_column_at_position(buffer,  delim, cmds.move_n);
	}
	return 0;
}
