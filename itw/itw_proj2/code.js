function show_elim_att_thor1(){
    document.getElementById("att_info_1").style.display = "block";
    document.getElementById("att_info_2").style.display = "none";
    document.getElementById("att_info_3").style.display = "none";
    document.getElementById("att_info_4").style.display = "none";
}

function show_elim_att_thor2(){
    document.getElementById("att_info_1").style.display = "none";
    document.getElementById("att_info_2").style.display = "block";
    document.getElementById("att_info_3").style.display = "none";
    document.getElementById("att_info_4").style.display = "none";
}

function show_elim_att_avengers1(){
    document.getElementById("att_info_1").style.display = "none";
    document.getElementById("att_info_2").style.display = "none";
    document.getElementById("att_info_3").style.display = "block";
    document.getElementById("att_info_4").style.display = "none";
}

function show_elim_att_thor3(){
    document.getElementById("att_info_1").style.display = "none";
    document.getElementById("att_info_2").style.display = "none";
    document.getElementById("att_info_3").style.display = "none";
    document.getElementById("att_info_4").style.display = "block";
}