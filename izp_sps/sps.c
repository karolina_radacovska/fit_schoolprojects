/**************************************************/
/* * *                 sps.c                  * * */
/* * *                                        * * */
/* * *         semestralny projekt 2          * * */
/* * *                                        * * */
/* * *          Karolina Radacovska           * * */
/* * *               xradac00                 * * */
/* * *             December 2020              * * */
/**************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>

#define ERROR_NOT_ENOUGH_ARGUMENTS -1
#define ERROR_FILE_NAME_MISSING -2
#define ERROR_EMPTY_COMMAND -3
#define ERROR_SELECTION_CONVERSION_FAILED -4
#define ERROR_NO_SUBSTRING_FOUND -5
#define ERROR_NO_MIN_FOUND -6
#define ERROR_NO_MAX_FOUND -7
#define ERROR_SELECTION_FAILED -8
#define ERROR_EMPTY_LINE -9
#define ERROR_NOT_VALID_SELECTION -10
#define ERROR_TMP_INCORRECT_FORMAT -11
#define ERROR_TMP_SELECTION_FAILED -12
#define ERROR_INVALID_NUMBER_OF_COMMANDS -13
#define ERROR_AVG_ZERO_COUNTS -14

#define ALL_ROW_COL_SELECTED -20
#define MAX_SIZE 1001

#define CMD_IROW 1
#define CMD_AROW 2
#define CMD_DROW 3
#define CMD_ICOL 4
#define CMD_ACOL 5
#define CMD_DCOL 6

#define CMD_SET 7
#define CMD_CLEAR 8
#define CMD_SWAP 9
#define CMD_SUM 10
#define CMD_COUNT 11
#define CMD_LEN 12
#define CMD_AVG 13

#define CMD_TMP_DEF 14
#define CMD_TMP_USE 15
#define CMD_TMP_INC 16
#define CMD_TMP_SET 17

#define CMD_NO_COMMAND 20

typedef char* cell_t;
typedef cell_t* row_t;

typedef struct
{
    row_t* data;
    int total_rows;
    int total_cols;
} table_t;

typedef struct
{
    int row_from, row_to;
    int col_from, col_to;
} selection_t;

typedef char* delim_t;
typedef char* string_t;
typedef string_t* all_commands_t;

typedef struct 
{
    string_t value;
} tmp_variable_t;


bool is_delim(char c, char *delim);
bool is_selection(string_t command);
bool is_string_bordered_by_quots(string_t string);
// comandy
int parse_arguments(int argc, char *argv[], string_t *delim, all_commands_t *cmds, int *number_of_commands, char* file_name);
int get_number_of_commands(string_t cmd_sequence);
int parse_commands(char* cmd_sequence, all_commands_t* cmds, int *num_of_commands);
int read_str_from_command(string_t current_command, int from_pos, string_t str);
int get_value_from_command(char *command, int *i_from, char *stop_char);
void release_all_commands(all_commands_t cmds, int num);
// file
int parse_file_name(char *file_name_in_one_arg, char* file_name);
int read_line_from_file(FILE *pfile, string_t *line);
int fill_table_from_file(table_t *tab, char *file_name, char* delim);
// cells
int get_string_from_cell(table_t table, int i_row, int i_col, string_t *cell_string);
int set_string_to_cell(table_t *table, int i_row, int i_col, string_t cell_string);
void print_cell(char *string, bool quoted);
bool contains_cell_any_delim(char *string, char * delim);
//selection min max find
int set_selection_to_min(table_t tab, selection_t *selection);
int set_selection_to_max(table_t tab, selection_t *selection);
int set_selection_to_find_str(table_t tab, selection_t *selection, string_t substring);
// pomocne
int delete_char_at_position(char char_array[], int pos);
int append_char(string_t *string, char ch);
int remove_quots(string_t *string);
int float_to_string(float f, char string[]);
float my_pow_10(int exponent);
int copy_row_content(table_t *table, int row_to, int row_from);
// table
void fprintf_table(table_t tab, char* delim);
void release_table(table_t *tab);
int append_line_to_table(string_t line, char* delim, int *number_of_cols, table_t *tab, int i_row);
int select_the_cells_in_table(table_t *tab, selection_t *selection, string_t command);
void print_table(table_t tab, char* delim);
// alloc
int allocate_memmory_for_next_row(table_t *tab, int i_row);
// upravy tabulky
bool set_structure_change(string_t command, int *cmd_id);
int append_empty_row(table_t *table, selection_t selection);
int append_cols_to_tab(table_t *tab, int current_col, int current_row);
int insert_empty_row(table_t *table, selection_t selection);
int insert_empty_col(table_t *table, selection_t selection);
int append_empty_col(table_t *table, selection_t selection);
int delete_row(table_t *table, selection_t *selection);
int delete_col(table_t *table, selection_t *selection);
// uprava dat tabulky
bool set_cell_content_change(string_t command, int *cmd_id);
int update_table_set_str(table_t *tab, selection_t selection, string_t str);
int update_table_swap(table_t *table, selection_t source_selection, string_t target_selection_string);
int update_table_sum(table_t *table, selection_t source_selection, string_t target_sel_str);
int update_table_count(table_t *table, selection_t source_selection, string_t target_sel_str);
int update_table_len(table_t *table, selection_t source_selection, string_t target_sel_str);
int update_table_avg(table_t *table, selection_t source_selection, string_t target_sel_str);
// tmp premenne
bool set_temporary_variables(string_t command, int *cmd_id);
int init_tmp_vars(tmp_variable_t tmpvars[]);
int store_tmp_value(table_t table, selection_t selection, string_t str, tmp_variable_t tmp_vars[]);
void release_tmp_vars(tmp_variable_t tmpvars[]);
int use_tmp_value(table_t *table, selection_t selection, string_t str, tmp_variable_t tmpvars[]);
int inc_tmp_value(string_t str, tmp_variable_t tmpvars[]);
int set_tmp_selection(selection_t selection, selection_t *tmp_sel);
int create_selection_from_tmp(selection_t tmp_selection, char str_selection[]);

int main(int argc, char *argv[])
{
    all_commands_t cmds = NULL;
    int number_of_commands = 0;

    if(argc<2) // checking the count of arguments
        return ERROR_NOT_ENOUGH_ARGUMENTS;

    // delim delifinition, allocation, default set to " "
    //char *delim;
    string_t delim;
    delim = malloc(2*sizeof(char));
    strcpy(delim, " ");

    char file_name[255];
    parse_arguments(argc, argv, &delim, &cmds, &number_of_commands, file_name);

    // filling the table with data from file
    table_t table;
    fill_table_from_file(&table, file_name, delim);
    //fprintf_table(table, delim);
    
    tmp_variable_t tmpvars[10]; // array for 10 tmp variables
    init_tmp_vars(tmpvars);
    
    selection_t selection; // regular selection
    selection_t tmp_selection; // tmp selection

    int current_command = 0;

    // processing all cmds
    for(int j=0; j < number_of_commands; j++)
    {
        if(is_selection(cmds[j]))
        {
            // if [_] we replace current command with the one from the tmp variable
            if(!strcmp(cmds[j], "[_]"))
            {
                char str_selection[MAX_SIZE] = "";
                create_selection_from_tmp(tmp_selection, str_selection);
                cmds[j] = realloc(cmds[j], sizeof(char) * (strlen(str_selection)+1));
                strncpy(cmds[j], str_selection, strlen(str_selection)); 
            }
            // parse the selection from string and extend table if is needed
            if(select_the_cells_in_table(&table, &selection, cmds[j]) != 0)
                break;
            
            continue;
        }
        // selection is done, now acting based on the command
        current_command = CMD_NO_COMMAND;
        // check for table struct change commands
        set_structure_change(cmds[j], &current_command);
        // check for content change
        set_cell_content_change(cmds[j], &current_command);
        // check for tmp variable commands
        set_temporary_variables(cmds[j], &current_command);

        char str[MAX_SIZE] = "";
        switch (current_command)
        {
            case CMD_IROW:
                //fprintf(stderr, "CMD_IROW %d\n", current_command);
                insert_empty_row(&table, selection);
                break;
            case CMD_AROW:
                //fprintf(stderr, "CMD_AROW \n");
                append_empty_row(&table, selection);
                break;
            case CMD_DROW:
                //fprintf(stderr, " CMD_DROW \n");
                //delete_row(&table, selection);
                break;
            case CMD_ICOL:
                //fprintf(stderr, "CMD_ICOL\n");
                insert_empty_col(&table, selection);
                break;
            case CMD_ACOL:
                //fprintf(stderr, "CMD_ACOL\n");
                append_empty_col(&table, selection);
                break;
            case CMD_DCOL:
                //fprintf(stderr, "command recognized\n");
                break;
            case CMD_SET:
                //fprintf(stderr, "CMD_SET\n");
                str[0] = '\0';
                read_str_from_command(cmds[j], 4, str);
                update_table_set_str(&table, selection, str);
                break;
            case CMD_CLEAR:
                //fprintf(stderr, "CMD_CLEAR\n");
                update_table_set_str(&table, selection, "");
                break;
            case CMD_SWAP:
                //fprintf(stderr, "CMD_SWAP\n");
                str[0] = '\0';
                read_str_from_command(cmds[j], 5, str);
                update_table_swap(&table, selection, str);
                break;
            case CMD_SUM:
                 //fprintf(stderr, "CMD_SUM\n");
                str[0] = '\0';
                read_str_from_command(cmds[j], 4, str);
                update_table_sum(&table, selection, str);
                break;
            case CMD_COUNT:
                //fprintf(stderr, "CMD_COUNT\n");
                str[0] = '\0';
                read_str_from_command(cmds[j], 6, str);
                update_table_count(&table, selection, str);
                break;
            case CMD_LEN:
                //fprintf(stderr, "CMD_LEN\n");
                str[0] = '\0';
                read_str_from_command(cmds[j], 4, str);
                update_table_len(&table, selection, str);
                break;
            case CMD_AVG:
                //fprintf(stderr, "CMD_AVG\n");
                str[0] = '\0';
                read_str_from_command(cmds[j], 4, str);
                if(update_table_avg(&table, selection, str) == ERROR_AVG_ZERO_COUNTS)
                {
                    fprintf(stderr, "ERROR: average: division by zero\n");
                    current_command = CMD_NO_COMMAND; 
                }               
                break;
            case CMD_TMP_DEF:
                //fprintf(stderr, "CMD_TMP_DEF\n");
                str[0] = '\0';
                read_str_from_command(cmds[j], 4, str);
                store_tmp_value(table, selection, str, tmpvars);
                break;
            case CMD_TMP_USE:
                //fprintf(stderr, "CMD_TMP_USE\n");
                str[0] = '\0';
                read_str_from_command(cmds[j], 4, str);
                use_tmp_value(&table, selection, str, tmpvars);
                break;
            case CMD_TMP_INC:
                //fprintf(stderr, "CMD_TMP_INC\n");
                str[0] = '\0';
                read_str_from_command(cmds[j], 4, str);
                inc_tmp_value(str, tmpvars);
                break;
            case CMD_TMP_SET:
                //fprintf(stderr, "CMD_TMP_SET\n");
                if(set_tmp_selection(selection, &tmp_selection) == ERROR_TMP_SELECTION_FAILED)
                    current_command = CMD_NO_COMMAND;
                break;
            
            default:
                fprintf(stderr, "ERROR: unsupported command\n");
                break;
        }
        if(current_command == CMD_NO_COMMAND)
        {
            break;
        }
    }
    print_table(table, delim);
    release_table(&table);
    free(delim);
    release_all_commands(cmds, number_of_commands);
    release_tmp_vars(tmpvars);
    return 0;
}

//function implementation

// function creates string from tmp_selection
// we need string in nex function, in main, to extend the table if is needed
int create_selection_from_tmp(selection_t tmp_selection, char str_selection[])
{
    // '[' + tmp_selection.row_from + ',' + tmp_selection.col_from + ',' + tmp_selection.row_to + ',' + tmp_selection.col_to + ']'
    char tmp[MAX_SIZE] = "";

    str_selection[0] = '\0';
    strcat(str_selection, "[");
    float_to_string((float)tmp_selection.row_from, tmp);
    strcat(str_selection, tmp);
    strcat(str_selection, ",");
    float_to_string((float)tmp_selection.col_from, tmp);
    strcat(str_selection, tmp);
    strcat(str_selection, ",");
    float_to_string((float)tmp_selection.row_to, tmp);
    strcat(str_selection, tmp);
    strcat(str_selection, ",");
    float_to_string((float)tmp_selection.col_to, tmp);
    strcat(str_selection, tmp);
    strcat(str_selection, "]");

    return 0;
}

// funtion stores value from selection to tmp variable 
int store_tmp_value(table_t table, selection_t selection, string_t str, tmp_variable_t tmp_vars[])
{
    // def _X, str="_X"
    if(str[0] != '_')
    {
        fprintf(stderr, "ERROR: temporary variable - incorrect format \n");
        return ERROR_TMP_INCORRECT_FORMAT;
    }    

    int tmp_index = str[1] - '0';
    if((tmp_index < 0) || (tmp_index > 9))
    {
        fprintf(stderr, "ERROR: temporary variable - incorrect format \n");
        return ERROR_TMP_INCORRECT_FORMAT;
    }   
     
    string_t cell_value = NULL; 
    // selected value from table.data[selection.row_from-1][selection.col_from-1] will be stored in tmp var value
    get_string_from_cell(table, selection.row_from-1, selection.col_from-1, &cell_value);
    if(tmp_vars[tmp_index].value != NULL)
    {
        free(tmp_vars[tmp_index].value);
        tmp_vars[tmp_index].value = NULL;
    }
    tmp_vars[tmp_index].value = calloc(strlen(cell_value)+1, sizeof(char));
    strncpy(tmp_vars[tmp_index].value, cell_value, strlen(cell_value));

    free(cell_value);
    cell_value = NULL;
    return 0;
}

// function copies value from tmp variable to cells in selection range
int use_tmp_value(table_t *table, selection_t selection, string_t str, tmp_variable_t tmpvars[])
{
    // use _X, str="_X"
    if(str[0] != '_')
    {
        fprintf(stderr, "ERROR: temporary variable - incorrect format \n");
        return ERROR_TMP_INCORRECT_FORMAT;
    }    

    int tmp_index = str[1] - '0';
    if((tmp_index < 0) || (tmp_index > 9))
    {
        fprintf(stderr, "ERROR: temporary variable - incorrect format \n");
        return ERROR_TMP_INCORRECT_FORMAT;
    }   

    // tmp_vars[tmp_index].value -> table->data[irow][icol]
    int i_row, i_col;
    for(i_row = selection.row_from-1; i_row < selection.row_to; i_row++)
    {
        for(i_col = selection.col_from-1; i_col < selection.col_to; i_col++)
        {
           // replacing current cell value with tmp one
            set_string_to_cell(table, i_row, i_col, tmpvars[tmp_index].value);
        }
    }

    return 0;
}

int inc_tmp_value(string_t str, tmp_variable_t tmpvars[])
{
    // inc _X, str="_X"
    if(str[0] != '_')
    {
        fprintf(stderr, "ERROR: temporary variable - incorrect format \n");
        return ERROR_TMP_INCORRECT_FORMAT;
    }    

    int tmp_index = str[1] - '0';
    if((tmp_index < 0) || (tmp_index > 9))
    {
        fprintf(stderr, "ERROR: temporary variable - incorrect format \n");
        return ERROR_TMP_INCORRECT_FORMAT;
    }   

    if(tmpvars[tmp_index].value == NULL)
    {
        tmpvars[tmp_index].value = calloc(2, sizeof(char));
        tmpvars[tmp_index].value[0] = '1';
        tmpvars[tmp_index].value[1] = '\0';
        return 0;
    }

    // tmp variable has some string    
    char *end;
    float f = strtof(tmpvars[tmp_index].value, &end);
    if(strlen(end) > 0) // string has not valid number, setting to "1"
    {
        free(tmpvars[tmp_index].value);
        tmpvars[tmp_index].value = NULL;
        tmpvars[tmp_index].value = calloc(2, sizeof(char));
        tmpvars[tmp_index].value[0] = '1';
        tmpvars[tmp_index].value[1] = '\0';
        return 0;
    }
    
    // we have valid number from conversion, increasing f
    f += 1;
     // new_str will keep converted value, these will be copied to tmp variable value
    char new_str[MAX_SIZE] = "";
    float_to_string(f, new_str);
    free(tmpvars[tmp_index].value);
    tmpvars[tmp_index].value = NULL;
    tmpvars[tmp_index].value = calloc(strlen(new_str)+1 ,sizeof(char));
    strncpy(tmpvars[tmp_index].value, new_str, strlen(new_str));

    return 0;
}

// storing current selection in tmp variable
int set_tmp_selection(selection_t selection, selection_t *tmp_sel)
{
    tmp_sel->row_from = selection.row_from;
    tmp_sel->row_to = selection.row_to;
    tmp_sel->col_from = selection.col_from;
    tmp_sel->col_to = selection.col_to;
    return 0;
}

int init_tmp_vars(tmp_variable_t tmpvars[])
{   
    int i; // tmp counter
    for(i=0; i<10; i++)
    {
        tmpvars[i].value = NULL;
    }

    return 0;
}

// function releases memmory for strings in tmp variables
void release_tmp_vars(tmp_variable_t tmpvars[])
{
    int i;
    for(i=0; i<10; i++)
    {
        if(tmpvars[i].value != NULL)
        {
            free(tmpvars[i].value);
            tmpvars[i].value = NULL;
        }
    }
}

bool is_delim(char c, char *delim)
{
    if(delim == NULL)
        return false;

    for(size_t i=0; i < strlen(delim); i++)
    {
        if(delim[i] == c)
            return true;
    }
        
    return false;
}

int get_number_of_commands(string_t cmd_sequence)
{
    int i, cmds_count=0;
    for(i=0; cmd_sequence[i] != '\0'; i++)
    {
        if(cmd_sequence[i] == ';')
        {
            cmds_count++;
        }
    }
    cmds_count++;

    return cmds_count;
}

// function will put commands into the structure
int parse_commands(char* cmd_sequence, all_commands_t* cmds, int *num_of_commands)
{
    // handling empty arguments
    if((cmd_sequence == NULL) || (strlen(cmd_sequence) == 0))
    {
        fprintf(stderr, "no commands in command line\n");
        return ERROR_EMPTY_COMMAND;
    }

    *num_of_commands = get_number_of_commands(cmd_sequence);
    *cmds = (all_commands_t)calloc( (*num_of_commands), (sizeof(string_t)));

    char tmp_command[MAX_SIZE];
    int j,k=0; // k for tmp string iterating
    int i_cmds=0; // command iterator
    for(j=0; cmd_sequence[j] != '\0'; j++)
    {
        //printf("%d\n", j);
        if(is_delim(cmd_sequence[j], ";"))
        {
            // allocating chars +1 for '\0'
            (*cmds)[i_cmds] = (string_t)calloc((strlen(tmp_command)+1), sizeof(char));
            strncpy((*cmds)[i_cmds], tmp_command, strlen(tmp_command));
            k=0;
            i_cmds++; // value for next commmand
            tmp_command[0] = '\0';
            continue;
        }
        tmp_command[k] = cmd_sequence[j];
        tmp_command[k+1] = '\0';
        //printf("%s\n", tmp_command);
        k++;
    }
    // remaining tmp command keeps the last command from cmd_sequence
    (*cmds)[i_cmds] = (string_t)calloc((strlen(tmp_command)+1), sizeof(char));
    strncpy((*cmds)[i_cmds], tmp_command, strlen(tmp_command));

    return 0;
}

void release_all_commands(all_commands_t cmds, int num)
{
    for(int i=0; i<num; i++)
    {
        free(cmds[i]);
    }
    free(cmds);
}

// function checks file name, returning name
int parse_file_name(char *file_name_in_one_arg, char* file_name) 
{
    if(file_name_in_one_arg == NULL)
        return ERROR_FILE_NAME_MISSING;

    strcpy(file_name, file_name_in_one_arg);
    return 0;
}

// function fulfill the structure with arguments from command line
int parse_arguments(int argc, char *argv[], string_t *delim, all_commands_t *cmds, int *number_of_commands, char* file_name)
{
    // check if arguments are enough
    if(argc < 2)
        return ERROR_NOT_ENOUGH_ARGUMENTS;

    int i = 1;
    // checking delim
    if(!strcmp(argv[i], "-d"))
    {
        // delim found
        free(*delim);
        *delim = NULL;
        i++;
        *delim = calloc((strlen(argv[i])+ 1), sizeof(char));
        strncpy(*delim, argv[i], strlen(argv[i]));
        i++;
    }

    // parsing commands ending with ';' to string array
    parse_commands(argv[i], cmds, number_of_commands);
    i++;

    // parsing name of file
    parse_file_name(argv[i], file_name);

    return 0;
}

// returns true only if first char in cmd is [ and the last is ]
bool is_selection(string_t command)
{
    if((command[0] == '[') && (command[strlen(command)-1] == ']'))
    {
        if(is_delim(',', command))
            return true;
    }

    if(!strcmp(command, "[min]"))
        return true;
    
    if(!strcmp(command, "[max]"))
        return true;

    if(!strncmp(command, "[find ", 6))
        return true;

    if(!strncmp(command, "[_]", 3))
        return true;

    return false;
}

// reads chars from i_from to i_to, or stop_char is found, and convert them to int
// exceptions: _, - => managed as return value
int get_value_from_command(char *command, int *i_from, char *stop_char)
{
    int i=0; // command iterator
    int j=0; // temporary string iterator
    char tmp[MAX_SIZE] = ""; // empty temporary string


    for(i=*i_from; (command[i] != '\0') && (!is_delim(command[i], stop_char)); i++)
    {
        tmp[j] = command[i];
        j++;
    }
    tmp[j] = '\0';

    // moving i_from after proccessed chars
    *i_from = i;

    if(!strcmp(tmp, "_")) // handling whole column C
             return ALL_ROW_COL_SELECTED;

    if(!strcmp(tmp, "-")) // handling whole column C
             return ALL_ROW_COL_SELECTED;

    char *end;
    int value;
    value = strtod(tmp, &end);
    if(strlen(end) > 0)
    {
        return ERROR_SELECTION_CONVERSION_FAILED;
    }

    return value;
}

// function returns string from cell
int get_string_from_cell(table_t table, int i_row, int i_col, string_t *cell_string)
{
    int len = strlen(table.data[i_row][i_col]);
    *cell_string = NULL;
    *cell_string = calloc(len+1, sizeof(char));
    strncpy(*cell_string, table.data[i_row][i_col],len);
    return 0;    
}

// function searchs for min value in cells within the range
// if succees, returns selection, if failed, returns error
int set_selection_to_min(table_t tab, selection_t *selection)
{
    selection_t selection_for_min; // row, col position for min 
    bool min_found = false; // flag if any min found
    float cell_float; // tmp float for cell_string value conversion

    // min is the first cell
    string_t cell_str = NULL;
    char *end;
    get_string_from_cell(tab, selection->row_from-1, selection->col_from-1, &cell_str);
    float min = strtof(cell_str, &end); 
    if(strlen(end) == 0) // valid number found, no need to find the first one
    {
        min_found = true;
    } // otherwise we need to find first valid min number within range
    free(cell_str);
    cell_str = NULL;

    // setting min as first cell in selection
    selection_for_min.row_from = selection->row_from;
    selection_for_min.row_to = selection->row_from;
    selection_for_min.col_from = selection->col_from;
    selection_for_min.col_to = selection->col_from;

    
    // comparing min with other values in selection range
    int i_row, i_col;
    for(i_row = selection->row_from-1; i_row < selection->row_to; i_row++)
    {
        for(i_col = selection->col_from-1; i_col < selection->col_to; i_col++)
        {
            get_string_from_cell(tab, i_row, i_col, &cell_str);
            cell_float = strtof(cell_str, &end);
            if((strlen(end) > 0) || (strlen(cell_str) == 0)) // no valid number found
            {
                free(cell_str);
                cell_str = NULL;
                continue;
            }

            if((!min_found) || (cell_float < min))
            {
                min = cell_float;
                selection_for_min.row_from = i_row+1;
                selection_for_min.row_to = i_row+1;
                selection_for_min.col_from = i_col+1;
                selection_for_min.col_to = i_col+1;
                min_found = true;
            }

            free(cell_str);
            cell_str = NULL;
        }
    } 

    // returning selection for min value
    selection->row_from = selection_for_min.row_from;
    selection->row_to = selection_for_min.row_to;
    selection->col_from = selection_for_min.col_from;
    selection->col_to = selection_for_min.col_to;

    if(!min_found)
        return ERROR_NO_MIN_FOUND;

    return 0;
}

// function searchs for max value in range and setting selection to that position
// if failed, returns error
int set_selection_to_max(table_t tab, selection_t *selection)
{
    selection_t selection_for_max; // row, col position for min 
    bool max_found = false; // flag if any min found
    float cell_float; // tmp float for cell_string value conversion

    // min is the first cell
    string_t cell_str = NULL;
    char *end;
    get_string_from_cell(tab, selection->row_from-1, selection->col_from-1, &cell_str);
    float max = strtof(cell_str, &end); 
    if(strlen(end) == 0) // valid number found, no need to find the first one
    {
        max_found = true;
    } // otherwise we need to find first valid min number within range
    free(cell_str);
    cell_str = NULL;

    // setting min as first cell in selection
    selection_for_max.row_from = selection->row_from;
    selection_for_max.row_to = selection->row_from;
    selection_for_max.col_from = selection->col_from;
    selection_for_max.col_to = selection->col_from;

    // comparing min with other values in selection range
    int i_row, i_col;
    for(i_row = selection->row_from-1; i_row < selection->row_to; i_row++)
    {
        for(i_col = selection->col_from-1; i_col < selection->col_to; i_col++)
        {
            get_string_from_cell(tab, i_row, i_col, &cell_str);
            cell_float = strtof(cell_str, &end);
            if((strlen(end) > 0) || (strlen(cell_str) == 0)) // no valid number found
            {
                free(cell_str);
                cell_str = NULL;
                continue;
            }
            // checking higher value then max, or taking the first valid number
            if((!max_found) || (cell_float > max))
            {
                max = cell_float;
                selection_for_max.row_from = i_row+1;
                selection_for_max.row_to = i_row+1;
                selection_for_max.col_from = i_col+1;
                selection_for_max.col_to = i_col+1;
                max_found = true;
            }

            free(cell_str);
            cell_str = NULL;
        }
    } 

    // returning selection for min value
    selection->row_from = selection_for_max.row_from;
    selection->row_to = selection_for_max.row_to;
    selection->col_from = selection_for_max.col_from;
    selection->col_to = selection_for_max.col_to;

    if(!max_found)
        return ERROR_NO_MAX_FOUND;

    return 0;
}

// function will read string from command
int read_str_from_command(string_t current_command, int from_pos, string_t str)
{
    if(strlen(current_command) < 4)
        return ERROR_INVALID_NUMBER_OF_COMMANDS;

    if(from_pos < 0)
        return -1;

    str[0] = '\0'; // setting str to the empty value
    size_t i;
    for(i=from_pos; i < strlen(current_command); i++)
    {
        str[i - from_pos] = current_command[i];
    }
    str[i - from_pos] = '\0';
    return 0;
}

// function searchs for 'substring' 
// if success, returns position, if failed, returns error
int set_selection_to_find_str(table_t tab, selection_t *selection, string_t substring)
{
    string_t cell_string = NULL;
    int i_row, i_col; // iterators
    
    for(i_row = selection->row_from-1; i_row < selection->row_to; i_row++)
    {
        for(i_col = selection->col_from-1; i_col < selection->col_to; i_col++)
        {
            get_string_from_cell(tab, i_row, i_col, &cell_string);
            // searching for substring
            if(strstr(cell_string, substring))
            {
                free(cell_string);
                cell_string=NULL;
                selection->row_from = i_row+1;
                selection->row_to = i_row+1;
                selection->col_from = i_col+1;
                selection->col_to = i_col+1;
                return 0; 
            }
            free(cell_string);
            cell_string = NULL;
        }   
    }

    // no substring found, returning error
    fprintf(stderr, "ERROR: substring not found\n");

    return ERROR_NO_SUBSTRING_FOUND;
}

int delete_char_at_position(char char_array[], int pos)
{
    // copying a character at position i to the previous positon
    // new char_array will one character shorter
    // beggining of string remains the same so starting copying from pos position
	for(int i=pos; i < (int)strlen(char_array) ;i++)
	{
		char_array[i] = char_array[i+1];
	}
	return 0;
}

// setting commands for structure change
bool set_structure_change(string_t command, int *cmd_id)
{
    if(!strcmp(command, "irow"))
    {
        *cmd_id = CMD_IROW;
        return true;
    }

    if(!strcmp(command, "arow"))
    {
        *cmd_id = CMD_AROW;
        return true;
    }

    if(!strcmp(command, "drow"))
    {
        *cmd_id = CMD_DROW;
        return true;
    }

    if(!strcmp(command, "icol"))
    {
        *cmd_id = CMD_ICOL;
        return true;
    }

    if(!strcmp(command, "acol"))
    {
        *cmd_id = CMD_ACOL;
        return true;
    }

    if(!strcmp(command, "dcol"))
    {
        *cmd_id = CMD_DCOL;
        return true;
    }

    return false;
}

// setting commands for table content change
bool set_cell_content_change(string_t command, int *cmd_id)
{
    if(!strncmp(command, "set ", 4))
    {
        *cmd_id = CMD_SET;
        return true;
    }

    if(!strcmp(command, "clear"))
    {
        *cmd_id = CMD_CLEAR;
        return true;
    }

    if(!strncmp(command, "swap ", 5))
    {
        *cmd_id = CMD_SWAP;
        return true;
    }

    if(!strncmp(command, "sum ", 4))
    {
        *cmd_id = CMD_SUM;
        return true;
    }

    if(!strncmp(command, "avg ", 4))
    {
        *cmd_id = CMD_AVG;
        return true;
    }

    if(!strncmp(command, "count ", 6))
    {
        *cmd_id = CMD_COUNT;
        return true;
    }

    if(!strncmp(command, "len ", 4))
    {
        *cmd_id = CMD_LEN;
        return true;
    }

    return false;
}

// setting tmp variable commands
bool set_temporary_variables(string_t command, int *cmd_id)
{
    if(!strncmp(command, "def ", 4))
    {
        *cmd_id = CMD_TMP_DEF;
        return true;
    }

    if(!strncmp(command, "use ", 4))
    {
        *cmd_id = CMD_TMP_USE;
        return true;
    }

    if(!strncmp(command, "inc ", 4))
    {
        *cmd_id = CMD_TMP_INC;
        return true;
    }

    if(!strncmp(command, "[set", 4))
    {
        *cmd_id = CMD_TMP_SET;
        return true;
    }

    return false;
}

// formated testing table print
void fprintf_table(table_t tab, char* delim)
{
	fprintf(stderr, "(%d) Table data:\n", __LINE__);
	if((tab.data == NULL)) 
	{
		fprintf(stderr, " - empty\n");
		return ;
	}

	if(tab.data[0] == NULL)
	{
		fprintf(stderr, " -- empty\n");
		return ;
	}

	fprintf(stderr, "(%d) rows=%d cols=%d\n", __LINE__, tab.total_rows, tab.total_cols);
	int i=0, j=0;
	while( (i<tab.total_rows) )
	{
		j=0;
		while( j < tab.total_cols )
		{
			fprintf(stderr, "\t[%d][%d]:", i, j);

			// TODO: check delim char in cell content. If so, add " at the begining and at the end of string
			if (tab.data[i][j] != NULL)
			{
				fprintf(stderr, "%s", tab.data[i][j]);
				if(j != tab.total_cols-1)
				{
					fprintf(stderr, "%c", delim[0]);
				}	
			}
			else {
				fprintf(stderr, "NULL");
			}
			j++;
		}
		i++;
		//fprintf(stderr, "\n(%d) row completed\n", __LINE__);
		fprintf(stderr, "\n");		
	}

}

// releasing memmory allocated for table
void release_table(table_t *tab)
{
	if(tab == NULL)
	{
		return ;
	}

	if (tab->data == NULL)
	{
		return ;
	}

	for(int i_row=0; i_row < tab->total_rows; i_row++)
	{
		for(int i_col=0; i_col<tab->total_cols; i_col++)
		{
			free( tab->data[i_row][i_col] );
			tab->data[i_row][i_col] = NULL;
			
		}
		
		free ( tab->data[i_row] );
		tab->data[i_row] = NULL;
	}
	free(tab->data);
	tab->data=NULL;
	tab->total_cols=0;
	tab->total_rows=0;
}

int append_char(string_t *string, char ch)
{
	int i=0;
	if (*string == NULL)
	{
		*string = calloc(2, sizeof(char));
		i=0;
	} 
	else
	{
		// increasing size for memmory: 1char for 'ch' and 1 char for '\0'
		*string = realloc(*string, (strlen(*string) + 2) * sizeof(char));
		i = strlen(*string);
	}
	(*string)[i] = ch;
	(*string)[i+1] = '\0';

	return 0;
}

// function reads all line from file
int read_line_from_file(FILE *pfile, string_t *line)
{
	int c;
	while( ((c=fgetc(pfile)) != '\n') && (c != EOF) )
		append_char(line, c);

	return 0;
}

// function checks if entry string has ""
bool is_string_bordered_by_quots(string_t string)
{
	if(string == NULL)
		false;

	if(strlen(string) < 2)
		false;

	if((string[0] == '"') && (string[strlen(string)-1] == '"'))
		return true;

	return false;
}

int remove_quots(string_t *string)
{
	size_t i=0;
	for(i=0; i<strlen(*string)-2; i++)
	{
		(*string)[i] = (*string)[i+1];
	}
	(*string)[i]='\0';

	return 0;
}

// converting line (string) to row cells
// adding new row/cells to the table
int append_line_to_table(string_t line, char* delim, int *number_of_cols, table_t *tab, int i_row)
{
    *number_of_cols = 0;
    if(line == NULL || strlen(line) == 0)
    {
        tab->data[i_row]=0;
        return ERROR_EMPTY_LINE;
    }

    tab->data[i_row] = realloc(tab->data[i_row], sizeof(string_t));

    bool is_quot = false; // flag for quot accurance
    bool is_backslash = false; // flag for backslash, if exist the very next char is part of string
    int i=0; // line iterator
    int i_col = 0; // column iterator
    char *tmp_cell = NULL;

    do {
        // looking for quots to handle the cell correctly
        if(line[i] == '"')
		    is_quot = !is_quot;
		
        // skipping \ to store chars correctly in cells
		if(line[i] == '\\')
		{
            i++;
            is_backslash = true;        
        }	
        
        // cell is completed only we have both quots
        if(is_delim(line[i], delim) && (!is_quot) && (!is_backslash))
        {
            if(tmp_cell == NULL)
            {
                append_char(&tmp_cell, '\0');
            }   

            if(is_string_bordered_by_quots(tmp_cell))
				remove_quots(&tmp_cell);

            // reallocating the row for new cell, allocation cell for string, copying string to cell
            tab->data[i_row] = realloc(tab->data[i_row], (i_col+1)*sizeof(string_t)); 
            tab->data[i_row][i_col] = (string_t)calloc( (strlen(tmp_cell)+1), sizeof(char) );
            strncpy( (tab->data)[i_row][i_col], tmp_cell, strlen(tmp_cell));

            i_col++;
            free(tmp_cell);
            tmp_cell=NULL;
            i++;

            is_quot = false;	// cell string is completed, resetting is_quot flag
            continue;
        }

        if((line[i] == '\0') || (line[i] == '\n'))
        {
            if(tmp_cell == NULL)
                append_char(&tmp_cell, '\0');
                
            if(is_string_bordered_by_quots(tmp_cell))
                remove_quots(&tmp_cell);

            // realloc just to make sure I have memmory in case od just 1 cell
            tab->data[i_row] = realloc(tab->data[i_row], (i_col+1)*sizeof(string_t));
            tab->data[i_row][i_col] = (string_t)calloc( (strlen(tmp_cell)+1), sizeof(char));
            strncpy(tab->data[i_row][i_col], tmp_cell, strlen(tmp_cell));

            free(tmp_cell);
            tmp_cell=NULL;
            i_col++;
            break;
        }

        append_char(&tmp_cell, line[i]);
        i++;
        is_backslash = false; // reseting flag
    } while(true);

    if(*number_of_cols < i_col)
        *number_of_cols = i_col;

    return 0;
}

int copy_row_content(table_t *table, int row_to, int row_from)
{
    (void)row_to;
    int i_col; // iterator for cells
    string_t tmp_string = NULL;
    for(i_col = 0; i_col < table->total_cols; i_col++)
    {
        tmp_string = calloc( strlen(table->data[row_from][i_col])+1, sizeof(char));
        strncpy(tmp_string, table->data[row_from][i_col], strlen(table->data[row_from][i_col])); 

        free(table->data[row_to][i_col]);
        table->data[row_to][i_col] = NULL;
        table->data[row_to][i_col] = calloc((strlen(tmp_string)+1), sizeof(char));
        strncpy(table->data[row_to][i_col], tmp_string, strlen(tmp_string));

        free(tmp_string);
        tmp_string = NULL;
    }
    return 0;
}

int allocate_memmory_for_next_row(table_t *tab, int i_row)
{
	// allocation memmory for a new line (row_t)
	tab->data = realloc(tab->data, (i_row+1)*sizeof(row_t));
	if(tab->data == NULL)
		return -1;

	tab->data[i_row] = NULL;

	// allocating memmory for the 1st cell in the row
	tab->data[i_row] = realloc( tab->data[i_row], (1)*sizeof(string_t) );
	if(tab->data[i_row] == NULL)
		return -1;
		
	tab->data[i_row][0] = NULL;

	return 0;
}

int delete_col(table_t *table, selection_t *selection)
{
    if( selection->col_from != selection->col_to )
        return -1;

    int i_row, i_col;
    // copying cells to 'move' deleted column to the end of row
    for(i_row = 0; i_row<table->total_rows; i_row++)
    {
        for(i_col = selection->col_from-1; i_col < table->total_cols-1; i_col++)
        {
            string_t tmp;
            get_string_from_cell(*table, i_row, i_col+1, &tmp);
            set_string_to_cell(table, i_row, i_col, tmp);
            free(tmp);
            tmp=NULL;
        }        
    }

    // deleting last column:
    i_col = table->total_cols-1;
    for(i_row = 0; i_row<table->total_rows; i_row++)
    {
        // releasing cell
        free(table->data[i_row][i_col]);
        table->data[i_row][i_col] = NULL;

        // reallocating row as we have 1 less cell
        table->data[i_row] = realloc(table->data[i_row], (i_col)*sizeof(string_t));

    }
    table->total_cols--;
    selection->col_to--;

    return 0;
}

int delete_row(table_t *table, selection_t *selection)
{
    if( selection->row_from != selection->row_to)
        return -1;

    int i_row;
    int i_col;
    // copy content from 
    for(i_row=selection->row_to-1; i_row<table->total_rows-1; i_row++)
    {
        for(i_col=0; i_col<table->total_cols; i_col++)
        {
            string_t tmp = NULL;
            get_string_from_cell(*table, i_row+1, i_col, &tmp);
            set_string_to_cell(table, i_row, i_col, tmp);
            free(tmp);
            tmp = NULL;
        }
    }

    i_row = table->total_rows-1;
    // removing last row:
    for(i_col = 0; i_col < table->total_cols; i_col++)
    {
        free(table->data[i_row][i_col]);
        table->data[i_row][i_col] = NULL; 
    }

    free(table->data[i_row]);
    table->data = realloc(table->data, (table->total_rows-1) * sizeof(row_t));

    // adjusting row size
    selection->row_from--;
    selection->row_to--;
    table->total_rows--;

    return 0;
}

int append_empty_col(table_t *table, selection_t selection)
{
    // appending new cols at the end
    append_cols_to_tab(table, table->total_cols+1, 1);
    table->total_cols++;

    for(int i_row=0; i_row<table->total_rows; i_row++)
    {
        // copying cells from the last col to the selection.col_to
        for(int i_col= table->total_cols-2; i_col >= selection.col_to; i_col--)
        {
            string_t cell_str = NULL;
            get_string_from_cell(*table, i_row, i_col, &cell_str);
            set_string_to_cell(table, i_row, i_col+1, cell_str);

            free(cell_str);
            cell_str=NULL;
        }
    }

    // cleaning column selection.col_to
    for (int i_row=0; i_row<table->total_rows; i_row++)
    {
        set_string_to_cell(table, i_row, selection.col_to, "");
    }

    return 0;
}

int insert_empty_col(table_t *table, selection_t selection)
{
    // appending new cols at the end
    append_cols_to_tab(table, table->total_cols+1, 1);
    table->total_cols++;

    for(int i_row=0; i_row<table->total_rows; i_row++)
    {
        // copying cells from the last col to the selection.col_from
        for(int i_col= table->total_cols-2; i_col >= selection.col_from-1; i_col--)
        {
            string_t cell_str = NULL;
            get_string_from_cell(*table, i_row, i_col, &cell_str);
            set_string_to_cell(table, i_row, i_col+1, cell_str);

            free(cell_str);
            cell_str=NULL;
        }
    }

    // cleaning column selection.col_to
    for (int i_row=0; i_row<table->total_rows; i_row++)
        set_string_to_cell(table, i_row, selection.col_from-1, "");

    return 0;
}

// function add empty row after selection (row to)
int append_empty_row(table_t *table, selection_t selection)
{
    // append empty row at the end
    int last_row = table->total_rows;
    allocate_memmory_for_next_row(table, last_row);
    table->data[last_row] = realloc(table->data[last_row], (table->total_cols)*sizeof(string_t));
    int i_col; // iterator stlpcov
    for(i_col = 0; i_col < table->total_cols; i_col++)
    {
        table->data[last_row][i_col] = calloc(1, sizeof(char));
        table->data[last_row][i_col][0] = '\0';
    }
    table->total_rows++;

    // copy row content till selection.row_to, prechadzam cez riadky
    int i_row;
    for(i_row = table->total_rows - 1; i_row > selection.row_to ; i_row--)
    {
        copy_row_content(table, i_row, i_row-1);
    }

    // empty row cells 
    int j;
    for(j = 0; j < table->total_cols; j++)  
    {
        table->data[selection.row_to][j] = (string_t)realloc(table->data[selection.row_to][j], sizeof(char));
        strncpy( table->data[selection.row_to][j], "", 1);
    }   

    return 0;
}

// function appends empty col to the table from 'current_col' column, at row 'current_row'
int append_cols_to_tab(table_t *tab, int current_col, int current_row)
{
    if(tab == NULL)
        return -1;

    if(current_col < tab->total_cols)
    {
        int i_col = 0;
        for(i_col = current_col; i_col < tab->total_cols; i_col++)
        {
            // realocating row for cell, alloction cell for string, copying string to cell
            tab->data[current_row] = realloc(tab->data[current_row], (i_col+1)*sizeof(string_t)); 
            tab->data[current_row][i_col] = (string_t)calloc( 1, sizeof(char) );
            strncpy( (tab->data)[current_row][i_col], "", 1);
        }
        return 0;
    }

    // current col > tab->total_cols
    int i_row; // row counter
    int i_col; // col counter
    for(i_row = 0; i_row < tab->total_rows; i_row++)
    {
        for(i_col = tab->total_cols; i_col < current_col; i_col++)
        {
            // reallocating row for the new cell, alocating cell for the string, copying string to the cell
            tab->data[i_row] = realloc(tab->data[i_row], (i_col+1)*sizeof(string_t)); 
            tab->data[i_row][i_col] = (string_t)calloc( 1, sizeof(char) );
            strncpy( (tab->data)[i_row][i_col], "", 1);
        }
    } 
    return 0;
}

// reads file based on delim parse to the table cells/rows  
int fill_table_from_file(table_t *tab, char *file_name, char* delim)
{
    FILE *file = fopen(file_name, "r");
    tab->data=NULL;
    tab->total_cols=0;
    tab->total_rows=0;

    string_t line = NULL;
    int i_row = 0;
    int current_col; // cols in current row

    // allocation memory for a new lines (row_t)
    tab->data = calloc(1, sizeof(row_t));

    while(true)
    {
        line = NULL;
        read_line_from_file(file, &line);

        if(feof(file))
            break;
        
        if((line == NULL) || (strlen(line) == 0))
            break;
        
        //allocating memmory for next row
        allocate_memmory_for_next_row(tab, i_row);
        
        current_col = 0; // assuming there is just one string in the 'line' (only one column)

        // apending 'line' to the table
        append_line_to_table(line, delim, &current_col, tab, i_row);

        // checking if we need to append cells in the row
        // making sure we add cols to the rows if missing
        if(i_row == 0)
        {
            tab->total_cols = current_col;
        }
        else 
        {
            // i_row > 0
            if(current_col != tab->total_cols)
            {
                append_cols_to_tab(tab, current_col, i_row);
                if(current_col > tab->total_cols)
                    tab->total_cols = current_col; 
            }
        }
                 
        tab->total_rows = i_row + 1; // total rows update
        i_row++;

        free(line);
        line = NULL;
    }

    fclose(file);
    return 0;
}

int insert_empty_row(table_t *table, selection_t selection)
{
    int i_row; // row counter
    int i_col; // col counter
    for(i_row = selection.row_from -1; i_row < selection.row_to; i_row++)
    {
        for(i_col = selection.col_from -1; i_col < selection.col_to; i_col++)
        {
            // extending number of rows +1

            // allocation memmory for a new line (row_t) at the end
            int last_row = table->total_rows;
            table->data = realloc(table->data, (last_row+1)*sizeof(row_t));
	        if(table->data == NULL)
		        return -1;

            table->data[last_row] = NULL;

            // allocating memmory for all cells in the row
            table->data[last_row] = realloc( table->data[last_row], (table->total_cols)*sizeof(string_t) );
            if(table->data[last_row] == NULL)
                return -1;

            int j;
            for(j = 0; j < table->total_cols; j++)
            {
                table->data[last_row][j] = NULL;
            }    

            table->total_rows++;

            //copy_row_content(table, 4, 0);
            int i;
            for(i = table->total_rows - 1; i > 0; i-- )
            {
                copy_row_content(table, i, i-1);
            }

            // setting row_from with empty cells
            for(j = 0; j < table->total_cols; j++)  
            {
                table->data[i_row][j] = (string_t)realloc(table->data[i_row][j], sizeof(char));
                strncpy( table->data[i_row][j], "", 1);
            }        
        }
    }
    return 0;
}

// allocating appropriate memmory
// updating string to allocated cell
int set_string_to_cell(table_t *table, int i_row, int i_col, string_t cell_string)
{
    free(table->data[i_row][i_col]);
    table->data[i_row][i_col] = NULL;
    
    table->data[i_row][i_col] = calloc(strlen(cell_string) +1,sizeof(char));
    strncpy(table->data[i_row][i_col], cell_string, strlen(cell_string));
    return 0;
}

// set STR implementation
int update_table_set_str(table_t *tab, selection_t selection, string_t str)
{
    int i_row,i_col;
    for(i_row = selection.row_from -1; i_row < selection.row_to; i_row++)
    {
        for(i_col = selection.col_from -1; i_col < selection.col_to; i_col++)
        {
            set_string_to_cell(tab, i_row, i_col, str);
        }
    }
    return 0;
}

// swap implementation
int update_table_swap(table_t *table, selection_t source_selection, string_t target_selection_string)
{
    selection_t target_selection;
    select_the_cells_in_table(table, &target_selection, target_selection_string);
    int i_row, i_col;
    string_t source_string = NULL;
    string_t target_string = NULL;
    for(i_row = source_selection.row_from -1; i_row < source_selection.row_to; i_row++)
    {
        for(i_col = source_selection.col_from -1; i_col < source_selection.col_to; i_col++)
        {
            get_string_from_cell(*table, i_row, i_col, &source_string);
            get_string_from_cell(*table, target_selection.row_from -1, target_selection.col_from -1, &target_string);
            
            set_string_to_cell(table, i_row, i_col, target_string);
            set_string_to_cell(table, target_selection.row_from -1, target_selection.col_from -1, source_string);

            free(source_string);
            source_string = NULL;
            free(target_string);
            target_string = NULL;
        }
    }
    return 0;
}

// function returns 10^exponent
float my_pow_10(int exponent)
{
    float exp=1.0;

    if (exponent > 0)
    {
        for(int i=1; i<=exponent; i++)
            exp *= 10;
    }
    else if (exponent < 0)
    {
        for(int i=-1; i>=exponent; i--)
            exp /= 10;
    }
    return exp;
}

// function converts float variable "f" to string "string"
// converting from 10^8 till 10^-4
int float_to_string(float f, char string[])
{
	int i=0;

    // treating negative float number: string[0] = '-'
	if(f<0)
	{
		string[i] = '-';
		f = -1*f;
		i++;
	}

	float tmp = f;
	int exponent;
	int digit;

    // step 1: converting positive figures, starting 10^8 till 10^0
	for(exponent=8; exponent >= 0 ; exponent--)
	{
        // dividing with appropriate divider will get 1 single digit
		tmp = f/my_pow_10(exponent);

        // truncating  after decimal point and keep result in "digit"
		tmp = (float)((int)tmp);
		digit = (int)tmp;

        // after successful getting "digit", reducing f number for the size we already converted
        // already converted: digit*10^exponent
        // "f" will have lower exponent in the next round
		f = f-digit*my_pow_10(exponent);

        // making ASCII '0'
		string[i] = digit + '0';
		string[i+1] = '\0';
		i++;
	}

    // treating decimal point numbers, if none result is like %g format
	if(f>0)
	{
		string[i] = '.';
		i++;
        string[i+1] = '\0';

        // converting from 10^-1 till 10^-4
		for(exponent=-1; exponent>=-4; exponent--)
		{
			tmp = f/my_pow_10(exponent);
			tmp = (float)((int)tmp);
			digit = (int)tmp;
			f= f - digit*my_pow_10(exponent);

			string[i] = digit + '0';
			string[i+1] = '\0';
			i++;
		}

        // removing '0' at the end 
        i = strlen(string) -1;
        while(string[i] != '.')
        {
            if(string[i] != '0')
                break;

            // decimal point

            if(string[i] == '0')
            {
                delete_char_at_position(string, i);
            }
            i--;
        }

	}

	// deleting '0' from string in front of the number
	// expecting, there are some (flag still_have_zero_to_delete)
	bool still_have_zero_to_delete = true;
	i=0;

	if(string[0] == '-') // negative numbers have '-' at string[0], zero deletion will start from string[1]
		i=1;

	while(string[i] != '\0')
	{
		if(!still_have_zero_to_delete) // stop condition
			break;

		if(string[i] == '0')
		{
			delete_char_at_position(string, i);
			continue;
		}
		else // we already deleted all 0, only other digits remains
		{
			still_have_zero_to_delete=false;
		}
		i++;
	}
    
	return 0;
}

// sum implementation
int update_table_sum(table_t *table, selection_t source_selection, string_t target_sel_str)
{
    selection_t target_selection;
    select_the_cells_in_table(table, &target_selection, target_sel_str);
    float sum = 0.0;
    int i_row, i_col;
    char *end;
    string_t source_string = NULL;
    char target_string[MAX_SIZE] = "";
    for(i_row = source_selection.row_from -1; i_row < source_selection.row_to; i_row++)
    {
        for(i_col = source_selection.col_from -1; i_col < source_selection.col_to; i_col++)
        {
            get_string_from_cell(*table, i_row, i_col, &source_string); // value from source string
            float cell_value;
            end = NULL;
            cell_value = strtof(source_string, &end);

            if(strlen(end) > 0) // not valid number
            {
                free(source_string);
                source_string = NULL;
                continue;
            }    
            
            sum += cell_value;
           
            free(source_string);
            source_string = NULL;
        }

    }    
    float_to_string(sum, target_string);
    set_string_to_cell(table, target_selection.row_from -1, target_selection.col_from -1, target_string);

    return 0;
}

// count implementation
int update_table_count(table_t *table, selection_t source_selection, string_t target_sel_str)
{
    selection_t target_selection;
    select_the_cells_in_table(table, &target_selection, target_sel_str);
    int count = 0;
    int i_row, i_col;
    string_t source_string = NULL;
    char target_string[MAX_SIZE] = "";
    for(i_row = source_selection.row_from -1; i_row < source_selection.row_to; i_row++)
    {
        for(i_col = source_selection.col_from -1; i_col < source_selection.col_to; i_col++)
        {
            get_string_from_cell(*table, i_row, i_col, &source_string);

            if(source_string[0] != '\0')
                count++;

            free(source_string);
            source_string = NULL;
        }
    }
    float_to_string((float)count, target_string);
    set_string_to_cell(table, target_selection.row_from-1, target_selection.col_from-1, target_string);

    return 0;
}

// len implementation
int update_table_len(table_t *table, selection_t source_selection, string_t target_sel_str)
{
    selection_t target_selection;
    select_the_cells_in_table(table, &target_selection, target_sel_str);
    int len = 0;
    int i_row, i_col;
    string_t source_string = NULL;
    char target_string[MAX_SIZE] = "";
    for(i_row = source_selection.row_from -1; i_row < source_selection.row_to; i_row++)
    {
        for(i_col = source_selection.col_from -1; i_col < source_selection.col_to; i_col++)
        {
            get_string_from_cell(*table, i_row, i_col, &source_string);

            len += strlen(source_string);

            free(source_string);
            source_string = NULL;
        }
    }
    float_to_string((float)len, target_string);
    set_string_to_cell(table, target_selection.row_from-1, target_selection.col_from-1, target_string);

    return 0;
}

// avg implementation
int update_table_avg(table_t *table, selection_t source_selection, string_t target_sel_str)
{
    selection_t target_selection;
    select_the_cells_in_table(table, &target_selection, target_sel_str);
    int count = 0;
    float sum = 0, avg = 0, cell_value;
    int i_row, i_col;
    char *end;
    string_t source_string = NULL;
    char target_string[MAX_SIZE] = "";
    for(i_row = source_selection.row_from -1; i_row < source_selection.row_to; i_row++)
    {
        for(i_col = source_selection.col_from -1; i_col < source_selection.col_to; i_col++)
        {
            get_string_from_cell(*table, i_row, i_col, &source_string);

            cell_value = strtof(source_string, &end);
                        
            if(strlen(end) > 0) // not valid number
            {
                free(source_string);
                source_string = NULL;
                continue;        
            }    
            
            sum += cell_value;

            if(strlen(source_string) > 0)
                count++;


            free(source_string);
            source_string = NULL;
        }
    }

    if(count == 0)
        return ERROR_AVG_ZERO_COUNTS;

    avg = sum/count;
    float_to_string((float)avg, target_string);
    set_string_to_cell(table, target_selection.row_from-1, target_selection.col_from-1, target_string);

    return 0;
}

int select_the_cells_in_table(table_t *tab, selection_t *selection, string_t command)
{
    if(!strcmp(command, "[min]"))
    {
        // selection min
        if(set_selection_to_min(*tab, selection) == ERROR_NO_MIN_FOUND)
            return ERROR_NO_MIN_FOUND;
        
        return 0;
    }

    if(!strcmp(command, "[max]"))
    {
        // selection max
        if(set_selection_to_max(*tab, selection) == ERROR_NO_MAX_FOUND)
            return ERROR_NO_MAX_FOUND;
        return 0;
    }

    if(!strncmp(command, "[find", 5))
    {
        // selection find
        char str[MAX_SIZE] = "";
        read_str_from_command(command, 6, str);
        delete_char_at_position(str, strlen(str)-1);
        if(set_selection_to_find_str(*tab, selection, str) == ERROR_NO_SUBSTRING_FOUND)
            return ERROR_NO_SUBSTRING_FOUND;
        return 0;
    }

    // finding [ in cmd_sequence
    int i;
    for(i=0; (command[i] != '\0') && (command[i] != '['); i++)
    {
        ;
    }
    // number starts at the next position
    i++;

    // handlin row_from
    int row_from = get_value_from_command(command, &i, ",");
    if(row_from == ERROR_SELECTION_CONVERSION_FAILED)
    {
        return ERROR_SELECTION_CONVERSION_FAILED;
    }

    if(row_from == ALL_ROW_COL_SELECTED) // picking whole column
    {
        selection->row_from = 1;
        selection->row_to = tab->total_rows;
    }
    else
    {
        // [R,C], in case of one row, values from-to are the same
        selection->row_to = selection->row_from = row_from;
    }

    // skipping comma
    int j = i+1;

    // handling col_from
    int col_from = get_value_from_command(command, &j, "],");
    if(col_from == ERROR_SELECTION_CONVERSION_FAILED)
    {
        return ERROR_SELECTION_CONVERSION_FAILED;
    }

    if(col_from == ALL_ROW_COL_SELECTED)
    {
        // picking all row
        selection->col_from = 1;
        selection->col_to = tab->total_cols;
    }
    else
    {
        // [R,C] in case of one col, the from-to values are the same
        selection->col_to = selection->col_from = col_from;
    }

    int l=j;
    if(command[l] == ',')
    {
        // skipping coma
        l++;

        int row_to = get_value_from_command(command, &l, ",");

        if(row_to == ERROR_SELECTION_CONVERSION_FAILED)
            return ERROR_SELECTION_CONVERSION_FAILED;

        if(row_to == ALL_ROW_COL_SELECTED)
            selection->row_to = tab->total_rows;
        else
            selection->row_to = row_to;

        // handling C2
        l++; // skipping comma, moving to next character

        int col_to = get_value_from_command(command, &l, "]");

        if(col_to == ERROR_SELECTION_CONVERSION_FAILED)
            return ERROR_SELECTION_CONVERSION_FAILED;

        if(col_to == ALL_ROW_COL_SELECTED)
            selection->col_to = tab->total_cols;
        else
            selection->col_to = col_to;

    }

    // checking selection and table size
    if((selection->row_from < 1) ||  (selection->col_from < 1))
        return ERROR_NOT_VALID_SELECTION;

    // if selection exceedes table borders, we will add rows and cols
    if(selection->row_to > tab->total_rows)
    {
        int i_row;
        for(i_row = tab->total_rows; i_row < selection->row_to; i_row++)
        {
            selection_t sel;
            sel.row_to = tab->total_rows;
            sel.row_from = tab->total_rows;
            sel.col_from = sel.col_to = 1;
            append_empty_row(tab, sel);
        }
    }

    if(selection->col_to > tab->total_cols)
    {
        // adding cols
        append_cols_to_tab(tab, selection->col_to, 1);
        tab->total_cols = selection->col_to;
    }

    return 0;
}

// returns true if any delim character in string appears
bool contains_cell_any_delim(char *string, char * delim)
{
	size_t i;
	for(i=0; i<strlen(string); i++)
	{
		if(is_delim(string[i], delim))
			return true;
	}
	return false;
}

// handling "" when priting cell
void print_cell(char *string, bool quoted)
{
	if(string == NULL)
		return ;
	
	size_t i;
	
	if (quoted)
		printf("\"");

	for(i=0; i<strlen(string); i++)
	{
		if(string[i] == '"')
			printf("\\");
		printf("%c", string[i]);
	}

	if (quoted)
		printf("\"");

}

void print_table(table_t tab, char* delim)
{
	if((tab.data == NULL)) 
		return ;

	if(tab.data[0] == NULL)
		return ;

	int i=0, j=0;
	while( (i<tab.total_rows) )
	{
		j=0;
		while( j < tab.total_cols )
		{
			// check delim char in cell content
            // if so, add " at the begining and at the end of string
			if(contains_cell_any_delim(tab.data[i][j], delim))
				print_cell(tab.data[i][j], true);
			else
				print_cell(tab.data[i][j], false);
			

			if(j != tab.total_cols-1)
				printf("%c", delim[0]);
			
			j++;
		}
		i++;
		printf("\n");		
	}

}
