#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

#define CMD_LEN 1024

enum EndPoint {
    EP_HOSTNAME,
    EP_LOAD,
    EP_CPUNAME,
    EP_NONE
};

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int get_processor_utilization(char *utilization_string){

    char *cmd_non_idle = "cat /proc/stat | grep cpu | head -n 1 | awk ' { sum += ($2 + $3 + $4 + $7 + $8 + $9)}; END { print sum }' ";
    char *cmd_idle = "cat /proc/stat | grep cpu | head -n 1 | awk ' { sum += ($5 + $6)}; END { print sum }' ";

    FILE *fp;
    if ((fp = popen(cmd_non_idle, "r")) == NULL) 
        error("Error opening pipe!\n");
    while (fgets(utilization_string, CMD_LEN, fp) != NULL) {
        // fprintf(stderr, "cpu_non_idle_1: '%s'\n", utilization_string);
    }
    utilization_string[strlen(utilization_string)-1] = '\0';
    long non_idle_1 = atoi(utilization_string);

    if ((fp = popen(cmd_idle, "r")) == NULL) 
        error("Error opening pipe!\n");
    while (fgets(utilization_string, CMD_LEN, fp) != NULL) {
        // fprintf(stderr, "cpu_idle_1: '%s'\n", utilization_string);
    }
    utilization_string[strlen(utilization_string)-1] = '\0';
    long idle_1 = atoi(utilization_string);

    long total_1 = non_idle_1 + idle_1;

    sleep(2);

    if ((fp = popen(cmd_non_idle, "r")) == NULL) 
        error("Error opening pipe!\n");
    while (fgets(utilization_string, CMD_LEN, fp) != NULL) {
        // fprintf(stderr, "cpu_non_idle_2: '%s'\n", utilization_string);
    }
    utilization_string[strlen(utilization_string)-1] = '\0';
    long non_idle_2 = atoi(utilization_string);
    if ((fp = popen(cmd_idle, "r")) == NULL) 
        error("Error opening pipe!\n");
    while (fgets(utilization_string, CMD_LEN, fp) != NULL) {
        // fprintf(stderr, "cpu_idle_2: '%s'\n", utilization_string);
    }
    utilization_string[strlen(utilization_string)-1] = '\0';
    long idle_2 = atoi(utilization_string);

    long total_2 = non_idle_2 + idle_2;

    float total_diff = (float)(total_2 - total_1);
    //float non_idle_diff = (float)(non_idle_2 - non_idle_1);
    float idle_diff = (float)(idle_2 - idle_1);
    
    float cpu_u = 100*(total_diff - idle_diff) / total_diff;
    
    // fprintf(stderr, "(%d) total_d: %f, idle_d: %f\n", __LINE__, total_diff, idle_diff);
    // fprintf(stderr, "(%d) cpu_u: %f\n", __LINE__, cpu_u);

    if (pclose(fp)) {
        fprintf(stderr, "Command not found or exited with error status\n");
        return -1;
    }

    sprintf(utilization_string, "%.0f", cpu_u);
    strcat(utilization_string, "%");

    return 0;
}

int get_hostname(char *hostname){
    char *cmd = "cat /proc/sys/kernel/hostname";

    FILE *fp;
    if ((fp = popen(cmd, "r")) == NULL) {
        printf("Error opening pipe!\n");
        return -1;
    }

    while (fgets(hostname, CMD_LEN, fp) != NULL) {
        //fprintf(stderr, "'%s'\n", cpuname);
    }

    hostname[strlen(hostname)-1] = '\0';

    if (pclose(fp)) 
        error("Error: Command not found or exited with error status\n");

    return 0;
}

int get_cpuname(char *cpuname) {

    char *cmd = "cat /proc/cpuinfo | grep \"model name\" | head -n 1 | awk -F  \":\" '{print $2}'";

    FILE *fp;
    if ((fp = popen(cmd, "r")) == NULL) 
        error("Error opening pipe!\n");

    while (fgets(cpuname, CMD_LEN, fp) != NULL) {
        fprintf(stderr, "'%s'\n", cpuname);
    }

    // removing 1 space at the beginning
    for(size_t i=1; i<strlen(cpuname); i++)
        cpuname[i-1] = cpuname[i];
    // removing \n at the end
    cpuname[strlen(cpuname)-1] = '\0';

    if (pclose(fp)) 
        error("Error: Command not found or exited with error status\n");

    return 0;
}

int extract_endpoint(char *buffer) {
    if (buffer == NULL)
        return EP_NONE;

    char endpoint[CMD_LEN] = {0};
    // extrahujem substring iba od prvej medzery po dalsiu medzeru
    for (size_t i = 4; (i < strlen(buffer)) && (buffer[i] != ' '); i++) {
        endpoint[i-4] = buffer[i];
        
        // toto asi netreba, kedze sme na zaciatku dali {0}  
        endpoint[i-3] = '\0';
    }
    fprintf(stderr, "(%d) Extracted endpoint: '%s'\n", __LINE__, endpoint);

    if (!strcmp("/cpu-name", endpoint))
        return EP_CPUNAME;

    if (!strcmp("/hostname", endpoint))
        return EP_HOSTNAME;

    if (!strcmp("/load", endpoint))
        return EP_LOAD;

    return EP_NONE;
}

int main(int argc, char *argv[])
{
    fprintf(stderr, "(%d) Server started.\n", __LINE__);

    int server_fd, new_socket_fd, portno;
    socklen_t clilen;
    struct sockaddr_in server_addr, cli_addr;
    int n;
    if (argc < 2) {
        fprintf(stderr,"Error: No port provided\n");
        fprintf(stderr, "Usage: ./hinfosvc <port-number>\n");
        exit(1);
    }
    // create a socket
    // socket(int domain, int type, int protocol)
    // Options for communication domain: 
    // * AF_INET (IP), 
    // * AF_INET6 (IPv6), 
    // * AF_UNIX (local channel, similar to pipes), 
    // * AF_ISO (ISO protocols), 
    // * AF_NS (Xerox Network Systems protocols)
    // Options for type:
    // * SOCK_STREAM (virtual circuit service), 
    // * SOCK_DGRAM (datagram service), 
    // * SOCK_RAW (direct IP service)
    server_fd =  socket(AF_INET, SOCK_STREAM, 0);
    if (server_fd < 0) {
        error("Error: Opening socket failed\n");
        return EXIT_FAILURE;
    }

    // clear address structure
    // bzero((char *) &server_addr, sizeof(server_addr));
    memset((char *)&server_addr, 0, sizeof(server_addr));

    portno = atoi(argv[1]);

    /* setup the host_addr structure for use in bind call */
    // server byte order
    server_addr.sin_family = AF_INET;  

    // automatically be filled with current host's IP address
    // 0.0.0.0 -> INADDR_ANY
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);  

    // convert integer value for port into the network byte order
    server_addr.sin_port = htons(portno);

    // bind(int fd, struct sockaddr *local_addr, socklen_t addr_length)
    // bind() passes file descriptor, the address structure, 
    // and the length of the address structure
    // This bind() call will bind  the socket to the current IP address on port, portno
    if (bind(server_fd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) 
        error("Error: Failed on binding");

    // This listen() call tells the socket to listen to the incoming connections.
    // The listen() function places all incoming connection into a backlog queue
    // until accept() call accepts the connection.
    // maximum size for pending connections: 5.
    listen(server_fd, 5);

    clilen = sizeof(cli_addr);

    while (1) {

        // This accept() function will write the connecting client's address info 
        // into the the address structure and the size of that structure is clilen.
        // The accept() returns a new socket file descriptor for the accepted connection.
        // So, the original socket file descriptor can continue to be used 
        // for accepting new connections while the new socker file descriptor is used for
        // communicating with the connected client.
        new_socket_fd = accept(server_fd, (struct sockaddr *) &cli_addr, &clilen);
        if (new_socket_fd < 0) 
            error("Error: Failure on accepting the socket");

        printf("Server info: Received connection from port %d\n", ntohs(cli_addr.sin_port));


        // HTTP/1.1 200 OK\r\n
        // Content-Type: text/plain\r\n
        char response_message[1024] = "HTTP/1.1 ";
        

        char buffer[CMD_LEN] = {0};
        n = read(new_socket_fd, buffer, 1024);
        if (n < 0) 
            error("Error: Reading from socket failed");
        printf("Incoming message:\n'%s'\n",buffer);
        
        char response_answer[50] = {0};
        switch (extract_endpoint(buffer)) {
            case EP_HOSTNAME:
                get_hostname(response_answer);
                fprintf(stderr, "(%d) Responding .hostname. \n", __LINE__);
                strcat(response_message, "200 OK\r\nContent-Type: text/plain;\r\n");
                break;
            case EP_CPUNAME:
                get_cpuname(response_answer);
                fprintf(stderr, "(%d) Responding .cpu-name. \n", __LINE__);
                strcat(response_message, "200 OK\r\nContent-Type: text/plain;\r\n");
                break;
            case EP_LOAD:
                get_processor_utilization(response_answer);
                fprintf(stderr, "(%d) Responding .cpu load. \n", __LINE__);
                strcat(response_message, "200 OK\r\nContent-Type: text/plain;\r\n");
                break;
            default:
                // 400 Bad Request
                strcat(response_message, "400 Bad Request\r\nContent-Type: text/plain;\r\n");
                printf("\n .neznamy endpoint. \n");
        }
        fprintf(stderr, "(%d) '%s'\n", __LINE__, response_answer);
        // Content-Length: <size in # bytes>\r\n\r\n
        // <content>
        char response_content[255] = "Content-Length: ";
        strcat(response_message, response_content);
        char content_size[10] = {0};
        sprintf(content_size, "%d", (int)strlen(response_answer));
        strcat(response_message, content_size);
        strcat(response_message, "\r\n\r\n");
        strcat(response_message, response_answer);
        strcat(response_message, "\r\n\r\n");
        fprintf(stderr, "(%d) response: '%s'\n", __LINE__, response_message);

        write(new_socket_fd, response_message, strlen(response_message));

        close(new_socket_fd);

    }

    close(server_fd);
    fprintf(stderr, "(%d) Server ended.\n", __LINE__); 
    
    return 0; 
}