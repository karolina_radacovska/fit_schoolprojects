#/bin/bash
# source: https://www.linuxhowtos.org/System/procstat.htm
# source: https://stackoverflow.com/questions/21620406/how-do-i-pause-my-shell-script-for-a-second-before-continuing
# source: https://stackoverflow.com/questions/11356330/how-to-get-cpu-usage?noredirect=1&lq=1
# source: https://stackoverflow.com/questions/23367857/accurate-calculation-of-cpu-usage-given-in-percentage-in-linux
# source: https://www.kernel.org/doc/Documentation/filesystems/proc.txt

echo "Non-idle:"
cat /proc/stat | grep cpu | head -n 1 | awk ' { sum += ($2 + $3+ $4 + $7 + $8 + $9)}; END { print sum }' 

echo "idle:"
cat /proc/stat | grep cpu | head -n 1 | awk ' { sum += ( $5 + $6 )}; END { print sum }'

