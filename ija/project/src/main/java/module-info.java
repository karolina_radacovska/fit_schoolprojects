module cz.ija {
    requires javafx.controls;
    requires javafx.fxml;
    requires com.google.gson;

    opens cz.ija to javafx.fxml;
    opens cz.ija.uml to com.google.gson;
    exports cz.ija.uml;
    exports cz.ija;
}