package cz.ija.uml;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Class for relations.
 */
public class UMLRelation extends Element {

    private static final AtomicInteger ID_GENERATOR = new AtomicInteger(0);
    private final int id;
    private final UMLClass firstClass;
    private final UMLClass secondClass;
    private final RelationType type;
    private RelationMultiplicity first;
    private RelationMultiplicity second;

    /**
     * Constructor for UMLRelation.
     * @param type type of the relation
     * @param name name
     * @param first first class to which is relation connected
     * @param second second class to which is relation connected
     */
    public UMLRelation(RelationType type, String name, UMLClass first, UMLClass second) {
        super(name);
        this.type = type;
        this.firstClass = first;
        this.secondClass = second;
        this.id = ID_GENERATOR.getAndIncrement();

    }

    /**
     *
     * Constructor for UMLRelation.
     * @param type type of the relation
     * @param name name
     * @param firstClass first class to which is relation connected
     * @param secondClass second class to which is relation connected
     * @param first multiplicity of the first class
     * @param second multiplicity of the second class
     */
    public UMLRelation(RelationType type, String name, UMLClass firstClass, UMLClass secondClass, RelationMultiplicity first, RelationMultiplicity second) {
        super(name);
        this.type = type;
        this.firstClass = firstClass;
        this.secondClass = secondClass;
        this.first = first;
        this.second = second;
        this.id = ID_GENERATOR.getAndIncrement();
    }

    public UMLClass getFirstClass() {
        return this.firstClass;
    }

    public UMLClass getSecondClass() {
        return this.secondClass;
    }

    public RelationType getType() {
        return this.type;
    }

    public String getId() {
        return String.valueOf(this.id);
    }

    public RelationMultiplicity getFirstMultiplicity() {
        return this.first;
    }

    public RelationMultiplicity getSecondMultiplicity() {
        return this.second;
    }

    /**
     * Function, which converts enum types for multiplicity to pretty format.
     * @param multiplicity type of multiplicity
     * @return multiplicity in pretty format
     */
    public String MultiplicityConverter(RelationMultiplicity multiplicity) {
        //  1     0..1         0..n          1..n         n
        // ONE, ZERO_TO_ONE, ZERO_TO_MANY, ONE_TO_MANY, MANY
        switch (multiplicity) {
            case ONE: return "1";
            case MANY: return "n";
            case ONE_TO_MANY: return "1..n";
            case ZERO_TO_ONE: return "0..1";
            default: return "0..n";
        }
    }
}
