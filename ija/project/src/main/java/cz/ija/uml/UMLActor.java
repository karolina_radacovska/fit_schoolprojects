package cz.ija.uml;

import java.util.ArrayList;
import java.util.List;

/**
 * Class creates actor
 */
public class UMLActor extends UMLClass {

    public List<UMLOperation> messages;

    public UMLActor(String name) {
        super(name);
        this.messages = new ArrayList<>();
    }

    public void addMessage(UMLOperation message) {
        if(!this.messages.contains(message)){
            this.messages.add(message);
        }
    }

    public void removeMessage(UMLOperation message){
        this.messages.remove(message);
    }

    public void printMessages(){
        System.out.println(this.messages);
    }
}
