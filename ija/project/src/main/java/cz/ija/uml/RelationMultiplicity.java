package cz.ija.uml;

/**
 * Enum for multiplicity of classes in relations.
 */
public enum RelationMultiplicity {
  //  0    1     0..1         0..n          1..n         n
  ONE, ZERO_TO_ONE, ZERO_TO_MANY, ONE_TO_MANY, MANY
}
