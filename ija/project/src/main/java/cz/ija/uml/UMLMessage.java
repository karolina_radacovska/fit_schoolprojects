package cz.ija.uml;

/**
 * Class creates a message
 */
public class UMLMessage {
    private final UMLOperation message;
    private final UMLOperation returnMessage;
    private final UMLActor from;
    private final UMLActor to;

    /**
     * Constructor creates a synchronous message
     * @param name        message text
     * @param returnValue return message text
     * @param from        actor from
     * @param to          actor to
     */
    public UMLMessage(UMLOperation name, UMLOperation returnValue, UMLActor from, UMLActor to){
        this.message = name;
        this.from = from;
        this.to = to;
        this.returnMessage = returnValue;
    }

    /**
     * Constructor creates an asynchronous message
     * @param name message text
     * @param from actor form
     * @param to   actor to
     */
    public UMLMessage(UMLOperation name, UMLActor from, UMLActor to, boolean isAsync) {
        if(isAsync) {
            this.message = name;
            this.from = from;
            this.to = to;
            this.returnMessage = null;
        }
        else{
            this.message = null;
            this.from = from;
            this.to = to;
            this.returnMessage = name;
        }

    }

    public String getMessageName(){
        if(message != null)
            return message.getName();
        return null;
    }

    public UMLOperation getMessage() {
        return message;
    }

    public UMLActor getFrom() {
        return from;
    }

    public UMLActor getTo(){
        return to;
    }

    public UMLOperation getReturnMessage(){
        return returnMessage;
    }

    public String getReturnMessageName(){
        if(returnMessage != null)
            return this.returnMessage.getName();
        return null;
    }

}