package cz.ija.uml;

/**
 * General class for any element of the application.
 */
public class Element {

    private final String name;

    public Element(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
