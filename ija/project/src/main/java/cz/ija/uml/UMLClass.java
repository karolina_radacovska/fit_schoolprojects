package cz.ija.uml;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Class of the uml class from class diagram.
 */
public class UMLClass extends Element {

    private String parent; // rodic tridy
    private String child; // potomek tridy
    private final List<UMLAttribute> attributes;
    private final List<UMLOperation> operations;

    /**
     * Constructor for umlClass.
     * @param name name
     */
    public UMLClass(String name) {
        super(name);
        this.parent = "";
        this.child = "";
        this.attributes = new ArrayList<>();
        this.operations = new ArrayList<>();
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public void setChild(String child) {
        this.child = child;
    }

    public String getParent() {
        return this.parent;
    }

    public String getChild() {
        return this.child;
    }

    public List<UMLAttribute> getAttributes() {
        return this.attributes;
    }

    public List<UMLOperation> getOperations() {
        return this.operations;
    }

    public UMLOperation getOperationByName(String name) {
        for (UMLOperation umlOperation : operations) {
            if (Objects.equals(umlOperation.getName(), name))
                return umlOperation;
        }
        return null;
    }

    public Boolean hasOperation(UMLOperation operation) {
        for (UMLOperation umlOperation : operations) {
            if (Objects.equals(umlOperation, operation))
                return true;
        }
        return false;
    }

    public UMLOperation getOperation(UMLOperation operation) {
        for (UMLOperation umlOperation : operations) {
            if (Objects.equals(umlOperation, operation))
                return umlOperation;
        }
        return null;
    }

    public void deleteAttribute(UMLAttribute attribute) {
        this.attributes.remove(attribute);
    }

    public void deleteOperation(UMLOperation operation) {
        this.operations.remove(operation);
    }

    public UMLAttribute createAttribute(String name, Access access, String dataType) {
        if (!attributes.stream().map(Element::getName).collect(Collectors.toList()).contains(name)) {
            UMLAttribute attribute = new UMLAttribute(name, access, dataType);
            attributes.add(attribute);
            return attribute;
        }
        return null;
    }

    public UMLOperation createOperation(String name, Access access, String dataType, List<String> parameters) {
        if (!operations.stream().map(Element::getName).collect(Collectors.toList()).contains(name)) {
            UMLOperation method = new UMLOperation(name, access, dataType, parameters);
            operations.add(method);
            return method;
        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UMLClass umlClass = (UMLClass) o;
        return attributes.equals(umlClass.attributes) && operations.equals(umlClass.operations) && child.equals(umlClass.child);
    }

    @Override
    public int hashCode() {
        return Objects.hash(attributes, operations);
    }
}
