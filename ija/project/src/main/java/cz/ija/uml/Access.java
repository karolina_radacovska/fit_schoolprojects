package cz.ija.uml;

/**
 * Enum for access modifiers.
 */
public enum Access {
    PUBLIC, PRIVATE, PROTECTED, PACKAGE
}
