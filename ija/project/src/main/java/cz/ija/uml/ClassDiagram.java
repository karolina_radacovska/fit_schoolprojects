package cz.ija.uml;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Class which contains all informations about class diagram.
 */
public class ClassDiagram extends Element {

    private final List<UMLClass> classList;
    private final List<UMLRelation> relations;

    /**
     * Constructor for classDiagram.
     * @param name name
     */
    public ClassDiagram(String name) {
        super(name);
        classList = new ArrayList<>();
        relations = new ArrayList<>();
    }

    /**
     * Function for creating class and storing to classList.
     * @param name name of the class
     * @return returns an instance of UMLClass
     */
    public UMLClass createClass(String name) {
        // Na kazdy list z classListu aplikuje funkci getName -> z techto jmen vytvori list -> pote zkontrolujeme, jestli ten list obsahuje jmeno
        if (!classList.stream().map(Element::getName).collect(Collectors.toList()).contains(name)) {
            UMLClass newClass = new UMLClass(name);
            classList.add(newClass);
            return newClass;
        }
        return null;
    }

    /**
     * Deletes umlClass from diagram.
     * @param umlClass class to be deleted
     */
    public void deleteClass(UMLClass umlClass) {
        this.classList.remove(umlClass);
    }

    /**
     * Get class by name.
     * @param name name of the searched class
     * @return found class
     */
    public UMLClass findClass(String name) {
        for (UMLClass uml : classList) {
            if (Objects.equals(uml.getName(), name))
                return uml;
        }
        return null;
    }

    /**
     * Get list of classes in diagram.
     * @return list of classes
     */
    public List<UMLClass> getClassList() {
        return Collections.unmodifiableList(classList);
    }

    /**
     * Creates relation between two classes.
     * @param type type of the relation
     * @param name name
     * @param first first class
     * @param second second class
     * @return relation
     */
    public UMLRelation createRelation(RelationType type, String name, UMLClass first, UMLClass second) {
        UMLRelation relation = new UMLRelation(type, name, first, second);
        relations.add(relation);
        return relation;
    }

    /**
     * * Creates relation between two classes with multiplicities.
     * @param type type of the relation
     * @param name name
     * @param firstClass first class
     * @param secondClass second class
     * @param first multiplicity by first class
     * @param second multiplicity by second class
     * @return relation
     */
    public UMLRelation createRelation(RelationType type, String name, UMLClass firstClass, UMLClass secondClass, RelationMultiplicity first, RelationMultiplicity second) {
        UMLRelation relation = new UMLRelation(type, name, firstClass, secondClass, first, second);
        relations.add(relation);
        return relation;
    }

    /**
     * Deletes all relation connected with given class and returns ids of the deleted relations.
     * @param classToDelete class to which is relation connected
     * @return list of relations ids
     */
    public List<String> getDeleteIDsRelations(String classToDelete) {
        List<String> ids = new ArrayList<>();
        for (UMLRelation relation : relations) {
            if (Objects.equals(relation.getFirstClass().getName(), classToDelete) || Objects.equals(relation.getSecondClass().getName(), classToDelete)) {
                ids.add(relation.getId());
            }
        }
        relations.removeIf(relation -> ids.contains(relation.getId()));
        return ids;
    }

    /**
     * Get list of relations.
     * @return list of uml relations
     */
    public List<UMLRelation> getRelations() {
        return this.relations;
    }
}
