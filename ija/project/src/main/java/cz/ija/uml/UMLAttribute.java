package cz.ija.uml;

import java.util.Objects;

/**
 * Class for attribute of classes.
 */
public class UMLAttribute extends Element {

    private final Access access;
    private final String dataType;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UMLAttribute that = (UMLAttribute) o;
        return Objects.equals(this.getName(), that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }

    /**
     * Constructor of the UMLAttribute.
     * @param name name
     * @param access access modifier
     * @param dataType data type of the attribute
     */
    public UMLAttribute(String name, Access access, String dataType) {
        super(name);
        this.access = access;
        this.dataType = dataType;
    }

    /**
     * Constructor of the UMLAttribute.
     * @param name name
     */
    public UMLAttribute(String name) {
        super(name);
        this.access = null;
        this.dataType = "";
    }

    public Access getAccess() {
        return this.access;
    }

    public String getDataType() {
        return this.dataType;
    }

    public String toString() {
        return getName();
    }
}
