package cz.ija.uml;

/**
 * Enum for types of relation.
 */
public enum RelationType {
    ASOCIATION, DIRECTED, AGGREGATION, COMPOSITION, GENERALIZATION
}
