package cz.ija.uml;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Class contains information about sequence diagram
 */
public class SequenceDiagram extends Element{

    private final List<UMLActor> actorList;
    private final List<UMLMessage> syncMessages;
    private final List<UMLMessage> asyncMessages;
    private final List<UMLMessage> returnMessages;

    /**
     * Constructor for sequence diagram
     * @param name name
     */
    public SequenceDiagram(String name) {
        super(name);
        this.actorList = new ArrayList<>();
        this.syncMessages = new ArrayList<>();
        this.asyncMessages = new ArrayList<>();
        this.returnMessages = new ArrayList<>();
    }

    /**
     * Creates synchronous message
     * @param operation message
     * @param returnMessage return message
     * @param from actor from
     * @param to actor to
     * @return returns sync message
     */
    public UMLMessage createSyncMessage(UMLOperation operation, UMLOperation returnMessage, UMLActor from, UMLActor to){
        UMLMessage newMessage = new UMLMessage(operation, returnMessage, from, to);
        syncMessages.add(newMessage);
        return newMessage;
    }

    public void deleteSyncMessage(UMLMessage message){
        this.syncMessages.remove(message);
    }

    /**
     * Creates asynchronous message
     * @param operation message
     * @param from actor from
     * @param to actor to
     * @return returns async message
     */
    public UMLMessage createAsyncMessage(UMLOperation operation, UMLActor from, UMLActor to){
        UMLMessage newMessage = new UMLMessage(operation, from, to, true);
        asyncMessages.add(newMessage);
        return newMessage;
    }

    public void deleteAsyncMessage(UMLMessage message){
        this.asyncMessages.remove(message);
    }
    /**
     * Creates return message
     * @param operation message
     * @param from actor from
     * @param to actor to
     * @return returns return message
     */
    public UMLMessage createReturnMessage(UMLOperation operation, UMLActor from, UMLActor to){
        UMLMessage newMessage = new UMLMessage(operation, from, to, false);
        returnMessages.add(newMessage);
        return newMessage;
    }

    public void deleteReturnMessage(UMLMessage message){
        this.returnMessages.remove(message);
    }

    public List<UMLActor> getActorList(){
        return actorList;
    }

    public List<UMLMessage> getSyncMssages() {
        return this.syncMessages;
    }

    public List<UMLMessage> getAsyncMssages() {
        return this.asyncMessages;
    }

    public List<UMLMessage> getReturnMessages() {
        return this.returnMessages;
    }

    /**
     * Method creates a new actor
     * @param name name
     * @return new actor
     */
    public UMLActor createActor(String name) {
        if (!actorList.stream().map(Element::getName).collect(Collectors.toList()).contains(name)) {
            UMLActor newActor = new UMLActor(name);
            actorList.add(newActor);
            return newActor;
        }
        return null;
    }

    /**
     * Method deletes an actor
     * @param name name
     */
    public void deleteActor(String name) {
        if (!actorList.stream().map(Element::getName).collect(Collectors.toList()).contains(name)) {
            actorList.removeIf(umlActor -> Objects.equals(umlActor.getName(), name));
        }
    }

    /**
     * Method finds an actor
     * @param name name
     * @return actor
     */
    public UMLActor findActor(String name) {
        for (UMLActor actor : actorList) {
            if (Objects.equals(actor.getName(), name))
                return actor;
        }
        return null;
    }

    public void addActor(UMLActor actor){
        actorList.add(actor);
    }

}