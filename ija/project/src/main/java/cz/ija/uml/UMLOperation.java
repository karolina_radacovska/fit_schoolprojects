package cz.ija.uml;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for the operations/methods of umlClass;
 */
public class UMLOperation extends UMLAttribute {

    private final List<String> parameters;

    /**
     * Constructor for the UMLOperation.
     * @param name name
     * @param access access modifier
     * @param dataType return type
     * @param parameters list of parameter's data types
     */
    public UMLOperation(String name, Access access, String dataType, List<String> parameters) {
        super(name, access, dataType);
        this.parameters = parameters;
    }

    /**
     * Constructor for the UMLOperation.
     * @param name name
     * @param parameters list of parameter's values
     */
    public UMLOperation(String name, List<String> parameters)  {
        super(name);
        this.parameters = parameters;
    }

    public List<String> getParameters() {
        return this.parameters;
    }
}
