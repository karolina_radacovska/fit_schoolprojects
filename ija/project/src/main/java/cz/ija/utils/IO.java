package cz.ija.utils;

import javafx.stage.FileChooser;

public class IO {

    /**
     * Creates jsonFileChooser to filter files only with json extension
     * @return FileChooser
     */
    public static FileChooser getJsonFileChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JSON files (*.json)",
                "*.json"));
        return fileChooser;
    }
}
