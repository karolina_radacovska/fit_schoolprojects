package cz.ija.utils;

import cz.ija.App;
import cz.ija.uml.*;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Class for dialog pop-ups.
 */
public class Dialogs {

    private static final List<String> dataTypes = new ArrayList<>();

    /**
     * Creates data types for uml attributes and methods.
     */
    public static void createDataTypes() {
        dataTypes.addAll(Arrays.asList("String", "Double", "Boolean", "Int", "Char", "Void", "List<Double>", "List<Boolean>", "List<String>", "List<Int>", "List<Char>"));
    }

    /**
     * Function for getting available dataTypes.
     * @return list of dataTypes as strings
     */
    public static List<String> getDataTypes() {
        return dataTypes;
    }

    /**
     * Adds datatype to list od data types.
     */
    public static void addDataType(String name) {
        dataTypes.add(name);
        dataTypes.add("List<"+name+">");
    }

    /**
     * Removes datatype from list od data types.
     */
    public static void removeDataType(String name) {
        dataTypes.remove(name);
        dataTypes.remove("List<"+name+">");
    }

    /**
     * Function, which sets minimum size of the dialogs.
     * On manjaro javaFx has a bug, and sometimes it shows dialog so small, that it looks like a random line.
     * @param dialog any dialog
     */
    public static void fixDialog(Dialog<?> dialog) {
        dialog.getDialogPane().setMinHeight(100);
        dialog.getDialogPane().setMinWidth(100);
        dialog.setResizable(true);
        dialog.onShownProperty().addListener(e -> {
            Platform.runLater(() -> dialog.setResizable(false));
        });
    }

    /**
     * Creates simple text dialog.
     * @param title title of the dialog
     * @param header header of the dialog
     * @return Dialog
     */
    public static TextInputDialog createSimpleTextDialog(String title, String header) {
        TextInputDialog popUp = new TextInputDialog();
        popUp.setTitle(title);
        popUp.setHeaderText(header);
        fixDialog(popUp);
        return popUp;
    }

    /**
     * Generates dialog for button Add attribute and with selected information from user creates UMLAttribute.
     * @param umlClass attributes parent class
     * @return UMLAttribute
     */
    public static UMLAttribute attributeDialog(UMLClass umlClass) {
        Dialog<UMLAttribute> dialog = new Dialog<>();
        dialog.setTitle("Attribute dialog");
        dialog.setHeaderText("Please specify…");
        DialogPane dialogPane = dialog.getDialogPane();
        dialogPane.getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        // Name
        Label selectName = new Label("Attribute name: ");
        TextField name = new TextField("Name");

        // Access modifier
        ObservableList<Access> accessObservableList = FXCollections.observableArrayList(Access.values());
        Label access = new Label("Access modifier: ");
        ComboBox<Access> comboBox1 = new ComboBox<>(accessObservableList);
        comboBox1.getSelectionModel().selectFirst();

        // Type
        ObservableList<String> typesObservableList = FXCollections.observableArrayList(getDataTypes());
        Label types = new Label("Data type: ");
        ComboBox<String> comboBox2 = new ComboBox<>(typesObservableList);
        comboBox2.getSelectionModel().selectFirst();

        dialogPane.setContent(new VBox(8.0, selectName, name, access, comboBox1, types, comboBox2));

        Platform.runLater(name::requestFocus);
        Platform.runLater((comboBox1)::requestFocus);
        Platform.runLater((comboBox2)::requestFocus);

        dialog.setResultConverter((ButtonType button) -> {
            if (button == ButtonType.OK) {
                Access access1 = comboBox1.getValue();
                String dataType = comboBox2.getValue();
                return umlClass.createAttribute(name.getText(), access1, dataType);
            }
            return null;
        });
        fixDialog(dialog);

        Optional<UMLAttribute> optionalResult = dialog.showAndWait();
        return optionalResult.orElse(null);
    }

    /**
     * Generates dialog for different relations and creates UMLRelation.
     * @param type relation type
     * @param cardinalities if relation should contain cardinalities
     * @return created UMLRelation
     */
    public static UMLRelation relationDialog(RelationType type, Boolean cardinalities) {

        Dialog<UMLRelation> dialog = new Dialog<>();
        dialog.setTitle("Relation dialog");
        dialog.setHeaderText("Please specify…");
        DialogPane dialogPane = dialog.getDialogPane();
        dialogPane.getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        // Name
        Label selectName = new Label("Name relation: ");
        TextField name = new TextField("");

        List<String> classList = App.diagram.getClassList().stream().map(Element::getName).collect(Collectors.toList());
        ObservableList<String> classObservableList = FXCollections.observableArrayList(classList);

        // First Class
        Label first = new Label("Relation from class: ");
        ComboBox<String> comboBox1 = new ComboBox<>(classObservableList);
        comboBox1.getSelectionModel().selectFirst();

        // Second Class
        Label second = new Label("Relation to class: ");
        ComboBox<String> comboBox2 = new ComboBox<>(classObservableList);
        comboBox2.getSelectionModel().selectFirst();

        ComboBox<RelationMultiplicity> comboBox3 = null;
        ComboBox<RelationMultiplicity> comboBox4 = null;
        if (cardinalities) {
            ObservableList<RelationMultiplicity> options = FXCollections.observableArrayList(RelationMultiplicity.values());
            Label firstM = new Label("Multiplicity to first class: ");
            comboBox3 = new ComboBox<>(options);
            Label secondM = new Label("Multiplicity to second class: ");
            comboBox4 = new ComboBox<>(options);
            comboBox3.getSelectionModel().selectFirst();
            comboBox4.getSelectionModel().selectFirst();

            dialogPane.setContent(new VBox(8.0, selectName, name, first, comboBox1, second, comboBox2, firstM, comboBox3, secondM, comboBox4));
            Platform.runLater((comboBox3)::requestFocus);
            Platform.runLater((comboBox4)::requestFocus);
        }
        else {
            dialogPane.setContent(new VBox(8.0, selectName, name, first, comboBox1, second, comboBox2));
        }
        Platform.runLater(name::requestFocus);
        Platform.runLater((comboBox1)::requestFocus);
        Platform.runLater((comboBox2)::requestFocus);

        ComboBox<RelationMultiplicity> finalComboBox = comboBox3;
        ComboBox<RelationMultiplicity> finalComboBox1 = comboBox4;
        dialog.setResultConverter((ButtonType button) -> {
            if (button == ButtonType.OK) {
                UMLClass firstClass = App.diagram.findClass(comboBox1.getValue());
                UMLClass secondClass = App.diagram.findClass(comboBox2.getValue());

                if (cardinalities) {
                    RelationMultiplicity firstOne = finalComboBox.getValue();
                    RelationMultiplicity secondOne = finalComboBox1.getValue();
                    return App.diagram.createRelation(type, name.getText(), firstClass, secondClass, firstOne, secondOne);
                }
                return App.diagram.createRelation(type, name.getText(), firstClass, secondClass);
            }
            return null;
        });
        fixDialog(dialog);

        Optional<UMLRelation> optionalResult = dialog.showAndWait();
        return optionalResult.orElse(null);
    }

    /**
     * Generates dialog for button create method and creates UMLOperation.
     * @param umlClass operation's parent class
     * @return created UMLOperation
     */
    public static UMLOperation operationDialog(UMLClass umlClass) {
        Dialog<UMLOperation> dialog = new Dialog<>();
        dialog.setTitle("Attribute dialog");
        dialog.setHeaderText("Please specify…");
        DialogPane dialogPane = dialog.getDialogPane();
        dialogPane.getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        // Name
        Label selectName = new Label("Attribute name: ");
        TextField name = new TextField("Name");

        // Access modifier
        ObservableList<Access> accessObservableList = FXCollections.observableArrayList(Access.values());
        Label access = new Label("Access modifier: ");
        ComboBox<Access> comboBox1 = new ComboBox<>(accessObservableList);
        comboBox1.getSelectionModel().selectFirst();

        // Type
        ObservableList<String> typesObservableList = FXCollections.observableArrayList(getDataTypes());
        Label types = new Label("Data type: ");
        ComboBox<String> comboBox2 = new ComboBox<>(typesObservableList);
        comboBox2.getSelectionModel().selectFirst();

        // Params
        Label parameters = new Label("Parameters: (WHEN CLICKED -> SELECTED) ");
        ListView<String> listView = new ListView<String>();
        ObservableList<String> list = FXCollections.observableArrayList(getDataTypes());
        listView.setItems(list);
        listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        List<String> selectedParameters = new ArrayList<>();
        listView.setOnMouseClicked(new EventHandler<Event>() {
            @Override
            public void handle(Event event) {
                ObservableList<String> selectedItems =  listView.getSelectionModel().getSelectedItems();
                for(String s : selectedItems){
                    selectedParameters.add(s);
                }
            }
        });

        dialogPane.setContent(new VBox(8.0, selectName, name, access, comboBox1, types, comboBox2, parameters, listView));
        Platform.runLater(name::requestFocus);
        Platform.runLater(comboBox1::requestFocus);
        Platform.runLater(comboBox2::requestFocus);
        Platform.runLater(listView::requestFocus);

        dialog.setResultConverter((ButtonType button) -> {
            if (button == ButtonType.OK) {
                return umlClass.createOperation(name.getText(), comboBox1.getValue(), comboBox2.getValue(), selectedParameters);
            }
            return null;
        });
        Dialogs.fixDialog(dialog);

        Optional<UMLOperation> optionalResult = dialog.showAndWait();
        return optionalResult.orElse(null);
    }

    public enum MessageEnum {
        ACTOR_DOESNT_MATCH_CLASS, MESSAGE_ISNT_IN_CLASS
    }

    private static String messageConverter(MessageEnum message) {
        switch (message) {
            case ACTOR_DOESNT_MATCH_CLASS:
                return "The actor doesn't match any class from class diagram. Do you really want to add him to sequence diagram?";
            case MESSAGE_ISNT_IN_CLASS:
                return "The message doesn't match any method of the class from class diagram. Do you really want to add the message to sequence diagram?";
            default:
                return "";
        }
    }

    public static boolean yesOrNo(MessageEnum message) {
        Dialog<Boolean> dialog = new Dialog<>();
        dialog.setTitle("Inconsistency");
        dialog.setHeaderText(messageConverter(message));
        DialogPane dialogPane = dialog.getDialogPane();
        dialogPane.getButtonTypes().addAll(ButtonType.YES, ButtonType.NO);
        dialog.setResultConverter((ButtonType button) -> button == ButtonType.YES);
        Dialogs.fixDialog(dialog);
        Optional<Boolean> result = dialog.showAndWait();
        return result.orElse(false);
    }
}
