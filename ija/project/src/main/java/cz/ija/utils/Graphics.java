package cz.ija.utils;

import javafx.scene.Node;

import static cz.ija.utils.Dimensions.X;

public class Graphics {

    /**
     * Calculates the position of point on relation line.
     * @param n multiplication
     * @param m division
     * @param node1 first class
     * @param node2 second class
     * @param dimension dimension (X / Y)
     * @return position of point on relation line
     */
    public static double getDistanceFraction(int n, int m, Node node1, Node node2, Dimensions dimension ) {
        double position1 =  dimension == X ? node1.getLayoutX() : node1.getLayoutY();
        double position2 =  dimension == X ? node2.getLayoutX() : node2.getLayoutY();
        return position1 + (position1<position2 ? 1 : -1)  * n * (Math.abs(position1-position2)/m);
    }
}
