package cz.ija.utils;

import cz.ija.drawable.classDiagram.ClassModel;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import static cz.ija.utils.Dimensions.X;
import static cz.ija.utils.Dimensions.Y;
import static cz.ija.utils.Graphics.getDistanceFraction;

/**
 * Class which contains functions for displaying class diagram components.
 */
public class Drawings {

    /**
     * Displays relation line with label (label can be empty string).
     * @param firstClass first class
     * @param secondClass second class
     * @param relationName name of the relation
     * @param allTogether pane which connects all the components of relation
     */
    public static void drawLine(ClassModel firstClass, ClassModel secondClass, String relationName, Pane allTogether) {

        Line line = new Line(firstClass.getLayoutX(), firstClass.getLayoutY(), secondClass.getLayoutX(), secondClass.getLayoutY());
        Label name = new Label(relationName);
        name.setLayoutX(getDistanceFraction(1, 2, firstClass, secondClass, X));
        name.setLayoutY(getDistanceFraction(1, 2, firstClass, secondClass, Y));

        firstClass.addEventHandler(MouseEvent.MOUSE_DRAGGED, event -> {
            line.setStartX(firstClass.getLayoutX());
            line.setStartY(firstClass.getLayoutY());

            name.setLayoutX(getDistanceFraction(1, 2, firstClass, secondClass, X));
            name.setLayoutY(getDistanceFraction(1, 2, firstClass, secondClass, Y));
        });

        secondClass.addEventHandler(MouseEvent.MOUSE_DRAGGED, event -> {
            line.setEndX(secondClass.getLayoutX());
            line.setEndY(secondClass.getLayoutY());

            name.setLayoutX(getDistanceFraction(1, 2, firstClass, secondClass, X));
            name.setLayoutY(getDistanceFraction(1, 2, firstClass, secondClass, Y));
        });
        allTogether.getChildren().addAll(name, line);
    }

    /**
     * Gets degrees for rotation of the relation arrow or rhombus.
     * @param s second class
     * @param f first class
     * @param arrow bool if it's arrow (4 sides) or rhombus (2 sides)
     * @return number of degrees
     */
    private static double getRotation(ClassModel s, ClassModel f, Boolean arrow) {
        if(f.getLayoutX() > s.getLayoutX()) {
            if (f.getLayoutY() > s.getLayoutY()) {
                if (arrow)
                    return 135;
                return -45;
            }
            else
                return 45;
        } else {
            if (f.getLayoutY() > s.getLayoutY())
                return -135;
            else {
                if (arrow)
                    return -45;
                return 135;
            }
        }
    }

    /**
     * Sets position and rotation of the polygon.
     *
     * @param polygon     polygon
     * @param firstClass  first class
     * @param secondClass second class
     * @param arrow       if it's arrow (4 sides) or rhombus (2 sides)
     */
    private static void polygonTransformation(Polygon polygon, ClassModel firstClass, ClassModel secondClass, Boolean arrow) {
        polygon.setLayoutX(getDistanceFraction(1, 60, secondClass, firstClass, X) - 10);
        polygon.setLayoutY(getDistanceFraction(1, 60, secondClass,firstClass, Y) - 20);
        polygon.setRotate(getRotation(firstClass, secondClass, arrow));
    }

    /**
     * Displays relation shapes to the end of line.
     *
     * @param fill        if polygon should have color inside or not
     * @param firstClass  first class
     * @param secondClass second class
     * @param polygon     shape
     * @param arrow       if it's arrow (4 sides) or rhombus (2 sides)
     * @param allTogether pane which connects all the components of relation
     */
    public static void drawPolygon(Boolean fill, ClassModel firstClass, ClassModel secondClass, Polygon polygon, Boolean arrow, Pane allTogether) {

        if (fill)
            polygon.setFill(Color.BLACK);
        else
            polygon.setFill(Color.WHITE);
        polygon.setStroke(Color.BLACK);

        polygonTransformation(polygon, firstClass, secondClass, arrow);
        firstClass.addEventHandler(MouseEvent.MOUSE_DRAGGED, event -> {
            polygonTransformation(polygon, firstClass, secondClass, arrow);
        });
        secondClass.addEventHandler(MouseEvent.MOUSE_DRAGGED, event -> {
            polygonTransformation(polygon, firstClass, secondClass, arrow);
        });
        allTogether.getChildren().add(polygon);
    }

    private static void multiplicityPosition(Label label, ClassModel firstClass, ClassModel secondClass) {
        label.setLayoutX(getDistanceFraction(1, 10, firstClass, secondClass, X));
        label.setLayoutY(getDistanceFraction(1, 10, firstClass, secondClass, Y));
    }

    /**
     * Displays multiplicities of classes.
     * @param first label name of the first class multiplicity
     * @param second label name of the second class multiplicity
     * @param firstClass first class
     * @param secondClass second class
     * @param allTogether pane which connects all the components of relation
     */
    public static void drawMultiplicity(String first, String second, ClassModel firstClass, ClassModel secondClass, Pane allTogether) {
        Label toFirst = new Label(first);
        Label toSecond = new Label(second);

        multiplicityPosition(toFirst, firstClass, secondClass);
        multiplicityPosition(toSecond, secondClass, firstClass);

        firstClass.addEventHandler(MouseEvent.MOUSE_DRAGGED, event -> {
            multiplicityPosition(toFirst, firstClass, secondClass);
            multiplicityPosition(toSecond, secondClass, firstClass);
        });

        secondClass.addEventHandler(MouseEvent.MOUSE_DRAGGED, event -> {
            multiplicityPosition(toFirst, firstClass, secondClass);
            multiplicityPosition(toSecond, secondClass, firstClass);
        });
        allTogether.getChildren().addAll(toFirst, toSecond);
    }
}
