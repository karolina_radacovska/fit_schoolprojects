package cz.ija.utils;

/**
 * Enum for recognition of X and Y coordinates.
 */
public enum Dimensions
{
    X,Y
}
