package cz.ija.drawable.classDiagram;

import cz.ija.uml.Access;
import cz.ija.uml.UMLAttribute;
import javafx.scene.control.Label;

/**
 * Class for attribute model. Defines how will be attribute displayed.
 */
public class AttributeModel extends Label {
    private final UMLAttribute umlAttribute;

    /**
     * Converts elements of Access enum to string format for display.
     * @param access access modifier
     * @return string representation
     */
    private static String AccessConverter(Access access) {
        switch (access) {
            case PUBLIC: return "+";
            case PRIVATE: return "-";
            case PROTECTED: return "#";
            default: return "~";
        }
    }

    /**
     * Constructor which defines, how will be the attribute displayed.
     * @param attribute uml attribute
     */
    public AttributeModel (UMLAttribute attribute) {
        super((AccessConverter(attribute.getAccess()) + " " + attribute.getDataType() + " " + attribute.getName()));
        this.umlAttribute = attribute;
    }

    public UMLAttribute getUmlAttribute() {
        return umlAttribute;
    }
}
