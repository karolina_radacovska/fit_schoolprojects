package cz.ija.drawable.sequenceDiagram;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for vertically spaces between messages
 */
public class IteratorModel {
    public double iteratorBetweenMessages;
    public int msgCount = 0;
    public List<Object> messages;

    /**
     * Constructor initializes class attributes
     */
    public IteratorModel(){
        this.iteratorBetweenMessages = 0; // zaciatok
        this.messages = new ArrayList<>();
    }

    /**
     * Function counts spaces between messages
     * @return length for message from the top of timeline
     */
    public double getSpace(){
        this.msgCount++;
        this.iteratorBetweenMessages = msgCount*50;
        return this.iteratorBetweenMessages;
    }
}
