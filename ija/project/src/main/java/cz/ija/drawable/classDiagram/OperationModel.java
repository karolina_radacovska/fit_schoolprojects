package cz.ija.drawable.classDiagram;

import cz.ija.uml.Access;
import cz.ija.uml.UMLOperation;
import javafx.scene.control.Label;
import java.util.List;

/**
 * Class for class model. Defines how will be class displayed.
 */
public class OperationModel extends Label {
    private final UMLOperation umlOperation;

    private static String AccessConverter(Access access) {
        switch (access) {
            case PUBLIC: return "+";
            case PRIVATE: return "-";
            case PROTECTED: return "#";
            default: return "~";
        }
    }

    public UMLOperation getUmlOperation() {
        return this.umlOperation;
    }

    /**
     * Creates string of parameters in pretty format.
     * @param parameters list of parameters
     * @return string
     */
    private static String stringParameters(List<String> parameters) {
        StringBuilder result = new StringBuilder();
        for (String parameter: parameters) {
            result.append(", ").append(parameter);
        }
        result.replace(0, 2, "(");
        result.append(")");
        System.out.println(parameters);
        System.out.println(result.toString());
        return result.toString();
    }

    /**
     * Constructor which defines, how will be the class displayed.
     * @param operation uml operation
     */
    public OperationModel (UMLOperation operation) {
        super((AccessConverter(operation.getAccess()) + " " + operation.getDataType() + " " + operation.getName() + stringParameters(operation.getParameters())));
        this.umlOperation = operation;
    }
}
