package cz.ija.drawable.sequenceDiagram;

import cz.ija.uml.*;
import javafx.scene.control.TitledPane;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class for actor model. Describes how the actor will be displayed.
 */
public class ActorModel extends TitledPane {

    private final UMLActor umlActor;

    private final List<Object> allMessages = new ArrayList<>();

    /**
     * Constructor defines how the actor will be displayed, sets the values to the class attributes.
     * @param umlActor actor
     */
    public ActorModel(UMLActor umlActor){
        super(umlActor.getName(), null);
        this.setId(umlActor.getName() + "_actorModel_");
        this.umlActor = umlActor;
    }

    public String getName(){
        return this.umlActor.getName();
    }

    /**
     * Spaces between messages when drawing from a file
     * @param object message
     */
    public void addAll(Object object){
        this.allMessages.add(object);
    }

    public UMLActor getUmlActor(){
        return this.umlActor;
    }
}
