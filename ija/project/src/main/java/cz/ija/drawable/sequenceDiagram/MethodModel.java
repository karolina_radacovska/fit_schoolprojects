package cz.ija.drawable.sequenceDiagram;

import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import cz.ija.uml.UMLMessage;


/**
 * Class for displaying message as a pane
 */
public class MethodModel extends Pane {
    private UMLMessage umlMessage;
    private String message;


    /**
     * Constructor creates a message
     * @param arrow arrow of message direction
     * @param message message text
     */
    public MethodModel(Pane arrow, Label message, UMLMessage umlMessage){
        super(arrow, message);
        this.setId(message.getText() + "_message_");
        this.message = message.getText();
        this.umlMessage = umlMessage;
    }

    public String getMessage(){
        return this.message;
    }

    public UMLMessage getUmlMessage() {
        return this.umlMessage;
    }
}
