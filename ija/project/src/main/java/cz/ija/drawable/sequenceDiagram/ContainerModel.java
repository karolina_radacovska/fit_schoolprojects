package cz.ija.drawable.sequenceDiagram;

import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Line;

/**
 * Class for displaying actor and timeline as a pane
 */
public class ContainerModel extends Pane {
    private final ActorModel actor;

    /**
     * Constructor creates a new pane model
     * @param actor actor
     * @param timeLine actor's timeline
     */
    public ContainerModel(ActorModel actor, TimeLineModel timeLine){
        super(actor, timeLine);
        this.actor = actor;
    }

    public String getActorName(){
        return actor.getName();

    }
}
