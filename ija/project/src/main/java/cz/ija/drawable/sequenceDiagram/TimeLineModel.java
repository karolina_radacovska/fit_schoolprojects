package cz.ija.drawable.sequenceDiagram;

import javafx.scene.Node;
import javafx.scene.shape.Line;

/**
 * Class for timeline of an actor
 */

public class TimeLineModel extends Line{

    /**
     * Constructor creates timeline
     * @param startX start point on x-axis
     * @param startY start point on y-axis
     * @param EndX end point on x-axis
     * @param EndY end point on y-axis
     */
    public TimeLineModel(double startX, double startY, double EndX, double EndY){
        super(startX+15, startY+26, EndX+15, EndY+280);
        getStrokeDashArray().addAll(10d, 5d); // stackoverflow hint
    }
}
