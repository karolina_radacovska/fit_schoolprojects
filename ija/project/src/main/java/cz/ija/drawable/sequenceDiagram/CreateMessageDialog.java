package cz.ija.drawable.sequenceDiagram;

import cz.ija.App;
import cz.ija.uml.*;
import cz.ija.utils.Dialogs;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Class creates a message dialog for pop-ups
 */
public class CreateMessageDialog extends Dialog {

    public CreateMessageDialog() {
    }

    /**
     * Creates a dialog for different types od messages.
     * Synchronous messages has label, parameters, return message and actors
     * Asynchronous message has label, parameters and actors
     * Return message has label and actors
     * @param title type of message
     * @param isDirect direct message
     * @param isReturn return message
     * @return returns message
     */
    public UMLMessage messageDialog(String title, Boolean isDirect, Boolean isReturn) {

        Dialog<UMLMessage> dialog = new Dialog<>();
        dialog.setTitle(title + " Message Dialog");
        DialogPane dialogPane = dialog.getDialogPane();
        dialogPane.getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        // Name of message
        Label messageLabel = new Label("Message: ");
        TextField message = new TextField("");

        // parameters -> stringy
        Label parametersLabel = new Label("Parameters: (separate by comma w/out spaces)");
        TextField parameters = new TextField("");

        // return value -> string
        Label returnLabel = new Label("Return value: ");
        TextField returnValue = new TextField("");

        // First Actor
        List<String> actorList = App.sequenceDiagram.getActorList().stream().map(Element::getName).collect(Collectors.toList());
        ObservableList<String> actorObservableList = FXCollections.observableArrayList(actorList);

        Label firstActorLabel = new Label("Message from actor: ");
        ComboBox<String> comboBoxFrom = new ComboBox<>(actorObservableList);
        comboBoxFrom.getSelectionModel().selectFirst();

        // Second Actor
        Label secondActorLabel = new Label("Message to actor: ");
        ComboBox<String> comboBoxTo = new ComboBox<>(actorObservableList);
        comboBoxTo.getSelectionModel().selectFirst();

        // synchronous message properties
        if (isDirect && isReturn) {
            dialogPane.setContent(new VBox(8.0,
                    messageLabel, message,
                    parametersLabel, parameters,
                    returnLabel, returnValue,
                    firstActorLabel, comboBoxFrom,
                    secondActorLabel, comboBoxTo)
            );
            // asynchronous message properties
        } else if(isDirect && !isReturn){
            dialogPane.setContent(new VBox(8.0,
                    messageLabel, message,
                    parametersLabel, parameters,
                    firstActorLabel, comboBoxFrom,
                    secondActorLabel, comboBoxTo)
            );
            // return message properties
        }else{
            dialogPane.setContent(new VBox(8.0,
                    returnLabel, returnValue,
                    firstActorLabel, comboBoxFrom,
                    secondActorLabel, comboBoxTo)
            );
        }

        dialog.setResultConverter((ButtonType button) -> {
            if (button == ButtonType.OK) {
                UMLActor firstActor = App.sequenceDiagram.findActor(comboBoxFrom.getValue());
                UMLActor secondActor = App.sequenceDiagram.findActor(comboBoxTo.getValue());

                String p = parameters.getText();
                List<String> myParameters = new ArrayList<String>(Arrays.asList(p.split(",")));
                UMLOperation operation = new UMLOperation(message.getText(), myParameters);
                UMLOperation retOperation = new UMLOperation(returnValue.getText(), null);
                if (isDirect && isReturn) {
                    if(App.diagram != null) {
                        UMLClass umlClass = App.diagram.findClass(secondActor.getName());
                        if (umlClass != null) {
                            UMLOperation classOperation = umlClass.getOperationByName(operation.getName());
                            if (classOperation == null) {
                                if (!Dialogs.yesOrNo(Dialogs.MessageEnum.MESSAGE_ISNT_IN_CLASS)) {
                                    return null;
                                }
                            }
                        }
                    }
                    return App.sequenceDiagram.createSyncMessage(operation, retOperation, firstActor, secondActor);
                } else if(isDirect && !isReturn){
                    return App.sequenceDiagram.createAsyncMessage(operation, firstActor, secondActor);
                }else{
                    return App.sequenceDiagram.createReturnMessage(retOperation, firstActor, secondActor);
                }
            }
            return null;
        });

        Optional<UMLMessage> optionalResult = dialog.showAndWait();
        return optionalResult.orElse(null);
    }
}