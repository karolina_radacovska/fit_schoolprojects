package cz.ija.drawable.classDiagram;

import cz.ija.App;
import cz.ija.uml.UMLClass;
import javafx.geometry.Orientation;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.TilePane;
import java.util.Objects;

/**
 * Class for class model. Defines how will be class displayed.
 */
public class ClassModel extends TitledPane{
    private final UMLClass umlClass;

    private int relationNumber;

    /**
     * Constructor which defines, how will be the class displayed.
     * @param umlClass uml class
     */
    public ClassModel(UMLClass umlClass) {
        super(umlClass.getName(), new TilePane());
        ((TilePane)this.getContent()).setOrientation(Orientation.VERTICAL);
        this.umlClass = umlClass;
        App.diagram.createClass(umlClass.getName());
        this.relationNumber = 0;
    }

    public void increaseRelationNumber() {
        this.relationNumber++;
    }

    public void decreaseRelationNumber() {
        this.relationNumber--;
    }

    public int getRelationNumber() {
        return this.relationNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClassModel that = (ClassModel) o;
        return  Objects.equals(umlClass, that.umlClass) && Objects.equals(getText(), that.getText());
    }

    @Override
    public int hashCode() {
        return Objects.hash(umlClass);
    }

    public UMLClass getUmlClass() {
        return this.umlClass;
    }

}
