package cz.ija;

import com.google.gson.Gson;
import cz.ija.drawable.classDiagram.AttributeModel;
import cz.ija.drawable.classDiagram.ClassModel;
import cz.ija.drawable.classDiagram.OperationModel;
import cz.ija.drawable.sequenceDiagram.*;
import cz.ija.uml.*;
import cz.ija.utils.*;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.scene.shape.*;
import java.io.*;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class PrimaryController {

    // ================== GENERAL ==================
    private Object selection = null;
    private Object selectionChild = null;
    private static boolean ignoreNextClick = false;
    private final Gson gson = new Gson();
    private final FileChooser fileChooser = IO.getJsonFileChooser();

    /**
     * Separates messages vertically in sequence diagram.
     */
    private final IteratorModel iteratorModel = new IteratorModel();

    /**
     * Gets any model from scene by id.
     *
     * @param name name of the id
     * @return returns node
     */
    private Node getById(String name) {
        return App.scene.lookup("#" + name);
    }

    private void hideButtons() {
        App.scene.lookup("#btnDelete").setVisible(false);
        App.scene.lookup("#btnAddAttribute").setVisible(false);
        App.scene.lookup("#btnAddMethod").setVisible(false);
    }

    /**
     * Handles selection on click.
     *
     * @param e event
     */
    @FXML
    private void select(Event e) {
        select(e.getSource());
    }

    /**
     * Sets selection by clicked model.
     *
     * @param clicked clicked model
     */
    private void select(Object clicked) {

        if(ignoreNextClick){
            ignoreNextClick =  false;
            return;
        } else if (clicked instanceof MethodModel) {
            selection = clicked;
            ignoreNextClick = true;
            App.scene.lookup("#btnDelete").setVisible(true);
        }

        if (clicked instanceof TitledPane) {
            selection = clicked;
            selectionChild = clicked;
            if (selection instanceof ClassModel) {
                App.scene.lookup("#btnAddAttribute").setVisible(true);
                App.scene.lookup("#btnAddMethod").setVisible(true);
                App.scene.lookup("#btnDelete").setVisible(true);
            }
            if(selection instanceof ActorModel){
                App.scene.lookup("#btnDelete").setVisible(true);
            }
        } else {
            hideButtons();
            selection = null;
        }
    }

    void hideAndShow() {
            getById("btnAddClass").setVisible(false);
            getById("asociation").setVisible(false);
            getById("directed").setVisible(false);
            getById("aggregation").setVisible(false);
            getById("composition").setVisible(false);
            getById("generalization").setVisible(false);
            getById("btnCheckInconsistency").setVisible(false);
            getById("btnAddActor").setVisible(false);
            getById("btnAddSyncMessage").setVisible(false);
            getById("btnAddAsyncMessage").setVisible(false);
            getById("btnAddReturnMessage").setVisible(false);
            getById("btnCreateDiagram").setVisible(true);
            getById("btnCreateSeqDiagram").setVisible(true);
            App.scene.lookup("#btnAddAttribute").setVisible(false);
            App.scene.lookup("#btnAddMethod").setVisible(false);
//            App.scene.lookup("#btnDelete").setVisible(false);
    }

    @FXML
    private void closeDiagram() {
//        List<String> allButtons = Arrays.asList("btnAddClass", "asociation", "directed", "aggregation", "composition", "generalization",
//                "btnDelete", "btnAddAttribute", "btnAddMethod", "btnAddActor", "btnAddSyncMessage", "btnAddAsyncMessage", "btnAddReturnMessage",
//                "btnCheckInconsistency", "btnCreateDiagram", "btnCreateSeqDiagram", "separator");
        hideAndShow();

        App.diagram = null;
        App.sequenceDiagram = null;
        iteratorModel.msgCount = 0;

        List<Node> children = ((AnchorPane) App.scene.lookup("#mainArea")).getChildren();
//        List<Node> toRemove = new ArrayList<>();
//        for (Node child : children) {
//            if (!allButtons.contains(child.getId())) {
//                toRemove.add(child);
//            }
//            System.out.println(child.getId());
//        }
//        try {
//            ((AnchorPane) App.scene.lookup("#mainArea")).getChildren().removeAll(toRemove);
//        } catch (Exception e) {}
        children.clear();
    }

    @FXML
    private void help() {
    }

    @FXML
    private void saveDiagram() throws IOException {
        if (App.diagram != null) {
            fileChooser.setInitialFileName(App.diagram.getName() + ".json");
            File saveLocation = fileChooser.showSaveDialog(App.scene.getWindow());
            try (Writer writer = new FileWriter(saveLocation)) {
                gson.toJson(App.diagram, writer);
            }
        } else {
            Alert popUp = new Alert(Alert.AlertType.ERROR);
            popUp.setHeaderText("No diagram open");
            popUp.show();
        }
    }

    @FXML
    private void saveSequenceDiagram() {
        if (App.sequenceDiagram != null) {
            fileChooser.setInitialFileName(App.sequenceDiagram.getName() + ".json");
            File saveLocation = fileChooser.showSaveDialog(App.scene.getWindow());
            try (Writer writer = new FileWriter(saveLocation)) {
                gson.toJson(App.sequenceDiagram, writer);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            Alert popUp = new Alert(Alert.AlertType.ERROR);
            popUp.setHeaderText("No Sequence Diagram open!");
            popUp.show();
        }
    }

    @FXML
    private void loadDiagram() throws IOException {
        fileChooser.setInitialFileName("");

        fileChooser.setTitle("Open json file for Class Diagram");
        String loadLocationClass = fileChooser.showOpenDialog(App.scene.getWindow()).getPath();
        String jsonClass = Files.readString(Paths.get(loadLocationClass));
        App.diagram = gson.fromJson(jsonClass, ClassDiagram.class);
        Dialogs.createDataTypes();

        fileChooser.setTitle("Open json file for Sequence Diagram");
        String loadLocationSequence = fileChooser.showOpenDialog(App.scene.getWindow()).getPath();
        String jsonSequence = Files.readString(Paths.get(loadLocationSequence));
        App.sequenceDiagram = gson.fromJson(jsonSequence, SequenceDiagram.class);

        loadButtons();
        drawClassDiagram();
        drawSequenceDiagram();
        checkInconsistency();
    }

    /**
     * Checks inconsistency of diagrams and sets color to different components.
     */
    @FXML
    private void checkInconsistency() {
        if (App.diagram != null && App.sequenceDiagram != null) {
            List<String> actorsNames = App.sequenceDiagram.getActorList().stream().map(Element::getName).collect(Collectors.toList());
            List<String> classesNames = App.diagram.getClassList().stream().map(Element::getName).collect(Collectors.toList());

            for (String actorName : actorsNames) {
                if (!classesNames.contains(actorName)) {
                    ActorModel actorModel = (ActorModel) getById(actorName + "_actorModel_");
                    actorModel.setTextFill(Color.RED);
                } else {
                    ActorModel actorModel = (ActorModel) getById(actorName + "_actorModel_");
                    actorModel.setTextFill(Color.BLACK);
                }
            }
            for (String className : classesNames) {
                if (!actorsNames.contains(className)) {
                    ClassModel classModel = (ClassModel) getById(className);
                    classModel.setTextFill(Color.RED);
                } else {
                    ClassModel classModel = (ClassModel) getById(className);
                    classModel.setTextFill(Color.BLACK);
                }
            }
//        // Inconsistency of messages vs class operations(methods)
        for(UMLMessage message: App.sequenceDiagram.getSyncMssages()) {
            UMLOperation operation = message.getMessage();
            UMLClass umlClass = App.diagram.findClass(message.getTo().getName());
            UMLOperation classOperation = umlClass.getOperationByName(operation.getName());
            if(classOperation != null) {
                MethodModel methodModel = (MethodModel) getById(operation.getName() + "_message_");
                // TODO check types of params and return types
            } else {
                Pane pane = (Pane) getById(operation.getName() + "_messagePane_");
                if(pane == null) {
//                    node.setStyle("-fx-background-color: red");
                } else {
                    for (Node node : pane.getChildren()) {
                        node.setStyle("-fx-background-color: red");
                    }
                }
            }
        }

//        for(UMLMessage message: App.sequenceDiagram.getAsyncMssages()) {
//            UMLOperation operation = message.getMessage();
//            UMLClass umlClass = App.diagram.findClass(message.getTo().getName());
//            UMLOperation classOperation = umlClass.getOperationByName(operation.getName());
//            if(classOperation != null) {
//                MethodModel methodModel = (MethodModel) getById(operation.getName() + "_message_");
//                // TODO check types of params and return types
//            } else {
//                Pane pane = (Pane) getById(operation.getName() + "_messagePane_");
//                for(Node node : pane.getChildren()) {
//                    node.setStyle("-fx-background-color: red");
//                }
//            }
//        }

        }
    }

    private void classButtons() {
        getById("btnCreateDiagram").setVisible(false);
        getById("btnAddClass").setVisible(true);
        getById("asociation").setVisible(true);
        getById("directed").setVisible(true);
        getById("aggregation").setVisible(true);
        getById("composition").setVisible(true);
        getById("generalization").setVisible(true);
        if(App.sequenceDiagram != null) {
            getById("btnCheckInconsistency").setVisible(true);
        }
    }

    private void sequenceButtons() {
        getById("btnCreateSeqDiagram").setVisible(false);
        getById("btnAddActor").setVisible(true);
        getById("btnAddSyncMessage").setVisible(true);
        getById("btnAddAsyncMessage").setVisible(true);
        getById("btnAddReturnMessage").setVisible(true);
        if(App.diagram != null) {
            getById("btnCheckInconsistency").setVisible(true);
        }
    }

    private void loadButtons() {
        classButtons();
        sequenceButtons();
    }

    @FXML
    private void createDiagram() {
        TextInputDialog popUp = Dialogs.createSimpleTextDialog("Create new project:", "Diagram name: ");
        Dialogs.fixDialog(popUp);
        Optional<String> result = popUp.showAndWait();
        if (result.isPresent()) {
            App.diagram = new ClassDiagram(result.get());
            Label label = new Label(result.get());
            ((AnchorPane) App.scene.lookup("#mainArea")).getChildren().add(label);
            classButtons();
        }
        Dialogs.createDataTypes();
    }

    // ================== CLASS DIAGRAM ==================

    /**
     * Function which creates every model to display class diagram from loaded json file.
     */
    private void drawClassDiagram() {
        Label diagramName = new Label(App.diagram.getName());
        ((AnchorPane) App.scene.lookup("#mainArea")).getChildren().add(diagramName);
        for (UMLClass umlClass : App.diagram.getClassList()) {
            ClassModel classModel = createClassModel(umlClass);
            for (UMLAttribute attribute : umlClass.getAttributes()) {
                createAttributeModel(classModel, attribute);
            }
            for (UMLOperation operation : umlClass.getOperations()) {
                createOperationModel(classModel, operation);
            }
        }
        for (UMLRelation relation : App.diagram.getRelations()) {
            relation(relation);
            if (relation.getType() == RelationType.GENERALIZATION) {
                for (UMLOperation operation : relation.getSecondClass().getOperations()) {
                    colorOverloading((ClassModel) getById(relation.getSecondClass().getName()), operation, null);
                }
            }
        }
    }

    private ClassModel createClassModel(UMLClass newClass) {
        Dialogs.addDataType(newClass.getName());
        ClassModel classModel = new ClassModel(newClass);
        classModel.setId(newClass.getName());
        classModel.setCollapsible(false);

        AtomicReference<Double> originalX = new AtomicReference<>((double) 0);
        AtomicReference<Double> originalY = new AtomicReference<>((double) 0);
        classModel.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> {
            classModel.setCursor(Cursor.MOVE);
            originalX.set(event.getX());
            originalY.set(event.getY());
            select(classModel);
        });

        classModel.addEventHandler(MouseEvent.MOUSE_RELEASED, event -> classModel.setCursor(Cursor.DEFAULT));
        classModel.addEventHandler(MouseEvent.MOUSE_DRAGGED, event -> {
            double distanceX = event.getX() - originalX.get();
            double distanceY = event.getY() - originalY.get();
            double x = classModel.getLayoutX() + distanceX;
            double y = classModel.getLayoutY() + distanceY;
            classModel.relocate(x, y);
        });

        classModel.setId(classModel.getText());
        ((AnchorPane) getById("mainArea")).getChildren().add(classModel);
        return classModel;
    }

    @FXML
    private void addClass() {

        TextInputDialog popUp = Dialogs.createSimpleTextDialog("Create new class", "Class name:");
        Dialogs.fixDialog(popUp);
        Optional<String> result = popUp.showAndWait();
        if (result.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Class name cannot be empty");
            alert.show();
            return;
        }
        if (App.diagram.findClass(result.get()) != null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Class already exists");
            alert.show();
            return;
        }

        UMLClass newClass = App.diagram.createClass(result.get());
        createClassModel(newClass);
        checkInconsistency();
    }

    private void createAttributeModel(ClassModel selectedClass, UMLAttribute attribute) {
        if (attribute != null) {
            AttributeModel attrModel = new AttributeModel(attribute);
            attrModel.setStyle("-fx-label-padding: 2px");
            ((TilePane) selectedClass.getContent()).getChildren().add(attrModel);

            attrModel.setOnMouseClicked(event -> {
                selectionChild = attrModel;
                attrModel.setStyle("-fx-border-width: 1; -fx-border-color: #178fd5; -fx-label-padding: 2px");
            });

            App.scene.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    attrModel.setStyle("-fx-border-width: 1; -fx-border-color: transparent; -fx-label-padding: 2px");
                }
            });

            selectedClass.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    attrModel.setStyle("-fx-border-width: 1; -fx-border-color: transparent; -fx-label-padding: 2px");
                }
            });
        }
    }

    @FXML
    private void addAttribute() {
        if (!(selection instanceof ClassModel)) return;
        ClassModel selectedClass = (ClassModel) selection; // selectne class object
        UMLAttribute attribute = Dialogs.attributeDialog(selectedClass.getUmlClass());
        createAttributeModel(selectedClass, attribute);
    }

    private OperationModel createOperationModel(ClassModel selectedClass, UMLOperation operation) {
        if (operation != null) {
            OperationModel operationModel = new OperationModel(operation);
            operationModel.setId(operation.getName());
            operationModel.setStyle("-fx-label-padding: 2px");
            ((TilePane) selectedClass.getContent()).getChildren().add(operationModel);
            operationModel.setOnMouseClicked(event -> {
                selectionChild = operationModel;
                operationModel.setStyle("-fx-border-width: 1; -fx-border-color: #178fd5; -fx-label-padding: 2px");
            });

            App.scene.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    operationModel.setStyle("-fx-border-width: 1; -fx-border-color: transparent; -fx-label-padding: 2px");
                }
            });

            selectedClass.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    operationModel.setStyle("-fx-border-width: 1; -fx-border-color: transparent; -fx-label-padding: 2px");
                }
            });
            return operationModel;
        }
        return null;
    }

    void colorOverloading(ClassModel selectedClass, UMLOperation operation, OperationModel operationModel) {
        if (operationModel != null) {
            if (!Objects.equals(selectedClass.getUmlClass().getParent(), "")) {
                if (App.diagram.findClass(selectedClass.getUmlClass().getParent()).hasOperation(operation)) {
                    operationModel.setTextFill(Color.ORANGE);
                }
            }
        }
        if (!Objects.equals(selectedClass.getUmlClass().getChild(), "")) {
            UMLClass child = App.diagram.findClass(selectedClass.getUmlClass().getChild());
            if (child != null) {
                if (child.hasOperation(operation)) {
                    ClassModel childModel = (ClassModel) getById(child.getName());
                    for (Node node : ((TilePane) childModel.getContent()).getChildren()) {
                        if (node instanceof OperationModel) {
                            if (Objects.equals(((OperationModel) node).getId(), child.getOperation(operation).getName())) {
                                ((OperationModel) node).setTextFill(Color.ORANGE);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Adds operation to selected class.
     */
    @FXML
    private void addMethod() {
        if (!(selection instanceof ClassModel)) return;
        ClassModel selectedClass = (ClassModel) selection;
        UMLOperation operation = Dialogs.operationDialog(selectedClass.getUmlClass());
        OperationModel operationModel = createOperationModel(selectedClass, operation);
        colorOverloading(selectedClass, operation, operationModel);
    }

    /**
     * Displays all components of relation based on relation type.
     *
     * @param relation UMLRelation to display
     */
    private void relation(UMLRelation relation) {

        if (relation != null) {
            ClassModel firstClass = (ClassModel) getById(relation.getFirstClass().getName());
            ClassModel secondClass = (ClassModel) getById(relation.getSecondClass().getName());
            Pane allTogether = new Pane();

            if (firstClass != null && secondClass != null) {
                Drawings.drawLine(firstClass, secondClass, relation.getName(), allTogether);
                if (relation.getType() == RelationType.ASOCIATION) {
                    Drawings.drawMultiplicity(relation.MultiplicityConverter(relation.getFirstMultiplicity()), relation.MultiplicityConverter(relation.getSecondMultiplicity()), firstClass, secondClass, allTogether);
                } else if (relation.getType() == RelationType.DIRECTED) {
                    Polygon polygon = new Polygon(0, 25, 10, 10, 10, 10, 20, 25, 20, 25, 10, 10, 10, 10, 0, 25);
                    Drawings.drawPolygon(true, firstClass, secondClass, polygon, true, allTogether);
                } else if (relation.getType() == RelationType.GENERALIZATION) {
                    Polygon polygon = new Polygon();
                    polygon.getPoints().addAll(10.0, 10.0, 20.0, 20.0, 20.0, 20.0, 0.0, 20.0, 0.0, 20.0, 10.0, 10.0);
                    Drawings.drawPolygon(true, firstClass, secondClass, polygon, true, allTogether);
                } else {
                    Polygon polygon = new Polygon();
                    polygon.getPoints().addAll(5.0, 20.0, 10.0, 30.0, 10.0, 30.0, 15.0, 20.0, 15.0, 20.0, 10.0, 10.0, 10.0, 10.0, 5.0, 20.0);
                    Drawings.drawMultiplicity(relation.MultiplicityConverter(relation.getFirstMultiplicity()), relation.MultiplicityConverter(relation.getSecondMultiplicity()), firstClass, secondClass, allTogether);
                    Drawings.drawPolygon(relation.getType() != RelationType.AGGREGATION, firstClass, secondClass, polygon, false, allTogether);
                }
                allTogether.setId(relation.getId());
                allTogether.setPickOnBounds(false);
                ((AnchorPane) getById("mainArea")).getChildren().add(allTogether);

                firstClass.increaseRelationNumber();
                secondClass.increaseRelationNumber();
            }
        }
    }

    @FXML
    private void asociation() { // obycajna relacia      1...n------------------------1
        UMLRelation relation = Dialogs.relationDialog(RelationType.ASOCIATION, true);
        relation(relation);
    }

    @FXML
    private void directed() { // obycajna relacia      1...n------------------------1
        UMLRelation relation = Dialogs.relationDialog(RelationType.DIRECTED, false);
        relation(relation);
    }

    @FXML
    private void agregation() { // prazdny kosostvorec      1...n------------------------1
        UMLRelation relation = Dialogs.relationDialog(RelationType.AGGREGATION, true);
        relation(relation);
    }

    @FXML
    private void composition() { // plny kosostvorec      1...n------------------------1
        UMLRelation relation = Dialogs.relationDialog(RelationType.COMPOSITION, true);
        relation(relation);
    }

    @FXML
    private void generalization() { // prazdna sipka BEZ kardinality
        UMLRelation relation = Dialogs.relationDialog(RelationType.GENERALIZATION, false);
        relation.getFirstClass().setParent(relation.getSecondClass().getName());
        relation.getSecondClass().setChild(relation.getFirstClass().getName());

        for (UMLOperation operation : relation.getSecondClass().getOperations()) {
            colorOverloading((ClassModel) getById(relation.getSecondClass().getName()), operation, null);
        }

        relation(relation);
    }

    /**
     * Deletes selected item from scene and class diagram.
     */
    @FXML
    private void deleteSelected() {
        if (selection instanceof ClassModel) {
            ClassModel selectedClass = (ClassModel) selection;
            UMLClass umlClass = selectedClass.getUmlClass();
            if (selectionChild instanceof AttributeModel) {
                umlClass.deleteAttribute(((AttributeModel) selectionChild).getUmlAttribute());
                ((TilePane) selectedClass.getContent()).getChildren().remove((AttributeModel) selectionChild);
            } else if (selectionChild instanceof OperationModel) {
                umlClass.deleteOperation(((OperationModel) selectionChild).getUmlOperation());
                ((TilePane) selectedClass.getContent()).getChildren().remove((OperationModel) selectionChild);
            } else {
                List<String> relationIDs = App.diagram.getDeleteIDsRelations(umlClass.getName());
                Dialogs.removeDataType(umlClass.getName());
                App.diagram.deleteClass(umlClass);
                ((AnchorPane) getById("mainArea")).getChildren().remove(selectedClass);
                for (String id : relationIDs) {
                    ((AnchorPane) getById("mainArea")).getChildren().remove(getById(id));
                }
            }
        }
        if(selection instanceof MethodModel) {
            MethodModel selectedMessage = (MethodModel) selection;
            UMLMessage umlMessage = selectedMessage.getUmlMessage();
            if (umlMessage.getReturnMessage() == null) { // asynchronous message
                UMLOperation operation = umlMessage.getMessage();
                UMLActor actorFrom = umlMessage.getFrom();
                UMLActor actorTo = umlMessage.getTo();
                actorFrom.removeMessage(operation);
                actorTo.removeMessage(operation);
                App.sequenceDiagram.deleteAsyncMessage(umlMessage);
            } else if(umlMessage.getMessageName() == null){ // return message
                UMLOperation operation = umlMessage.getReturnMessage();
                UMLActor actorFrom = umlMessage.getFrom();
                UMLActor actorTo = umlMessage.getTo();
                actorFrom.removeMessage(operation);
                actorTo.removeMessage(operation);
                App.sequenceDiagram.deleteReturnMessage(umlMessage);
            }
            else{ // sync message
                UMLOperation operation = umlMessage.getMessage();
                UMLOperation retOperation = umlMessage.getReturnMessage();
                UMLActor actorFrom = umlMessage.getFrom();
                UMLActor actorTo = umlMessage.getTo();
                actorFrom.removeMessage(operation);
                actorTo.removeMessage(operation);
                actorFrom.removeMessage(retOperation);
                actorTo.removeMessage(retOperation);
                App.sequenceDiagram.deleteSyncMessage(umlMessage);
                String deletedID = operation.getName() + "_messagePane_";
                Node delete = getById(deletedID);
                ((AnchorPane) getById("mainArea")).getChildren().remove(delete);

            }
            iteratorModel.msgCount--;
            ((AnchorPane) getById("mainArea")).getChildren().remove(selectedMessage);
        }
    }

    // ========================= SEQUENCE DIAGRAM ======================

    /**
     * Function creates every model to display sequence diagram from loaded json file.
     */
    private void drawSequenceDiagram() {
        double MoveX = App.scene.getWidth() / 2;
        for (UMLActor umlActor : App.sequenceDiagram.getActorList()) {
            createActorModel(umlActor, MoveX);
            MoveX += 150;
        }
        for (UMLMessage umlMessage : App.sequenceDiagram.getSyncMssages()) {
            createSyncMessage(umlMessage);
        }
        for (UMLMessage umlMessage : App.sequenceDiagram.getAsyncMssages()) {
            createAsyncMessage(umlMessage);
        }
        for (UMLMessage umlMessage : App.sequenceDiagram.getReturnMessages()) {
            createReturnMessage(umlMessage);
        }
    }

    /**
     * Function creates an actor from loaded json file
     * @param actor actor
     * @param MoveX space between actors
     */
    private void createActorModel(UMLActor actor, double MoveX) {
        ActorModel actorModel = new ActorModel(actor);
        actorModel.setCollapsible(false);

        AtomicReference<Double> originalX = new AtomicReference<>((double) 0);
        AtomicReference<Double> originalY = new AtomicReference<>((double) 0);
        actorModel.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> {
            actorModel.setCursor(Cursor.MOVE);
            originalX.set(event.getX());
            originalY.set(event.getY());
            select(actorModel);
        });

        TimeLineModel timeLineModel = new TimeLineModel(0, 0, 0, 180);
        ContainerModel container = new ContainerModel(actorModel, timeLineModel);
        container.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> {
            container.setCursor(Cursor.MOVE);
            originalX.set(event.getX());
            originalY.set(event.getY());
            select(container);
        });
        container.addEventHandler(MouseEvent.MOUSE_RELEASED, event -> container.setCursor(Cursor.DEFAULT));
        container.addEventHandler(MouseEvent.MOUSE_DRAGGED, event -> {
            double distanceX = event.getX() - originalX.get();
            double distanceY = event.getY() - originalY.get();
            double x = container.getLayoutX() + distanceX;
            double y = container.getLayoutY() + distanceY;
            container.relocate(x, y);
        });

        container.setLayoutX(MoveX);
        container.setLayoutY(100);

        AnchorPane mainArea = (AnchorPane) App.scene.lookup("#mainArea");
        mainArea.getChildren().add(container);
    }

    /**
     * Function creates sychronnous message from a loaded json file
     * @param msg message
     */
    private void createSyncMessage(UMLMessage msg) {
        drawMessage(msg, true, true, true);
    }

    /**
     * Function creates asynchronous message from a loaded file
     * @param msg message
     */
    private void createAsyncMessage(UMLMessage msg) {
        drawMessage(msg, true, false, false);
    }

    /**
     * Function creates return message from a loaded file
     * @param msg
     */
    private void createReturnMessage(UMLMessage msg) {
        drawMessage(msg, false, true, false);
    }

    /**
     * Creates a new sequence diagram
     */
    @FXML
    private void createSeqDiagram() {
        TextInputDialog popUp = Dialogs.createSimpleTextDialog("Create Sequence Diagram:", "Sequence diagram name:");
        Optional<String> result = popUp.showAndWait();
        if (result.isEmpty()) return;
        App.scene.lookup("#btnCreateSeqDiagram").setVisible(false);
        App.sequenceDiagram = new SequenceDiagram(result.get());
        Label label = new Label(result.get());
        label.setLayoutX(App.scene.getWidth() / 2);
        ((AnchorPane) App.scene.lookup("#mainArea")).getChildren().add(label);
        sequenceButtons();
    }

    /**
     * Adds an actor to diagram
     */
    @FXML
    private void addActor() {

        // NAME OF ACTOR
        TextInputDialog popUpAddActor = Dialogs.createSimpleTextDialog("Create new actor", "Actor name:");
        Optional<String> result = popUpAddActor.showAndWait();
        if (result.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Actor name cannot be empty");
            alert.show();
            return;
        }
        if (App.sequenceDiagram.findActor(result.get() + "_actorModel_") != null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Class already exists");
            alert.show();
            return;
        }

        if(App.diagram != null) {
            if ((getById(result.get())) == null) {
                if(!Dialogs.yesOrNo(Dialogs.MessageEnum.ACTOR_DOESNT_MATCH_CLASS))
                    return;
//                    actorModel.setTextFill(Color.RED);
            }
        }

        // ACTOR
        UMLActor actor = App.sequenceDiagram.createActor(result.get());
        ActorModel actorModel = new ActorModel(actor);
        actorModel.setCollapsible(false);

        AtomicReference<Double> originalX = new AtomicReference<>((double) 0);
        AtomicReference<Double> originalY = new AtomicReference<>((double) 0);
        actorModel.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> {
            actorModel.setCursor(Cursor.MOVE);
            originalX.set(event.getX());
            originalY.set(event.getY());
            select(actorModel);
        });

        TimeLineModel timeLineModel = new TimeLineModel(0, 0, 0, 180);
        ContainerModel container = new ContainerModel(actorModel, timeLineModel);
        container.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> {
            container.setCursor(Cursor.MOVE);
            originalX.set(event.getX());
            originalY.set(event.getY());
            select(container);
        });
        container.addEventHandler(MouseEvent.MOUSE_RELEASED, event -> container.setCursor(Cursor.DEFAULT));
        container.addEventHandler(MouseEvent.MOUSE_DRAGGED, event -> {
            double distanceX = event.getX() - originalX.get();
            double distanceY = event.getY() - originalY.get();
            double x = container.getLayoutX() + distanceX;
            double y = container.getLayoutY() + distanceY;
            container.relocate(x, y);
        });

        AnchorPane mainArea = (AnchorPane) App.scene.lookup("#mainArea");
        mainArea.getChildren().add(container);
        checkInconsistency();
    }

    /**
     * Draw a message
     * @param umlMessage message
     * @param isDirect direct message
     * @param isReturn return message
     * @param isSync synchronous message
     */
    private void drawMessage(UMLMessage umlMessage, Boolean isDirect, Boolean isReturn, Boolean isSync) {
        UMLActor acF = umlMessage.getFrom();
        UMLActor acT = umlMessage.getTo();

        ActorModel actorFrom = (ActorModel) getById(umlMessage.getFrom().getName() + "_actorModel_");
        ActorModel actorTo = (ActorModel) getById(umlMessage.getTo().getName() + "_actorModel_");

        // getting coordinates of selected actors
        AtomicReference<Double> actorFromX = new AtomicReference<>(), actorFromY = new AtomicReference<>(), actorToX = new AtomicReference<>(), actorToY = new AtomicReference<>();
        if(isDirect) {
            actorFromX = new AtomicReference<>(actorFrom.getParent().getLayoutX());
            actorFromY = new AtomicReference<>(actorFrom.getParent().getLayoutY());
            actorToX = new AtomicReference<>(actorTo.getParent().getLayoutX());
            actorToY = new AtomicReference<>(actorTo.getParent().getLayoutY());
        }
        else if(isReturn){
            actorFromX = new AtomicReference<>(actorTo.getParent().getLayoutX());
            actorFromY = new AtomicReference<>(actorTo.getParent().getLayoutY());
            actorToX = new AtomicReference<>(actorFrom.getParent().getLayoutX());
            actorToY = new AtomicReference<>(actorFrom.getParent().getLayoutY());
        }
        AtomicReference<Double> originalX = new AtomicReference<>((double) 0);
        AtomicReference<Double> originalY = new AtomicReference<>((double) 0);
        double arrowWidth = actorToX.get() - actorFromX.get();
        Pane pane = new Pane();
        pane.setPickOnBounds(false);

        if (isDirect) {
            pane.setId(umlMessage.getMessageName() + "_messagePane_");
            acF.addMessage(umlMessage.getMessage());
            acT.addMessage(umlMessage.getMessage());
            // DIRECT ARROW & MESSAGE
            // ARROW
            Line upper = new Line(arrowWidth - 10, 10, arrowWidth, 20);
            Line main = new Line(0, 20, arrowWidth, 20);
            Line bellow = new Line(arrowWidth - 10, 30, arrowWidth, 20);
            Pane arrow = new Pane(main, upper, bellow);

            // MESSAGE
            String definiteMessage = umlMessage.getMessageName() + "(" + umlMessage.getMessage().getParameters() + ")";
            Label msg = new Label(definiteMessage);
            msg.setLayoutX(30);
            msg.setLayoutY(0);
            msg.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> {
                select(msg);
            });

            MethodModel arrowAndLabel = new MethodModel(arrow, msg, umlMessage);
            arrowAndLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
                arrowAndLabel.setCursor(Cursor.MOVE);
                originalX.set(event.getX());
                originalY.set(event.getY());
                select(arrowAndLabel);
            });
            arrowAndLabel.addEventHandler(MouseEvent.MOUSE_RELEASED, event -> arrowAndLabel.setCursor(Cursor.DEFAULT));
            arrowAndLabel.addEventHandler(MouseEvent.MOUSE_DRAGGED, event -> {
                double distanceY = event.getY() - originalY.get();
                double y = arrowAndLabel.getLayoutY() + distanceY;
                arrowAndLabel.relocate(arrowAndLabel.getLayoutX(), y);
            });

            arrowAndLabel.setLayoutX(actorFromX.get() + 15);
            arrowAndLabel.setLayoutY(actorFromY.get() + iteratorModel.getSpace());
            if (isSync)
                pane.getChildren().add(arrowAndLabel);
            else
                ((AnchorPane) getById("mainArea")).getChildren().add(arrowAndLabel);
        }
        if (isReturn) {
            acF.addMessage(umlMessage.getReturnMessage());
            acT.addMessage(umlMessage.getReturnMessage());
            // RETURN ARROW AND MESSAGE
            // ARROW
            Line upperReturn = new Line(0, 20, 10, 10);
            Line mainReturn = new Line(0, 20, arrowWidth, 20);
            mainReturn.getStrokeDashArray().addAll(4d, 2d);
            Line bellowReturn = new Line(0, 20, 10, 30);
            Pane returnArrow = new Pane(mainReturn, upperReturn, bellowReturn);

            // MESSAGE
            Label returnMsg = new Label(umlMessage.getReturnMessage().getName());
            returnMsg.setLayoutX(60);
            returnMsg.setLayoutY(0);
            MethodModel returnArrowAndLabel = new MethodModel(returnArrow, returnMsg, umlMessage);

            // HANDLERS
            returnArrowAndLabel.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> {
                returnArrowAndLabel.setCursor(Cursor.MOVE);
                originalX.set(event.getX());
                originalY.set(event.getY());
                select(returnArrowAndLabel);
            });
            returnArrowAndLabel.addEventHandler(MouseEvent.MOUSE_RELEASED, event -> returnArrowAndLabel.setCursor(Cursor.DEFAULT));
            returnArrowAndLabel.addEventHandler(MouseEvent.MOUSE_DRAGGED, event -> {
                double distanceY = event.getY() - originalY.get();
                double y = returnArrowAndLabel.getLayoutY() + distanceY;
                returnArrowAndLabel.relocate(returnArrowAndLabel.getLayoutX(), y);
            });

            returnArrowAndLabel.setLayoutX(actorFromX.get() + 15);
            if (isDirect) {
                iteratorModel.msgCount--; // to handle return from sync message as one action -> won't be separated
                returnArrowAndLabel.setLayoutY(actorFromY.get() + iteratorModel.getSpace() + 25);
            } else returnArrowAndLabel.setLayoutY(actorFromY.get() + iteratorModel.getSpace());
            if (isSync)
                pane.getChildren().add(returnArrowAndLabel);
            else
                ((AnchorPane) getById("mainArea")).getChildren().add(returnArrowAndLabel);
        }
        if (isSync) {
            ((AnchorPane) getById("mainArea")).getChildren().add(pane);
        }
    }


    /**
     * Adds synchronous message to chosen actors
     */
    @FXML
    private void addSyncMessage() {
        AnchorPane mainArea = (AnchorPane) App.scene.lookup("#mainArea");

        // CREATING MESSAGE
        CreateMessageDialog dialog = new CreateMessageDialog();
        UMLMessage messageFromDialog = dialog.messageDialog("Synchronous", true, true);
        if (messageFromDialog == null) return;
        drawMessage(messageFromDialog, true, true, true);
    }

    /**
     * Adds asycnhronous message to chosen actors
     */
    @FXML
    private void addAsyncMessage() {
        AnchorPane mainArea = (AnchorPane) App.scene.lookup("#mainArea");

        // CREATING MESSAGE
        CreateMessageDialog dialog = new CreateMessageDialog();
        UMLMessage messageFromDialog = dialog.messageDialog("Asynchronous", true, false);
        if (messageFromDialog == null) return;

        drawMessage(messageFromDialog, true, false, false);
    }

    /**
     * Adds return message to chosen actors
     */
    @FXML
    private void addReturnMessage() {
        AnchorPane mainArea = (AnchorPane) App.scene.lookup("#mainArea");

        // CREATING MESSAGE
        CreateMessageDialog dialog = new CreateMessageDialog();
        UMLMessage messageFromDialog = dialog.messageDialog("Return", false, true);
        if (messageFromDialog == null) return;

        drawMessage(messageFromDialog, false, true, false);
    }

    @FXML
    private void deleteActor() {

    }
}