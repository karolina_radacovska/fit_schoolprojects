IJA - projekt

VUT FIT - IJA projekt
Týmový projekt

Vývojové prostředí
IntelliJ IDEA

Členové
xradac00 Karolína Radačovská
xsnase07 Iveta Snášelová

Popis projektu
Návrh a implementace aplikace pro zobrazení a editaci diagramů tříd a sekvenčního diagramu.

Instalace
Překlad: mvn clean install
Spuštění: v složce dest se vytvoří spustitelný jar
