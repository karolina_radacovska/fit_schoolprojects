## Zadání projektu
Zadání: Navrhněte a implementujte aplikaci pro zobrazení a editaci diagramů tříd a sekvenčního diagramu.
Poznámka: Zadání definuje podstatné vlastnosti aplikace, které musí být splněny. Předpokládá se, že detaily řešení si doplní řešitelské týmy. Nejasnosti v zadání řešte, prosím, primárně na k tomu příslušném Fóru.

Specifikace základních požadavků

Obecné požadavky
Aplikace umožňuje načítat, editovat a ukládat diagramy tříd a sekvenční diagramy z jazyka UML.
Pro zápis diagramů do souboru zvolte libovolnou vhodnou syntax, inspirace např zde: https://real-world-plantuml.com/umls/4613222493585408
Základní mód aplikace je práce s jedním diagramem tříd, ke kterému může být zobrazeno více sekvenčních diagramů modelujících různé scénáře vycházející z diagramu tříd. Prvky sekvenčního diagramu odpovídají diagramu tříd – objekt v sekvenčním diagramu je instancí třídy z diagramu tříd, objekt přijímá zprávu, pokud třída objektu implementuje odpovídající metodu apod.
Při interaktivních zásazích (editaci) musí být zohledněna provázanost diagramu tříd a sekvenčních diagramů. Např. při změně názvu třídy v diagramu tříd se změní i názvy v sekvenčních diagramech.
Implementujte operaci undo (alespoň pro základní editační operace, např. vytvoření třídy, operace, změna názvu).
Diagram tříd
Aplikace načte ze souboru textové parametry diagramu třídy a zobrazí jej se všemi potřebnými komponentami, tj. název třídy, atributy s datovými typy vč. podpory čtyř možných modifikátorů přístupu ( + , - , # , ~ ) a metody. Obdobně pro rozhraní.
Třídy/Rozhraní mohou být dále propojeny pomocí vztahů: asociace, agregace, kompozice a generalizace.
V případě generalizace budou přepsané metody v odvozených třídách vizuálně odlišeny.
Sekvenční diagram
Aplikace načte textové parametry sekvenčního diagramu a zobrazí jej se všemi potřebnými komponentami (časová osa, objekty).
Objekty mohou spolu dále interagovat pomocí zpráv, které mohou být těchto typů: synchronní zpráva, asynchronní zpráva, návrat zprávy, tvorba objektu, zánik (uvolnění) objektu.
Implementujte podporu aktivace, vytváření a rušení objektů.
Řešení nekonzistence mezi sekvenčními diagramy a diagramem tříd
Pokud se při načítání diagramů ze souborů nalezne nekonzistence, diagram se přesto načte a zobrazí celý – části, které jsou nekonzistentní, se graficky zvýrazní (např. barevně).
Pokud by interaktivní zásah způsobil nekonzistenci (v sekvenčním diagramu chci zaslat zprávu, která neodpovídá diagramu tříd, v diagramu tříd ruším třídu, jejíž instance se používají v sekvenčním diagramu apod.), lze to řešit následovně:
aplikace na to upozorní a dá na výběr, zda skutečně provést
zruší se vazba mezi prvky a tyto prvky se graficky zvýrazní
aplikace nabídne možnost doplnit metodu do třídy
…
Je nutné, aby výše uvedené nekonzistence aplikace detekovala a nabídla řešení. Konkrétní způsob řešení je ponechán na řešitelském týmu.
Rozšíření pro tříčlenný tým

Implementujte podporu diagramu komunikace za stejných podmínek jako sekvenční diagram (ukládání a načítání ze souboru, provázanost s diagramem tříd). Diagram komunikace musí obsahovat minimálně objekty, spojení, zprávy (stejných typů jako u sekvenčního diagramu včetně číslování zpráv) a účastníky (users).
Sofistikované řešení nekonzistencí - nestačí pouze označit, ale, pokud je to možné, nabídnout a provést řešení (přidání metody do třídy apod.)
Minimální požadavky pro získání zápočtu

Návrh a (alespoň minimalistická) implementace všech základních požadavků uvedených ve specifikaci.
Součástí hodnocení bude způsob zpracování požadavků a kvalita návrhu a implementace.





### Úkol 3
Zadání úkolu

Úkol je týmový, odevzdává pouze vedoucí týmu.
Implementujte část projektu podle specifikace požadavků vytvořené v rámci 2. úkolu.
Pro realizaci projektu použijte Java SE 11.
Pro grafické uživatelské rozhraní použijte JavaFX, příp. Swing.
Pokud dojde k odchylce od plánu, upravte specifikaci požadavků a doplňte zdůvodnění.
Pokyny k vypracování úkolu

Úkol bude tvořen následující adresářovou strukturou, která bude umístěna v kořenovém adresáři, jehož název odpovídá loginu vedoucího týmu:
 src/             (adres.) zdrojové soubory (hierarchie balíků)
 data/            (adres.) připravené datové soubory
 build/           (adres.) přeložené class soubory
 doc/             (adres.) programová dokumentace
 dest/            (adres.) umístění výsledného jar archivu (+ dalších potřebných souborů) po kompilaci aplikace, 
                           tento adresář bude představovat adresář spustitelné aplikace
 lib/             (adres.) externí soubory a knihovny (balíky třetích stran, obrázky apod.), které vaše aplikace využívá
 readme.txt       (soubor) základní popis projektu (název, členové týmu, ...)
                           informace ke způsobu překladu a spuštění aplikace
 rozdeleni.txt    (soubor) soubor obsahuje rozdělení bodů mezi členy týmu; 
                           pokud tento soubor neexistuje, předpokládá se rovnoměrné rozdělení
 requirements.pdf (soubor) aktualizovaný seznam požadavků
Všechny zdrojové soubory musí obsahovat na začátku dokumentační komentář se jmény autorů a popisem obsahu.
Součástí bude programová dokumentace vygenerovaná nástrojem javadoc. Dokumentace bude uložena v adresáři doc.
Pokud vaše řešení vyžaduje další externí soubory (obrázky, jiné balíky apod.), umístěte je do adresáře lib.
Pro překlad a spuštění využijte nástroj ant nebo maven. V souboru readme.txt specifikujte konkrétní způsob překladu a spuštění.
Přeložení aplikace:
zkompilují se zdrojové texty, class soubory budou umístěny v adresáři build
vygenerujete se programová dokumentace a uloží se do adresáře doc
vytvoří se jar archiv s názvem ija-app.jar v adresáři dest
Spuštění aplikace:
spouštět se bude vytvořený archiv jar
po spuštění se
buď spustí demonstrace vlastností (lze využít JUnit apod.) v textovém režimu
nebo se spustí GUI včetně okna popisující možnosti použití/demonstrace (co může uživatel udělat a jak)
Pokyny k odevzdání

Adresářovou strukturu umístěte do kořenového adresáře, jehož název odpovídá loginu vedoucího týmu (xloginXX). Pro obsah podadresářů platí:
Adresáře build, doc a dest budou prázdné.
V adresáři data budou ukázkové datové soubory (pokud je zadání vyžaduje).
Adresář lib
obsahuje pouze skript s názvem get-libs.sh, který po spuštění stáhne z internetu požadované externí knihovny či soubory (předpokládejte prostředí Unix/Linux a přítomnost nástroje wget a zip)
externí knihovny lze stáhnout přímo ze zdroje, další soubory můžete dočasně umístit na váš webový prostor
pokud vaše řešení žádné další knihovny a soubory nepožaduje, nedělá skript nic
Kořenový adresář zabalte do archivu zip:
Název archivu bude stejný jako název kořenového adresáře (s příponou .zip), tj. login vedoucího týmu (např. xloginXX.zip). Po rozbalení archivu vznikne adresářová struktura definovaná výše včetně adresáře xloginXX.
Archiv zip odevzdá pouze vedoucí týmu do WIS FIT, termín Úkol 3.
Hodnocení

Úkol je hodnocen jako celek, každý člen týmu získává stejný počet bodů. Hodnocení každého člena týmu lze přerozdělit:
přerozdělení bodů je možné v rozsahu -50% až +50%
při přerozdělení může být vzata v úvahu aktivita jednotlivých členů
přerozdělení bodů musí být zapsáno v souboru rozdeleni.txt v následujícím formátu (uvádí se procenta, která získává člen týmu; součet musí odpovídat hodnotě pocet_clenu * 100):
