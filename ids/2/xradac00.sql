DROP TABLE "supervisor_on_shift";
DROP TABLE "prisoner_causes_incident";
DROP TABLE "placement_to_solitary_confinement";
DROP TABLE "supervisor";
DROP TABLE "record_of_intolerance";
DROP TABLE "incident";
DROP TABLE "shift";
DROP TABLE "crime";
DROP TABLE "prisoner";
DROP TABLE "cell";
DROP TABLE "solitary_confinement";
DROP SEQUENCE "seq_prisoner";


CREATE TABLE "solitary_confinement" (
    "id" NUMBER PRIMARY KEY
);

CREATE TABLE "cell" (
    "id" INT GENERATED AS IDENTITY NOT NULL PRIMARY KEY,
    "number_of_prisoners" INT DEFAULT 0
            CHECK ( "number_of_prisoners" BETWEEN 0 and 4 ),
    "id_solitary_confinement" INT DEFAULT NULL,
	CONSTRAINT "id_solitary_confinement_fk" FOREIGN KEY ("id_solitary_confinement") REFERENCES "solitary_confinement" ("id") ON DELETE CASCADE
);

CREATE TABLE "prisoner" (
    "id" INT NOT NULL PRIMARY KEY,
    "security_degree" INT NOT NULL
        CHECK ("security_degree" BETWEEN 1 AND 5),
    "id_cell" INT NOT NULL,
    CONSTRAINT "id_cell_fk" FOREIGN KEY ("id_cell") REFERENCES "cell" ("id") ON DELETE SET NULL
);

CREATE TABLE "crime" (
    "id" INT GENERATED AS IDENTITY NOT NULL PRIMARY KEY,
    "type" VARCHAR(16) NOT NULL
        CHECK ( "type" IN ('MURDER', 'MURDER ATTEMPT', 'THEFT', 'ABUSE', 'KIDNAP', 'DRUG CRIME', 'CYBERCRIME', 'TAX EVASION') ),
    "sentence_start_time" TIMESTAMP NOT NULL,
    "sentence_end_time" TIMESTAMP NOT NULL,
    "sentence_duration" INTERVAL DAY TO SECOND GENERATED ALWAYS AS ( "sentence_end_time" - "sentence_start_time" ),
    "id_prisoner" INT NOT NULL,
    CONSTRAINT "id_prisoner_fk" FOREIGN KEY ("id_prisoner") REFERENCES "prisoner" ("id") ON DELETE CASCADE
);

CREATE TABLE "shift" (
    "id" INT GENERATED AS IDENTITY NOT NULL PRIMARY KEY,
    "start_time" interval day (0) to second(0) NOT NULL,
    "end_time" interval day (0) to second(0) NOT NULL
);

CREATE TABLE "incident" (
    "id" INT GENERATED AS IDENTITY NOT NULL PRIMARY KEY,
    "type" VARCHAR(16) NOT NULL
        CHECK ( "type" IN ('STAFF VIOLENCE', 'GANG RIVALRIES', 'SMUGGLING', 'PRISONER FIGHT', 'ESCAPE ATTEMPT', 'ABUSE', 'SELF-HARM') ),
    "date" TIMESTAMP NOT NULL,
    "id_shift" INT NOT NULL,
    CONSTRAINT "id_shift_fk" FOREIGN KEY ("id_shift") REFERENCES "shift" ("id") ON DELETE SET NULL
);

CREATE TABLE "record_of_intolerance" (
    "id" INT GENERATED AS IDENTITY NOT NULL PRIMARY KEY,
    "start_of_increased_security" TIMESTAMP NOT NULL,
    "end_of_increased_security" TIMESTAMP NOT NULL,
    "id_prisoner_1" INT NOT NULL,
    "id_prisoner_2" INT NOT NULL,
    "id_incident" INT NOT NULL,
    CONSTRAINT "id_prisoner1_fk" FOREIGN KEY ("id_prisoner_1") REFERENCES "prisoner" ("id") ON DELETE SET NULL,
    CONSTRAINT "id_prisoner2_fk" FOREIGN KEY ("id_prisoner_2") REFERENCES "prisoner" ("id") ON DELETE SET NULL,
    CONSTRAINT "id_incident_fk" FOREIGN KEY ("id_incident") REFERENCES "incident" ("id") ON DELETE SET NULL
);

CREATE TABLE "supervisor" (
    "id" INT GENERATED AS IDENTITY NOT NULL PRIMARY KEY
);

CREATE TABLE "placement_to_solitary_confinement" (
    "id" INT GENERATED AS IDENTITY NOT NULL PRIMARY KEY,
    "start_time" TIMESTAMP NOT NULL,
    "end_time" TIMESTAMP NOT NULL,
    "id_incident" INT NOT NULL,
    "id_solitary_confinement" INT NOT NULL,
    CONSTRAINT "id_incident_solitary_fk" FOREIGN KEY ("id_incident") REFERENCES "incident" ("id") ON DELETE SET NULL,
    CONSTRAINT "id_solitary_confinement_solitary_fk" FOREIGN KEY ("id_solitary_confinement") REFERENCES "solitary_confinement" ("id") ON DELETE SET NULL
);

CREATE TABLE "prisoner_causes_incident" (
    "id_incident" INT NOT NULL,
    "id_prisoner" INT NOT NULL,
    CONSTRAINT "id_cause_fk" PRIMARY KEY ("id_incident", "id_prisoner"),
    CONSTRAINT "id_prisoner_cause_fk" FOREIGN KEY ("id_prisoner") REFERENCES "prisoner" ("id") ON DELETE SET NULL,
    CONSTRAINT "id_incident_cause_fk" FOREIGN KEY ("id_incident") REFERENCES "incident" ("id") ON DELETE CASCADE
);

CREATE TABLE "supervisor_on_shift" (
    "id_supervisor" INT NOT NULL,
    "id_shift" INT NOT NULL,
    CONSTRAINT "id_on_fk" PRIMARY KEY ("id_supervisor", "id_shift"),
    CONSTRAINT "id_supervisor_on_fk" FOREIGN KEY ("id_supervisor") REFERENCES "supervisor" ("id") ON DELETE SET NULL,
    CONSTRAINT "id_shift_on_fk" FOREIGN KEY ("id_shift") REFERENCES "shift" ("id") ON DELETE SET NULL
);

CREATE SEQUENCE "seq_prisoner" INCREMENT BY 1 START WITH 1 NOMAXVALUE MINVALUE 0;
CREATE OR REPLACE TRIGGER "trig_prisoner" BEFORE INSERT ON "prisoner" FOR EACH ROW
begin
  if :NEW."id" is null then
    SELECT "seq_prisoner".nextval INTO :NEW."id" FROM DUAL;
  end if;
end "trig_prisoner";


INSERT INTO "solitary_confinement" ("id") VALUES (1);
INSERT INTO "solitary_confinement" ("id") VALUES (2);

INSERT INTO "cell" ("number_of_prisoners", "id_solitary_confinement") VALUES (0, 1);
INSERT INTO "cell" ("number_of_prisoners", "id_solitary_confinement") VALUES (1, 2);
INSERT INTO "cell" ("number_of_prisoners", "id_solitary_confinement") VALUES (2, NULL);
INSERT INTO "cell" ("number_of_prisoners", "id_solitary_confinement") VALUES (3, NULL);

INSERT INTO "prisoner" ("id", "security_degree", "id_cell") VALUES ("seq_prisoner".nextval, 1, 1);
INSERT INTO "prisoner" ("id", "security_degree", "id_cell") VALUES ("seq_prisoner".nextval, 2, 2);
INSERT INTO "prisoner" ("id", "security_degree", "id_cell") VALUES ("seq_prisoner".nextval, 3, 2);
INSERT INTO "prisoner" ("id", "security_degree", "id_cell") VALUES ("seq_prisoner".nextval, 4, 3);
INSERT INTO "prisoner" ("id", "security_degree", "id_cell") VALUES ("seq_prisoner".nextval, 5, 3);

INSERT INTO "crime" ("type", "sentence_start_time", "sentence_end_time", "id_prisoner") VALUES ('MURDER', TO_DATE('1947-06-14', 'yyyy/mm/dd'), TO_DATE('1977-06-14', 'yyyy/mm/dd'), 1);
INSERT INTO "crime" ("type", "sentence_start_time", "sentence_end_time", "id_prisoner") VALUES ('ABUSE', TO_DATE('1938-05-01', 'yyyy/mm/dd'), TO_DATE('1962-05-01', 'yyyy/mm/dd'), 2);
INSERT INTO "crime" ("type", "sentence_start_time", "sentence_end_time", "id_prisoner") VALUES ('THEFT', TO_DATE('1956-01-25', 'yyyy/mm/dd'), TO_DATE('1961-01-25', 'yyyy/mm/dd'), 3);
INSERT INTO "crime" ("type", "sentence_start_time", "sentence_end_time", "id_prisoner") VALUES ('KIDNAP', TO_DATE('1955-08-22', 'yyyy/mm/dd'), TO_DATE('1970-08-22', 'yyyy/mm/dd'), 4);
INSERT INTO "crime" ("type", "sentence_start_time", "sentence_end_time", "id_prisoner") VALUES ('CYBERCRIME', TO_DATE('1951-11-09', 'yyyy/mm/dd'), TO_DATE('1954-11-09', 'yyyy/mm/dd'), 5);

INSERT INTO "supervisor" ("id") VALUES (default);
INSERT INTO "supervisor" ("id") VALUES (default);
INSERT INTO "supervisor" ("id") VALUES (default);
INSERT INTO "supervisor" ("id") VALUES (default);
INSERT INTO "supervisor" ("id") VALUES (default);
INSERT INTO "supervisor" ("id") VALUES (default);

INSERT INTO "shift" ("start_time", "end_time") VALUES (TO_DSINTERVAL('0 06:00:00'), TO_DSINTERVAL('0 14:00:00'));
INSERT INTO "shift" ("start_time", "end_time") VALUES (TO_DSINTERVAL('0 14:00:00'), TO_DSINTERVAL('0 22:00:00'));
INSERT INTO "shift" ("start_time", "end_time") VALUES (TO_DSINTERVAL('0 22:00:00'), TO_DSINTERVAL('0 06:00:00'));

INSERT INTO "supervisor_on_shift" ("id_supervisor", "id_shift") VALUES (1, 1);
INSERT INTO "supervisor_on_shift" ("id_supervisor", "id_shift") VALUES (2, 1);
INSERT INTO "supervisor_on_shift" ("id_supervisor", "id_shift") VALUES (3, 2);
INSERT INTO "supervisor_on_shift" ("id_supervisor", "id_shift") VALUES (4, 2);
INSERT INTO "supervisor_on_shift" ("id_supervisor", "id_shift") VALUES (5, 3);
INSERT INTO "supervisor_on_shift" ("id_supervisor", "id_shift") VALUES (6, 3);

INSERT INTO "incident" ("type", "date", "id_shift") VALUES ('STAFF VIOLENCE', TO_DATE('1951-09-19', 'yyyy/mm/dd'), 1);
INSERT INTO "incident" ("type", "date", "id_shift") VALUES ('ESCAPE ATTEMPT', TO_DATE('1950-04-01', 'yyyy/mm/dd'), 3);

INSERT INTO "prisoner_causes_incident" ("id_incident", "id_prisoner") VALUES (1, 1);
INSERT INTO "prisoner_causes_incident" ("id_incident", "id_prisoner") VALUES (1, 2);
INSERT INTO "prisoner_causes_incident" ("id_incident", "id_prisoner") VALUES (1, 3);
INSERT INTO "prisoner_causes_incident" ("id_incident", "id_prisoner") VALUES (2, 4);

INSERT INTO "placement_to_solitary_confinement" ("start_time", "end_time", "id_incident", "id_solitary_confinement") VALUES (TO_DATE('1951-09-19', 'yyyy/mm/dd'), TO_DATE('1951-09-29', 'yyyy/mm/dd'), 1, 2);