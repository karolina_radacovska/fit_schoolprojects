# IDS_project
Project divided to 4 parts and final presentation with reviewer.

## Part 1
ER Diagram
Use Case Diagram

## Part 2
Create tables, insert values into tables.

## Part 3 
Add a few select queries. 

## Part 4
Add sequences, triggers, indexes and materialize view. 

