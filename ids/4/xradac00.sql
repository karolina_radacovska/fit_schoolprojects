DROP TABLE "supervisor_on_shift";
DROP TABLE "prisoner_causes_incident";
DROP TABLE "placement_to_solitary_confinement";
DROP TABLE "supervisor";
DROP TABLE "record_of_intolerance";
DROP TABLE "incident";
DROP TABLE "shift";
DROP TABLE "crime";
DROP TABLE "prisoner";
DROP TABLE "cell";
DROP TABLE "solitary_confinement";
DROP SEQUENCE "seq_prisoner";
DROP SEQUENCE cell_sequence;
DROP INDEX crime_type_index;
DROP MATERIALIZED VIEW crime_prisoner_view;
DROP MATERIALIZED VIEW LOG ON XSNASE07."crime";
DROP MATERIALIZED VIEW LOG ON XSNASE07."prisoner";

CREATE TABLE "solitary_confinement" (
    "id" NUMBER PRIMARY KEY
);

CREATE TABLE "cell" (
    "id" INT NOT NULL PRIMARY KEY,
    "number_of_prisoners" INT DEFAULT 0
            CHECK ( "number_of_prisoners" BETWEEN 0 and 4 ),
    "id_solitary_confinement" INT DEFAULT NULL,
	CONSTRAINT "id_solitary_confinement_fk" FOREIGN KEY ("id_solitary_confinement") REFERENCES "solitary_confinement" ("id") ON DELETE CASCADE
);

CREATE TABLE "prisoner" (
    "id" INT NOT NULL PRIMARY KEY,
    "security_degree" INT NOT NULL
        CHECK ("security_degree" BETWEEN 1 AND 5),
    "id_cell" INT NOT NULL,
    CONSTRAINT "id_cell_fk" FOREIGN KEY ("id_cell") REFERENCES "cell" ("id") ON DELETE SET NULL
);

CREATE TABLE "crime" (
    "id" INT GENERATED AS IDENTITY NOT NULL PRIMARY KEY,
    "type" VARCHAR(16) NOT NULL
        CHECK ( "type" IN ('MURDER', 'MURDER ATTEMPT', 'THEFT', 'ABUSE', 'KIDNAP', 'DRUG CRIME', 'CYBERCRIME', 'TAX EVASION') ),
    "sentence_start_time" TIMESTAMP NOT NULL,
    "sentence_end_time" TIMESTAMP NOT NULL,
    "sentence_duration" INTERVAL DAY(5) TO SECOND NULL,
    "id_prisoner" INT NOT NULL,
    CONSTRAINT "id_prisoner_fk" FOREIGN KEY ("id_prisoner") REFERENCES "prisoner" ("id") ON DELETE CASCADE
);

CREATE TABLE "shift" (
    "id" INT GENERATED AS IDENTITY NOT NULL PRIMARY KEY,
    "start_time" interval day (0) to second(0) NOT NULL,
    "end_time" interval day (0) to second(0) NOT NULL
);

CREATE TABLE "incident" (
    "id" INT GENERATED AS IDENTITY NOT NULL PRIMARY KEY,
    "type" VARCHAR(16) NOT NULL
        CHECK ( "type" IN ('STAFF VIOLENCE', 'GANG RIVALRIES', 'SMUGGLING', 'PRISONER FIGHT', 'ESCAPE ATTEMPT', 'ABUSE', 'SELF-HARM') ),
    "date" TIMESTAMP NOT NULL,
    "id_shift" INT NOT NULL,
    CONSTRAINT "id_shift_fk" FOREIGN KEY ("id_shift") REFERENCES "shift" ("id") ON DELETE SET NULL
);

CREATE TABLE "record_of_intolerance" (
    "id" INT GENERATED AS IDENTITY NOT NULL PRIMARY KEY,
    "start_of_increased_security" TIMESTAMP NOT NULL,
    "end_of_increased_security" TIMESTAMP NOT NULL,
    "id_prisoner_1" INT NOT NULL,
    "id_prisoner_2" INT NOT NULL,
    "id_incident" INT NOT NULL,
    CONSTRAINT "id_prisoner1_fk" FOREIGN KEY ("id_prisoner_1") REFERENCES "prisoner" ("id") ON DELETE SET NULL,
    CONSTRAINT "id_prisoner2_fk" FOREIGN KEY ("id_prisoner_2") REFERENCES "prisoner" ("id") ON DELETE SET NULL,
    CONSTRAINT "id_incident_fk" FOREIGN KEY ("id_incident") REFERENCES "incident" ("id") ON DELETE SET NULL
);

CREATE TABLE "supervisor" (
    "id" INT GENERATED AS IDENTITY NOT NULL PRIMARY KEY
);

CREATE TABLE "placement_to_solitary_confinement" (
    "id" INT GENERATED AS IDENTITY NOT NULL PRIMARY KEY,
    "start_time" TIMESTAMP NOT NULL,
    "end_time" TIMESTAMP NOT NULL,
    "id_incident" INT NOT NULL,
    "id_solitary_confinement" INT NOT NULL,
    CONSTRAINT "id_incident_solitary_fk" FOREIGN KEY ("id_incident") REFERENCES "incident" ("id") ON DELETE SET NULL,
    CONSTRAINT "id_solitary_confinement_solitary_fk" FOREIGN KEY ("id_solitary_confinement") REFERENCES "solitary_confinement" ("id") ON DELETE SET NULL
);

CREATE TABLE "prisoner_causes_incident" (
    "id_incident" INT NOT NULL,
    "id_prisoner" INT NOT NULL,
    CONSTRAINT "id_cause_fk" PRIMARY KEY ("id_incident", "id_prisoner"),
    CONSTRAINT "id_prisoner_cause_fk" FOREIGN KEY ("id_prisoner") REFERENCES "prisoner" ("id") ON DELETE SET NULL,
    CONSTRAINT "id_incident_cause_fk" FOREIGN KEY ("id_incident") REFERENCES "incident" ("id") ON DELETE CASCADE
);

CREATE TABLE "supervisor_on_shift" (
    "id_supervisor" INT NOT NULL,
    "id_shift" INT NOT NULL,
    CONSTRAINT "id_on_fk" PRIMARY KEY ("id_supervisor", "id_shift"),
    CONSTRAINT "id_supervisor_on_fk" FOREIGN KEY ("id_supervisor") REFERENCES "supervisor" ("id") ON DELETE SET NULL,
    CONSTRAINT "id_shift_on_fk" FOREIGN KEY ("id_shift") REFERENCES "shift" ("id") ON DELETE SET NULL
);

CREATE SEQUENCE "seq_prisoner" INCREMENT BY 1 START WITH 1 NOMAXVALUE MINVALUE 0;

CREATE OR REPLACE TRIGGER "trig_prisoner" BEFORE INSERT ON "prisoner" FOR EACH ROW
begin
  if :NEW."id" is null then
    SELECT "seq_prisoner".nextval INTO :NEW."id" FROM DUAL;
  end if;
end "trig_prisoner";


------ TRIGGERY ------

-- Generovani id pro celu
CREATE SEQUENCE cell_sequence;
CREATE OR REPLACE TRIGGER generate_id BEFORE
    INSERT ON "cell" FOR EACH ROW
    WHEN ( new."id" IS NULL )
BEGIN:
    new."id" := cell_sequence.nextval;
end;



-- Trigger, ktery vytvori sentence_duration na zaklade zacatku a konce trestu
--     "sentence_duration" INTERVAL DAY(5) TO SECOND GENERATED ALWAYS AS ( "sentence_end_time" - "sentence_start_time" ),
CREATE OR REPLACE TRIGGER  generate_duration BEFORE INSERT OR UPDATE ON "crime"
    FOR EACH ROW
    BEGIN
        :new."sentence_duration" :=  :new."sentence_end_time" - :new."sentence_start_time";
    end;


INSERT INTO "solitary_confinement" ("id") VALUES (1);
INSERT INTO "solitary_confinement" ("id") VALUES (2);

INSERT INTO "cell" ("number_of_prisoners", "id_solitary_confinement") VALUES (0, 1);
INSERT INTO "cell" ("number_of_prisoners", "id_solitary_confinement") VALUES (1, 2);
INSERT INTO "cell" ("number_of_prisoners", "id_solitary_confinement") VALUES (2, NULL);
INSERT INTO "cell" ("number_of_prisoners", "id_solitary_confinement") VALUES (3, NULL);

INSERT INTO "prisoner" ("id", "security_degree", "id_cell") VALUES ("seq_prisoner".nextval, 1, 1);
INSERT INTO "prisoner" ("id", "security_degree", "id_cell") VALUES ("seq_prisoner".nextval, 2, 2);
INSERT INTO "prisoner" ("id", "security_degree", "id_cell") VALUES ("seq_prisoner".nextval, 3, 2);
INSERT INTO "prisoner" ("id", "security_degree", "id_cell") VALUES ("seq_prisoner".nextval, 4, 3);
INSERT INTO "prisoner" ("id", "security_degree", "id_cell") VALUES ("seq_prisoner".nextval, 5, 3);

INSERT INTO "crime" ("type", "sentence_start_time", "sentence_end_time", "id_prisoner")
VALUES ('MURDER', TO_DATE('1947-06-14', 'yyyy/mm/dd'), TO_DATE('1977-06-14', 'yyyy/mm/dd'), 1);
INSERT INTO "crime" ("type", "sentence_start_time", "sentence_end_time", "id_prisoner")
VALUES ('MURDER', TO_DATE('1977-06-14', 'yyyy/mm/dd'), TO_DATE('2007-06-14', 'yyyy/mm/dd'), 1);
INSERT INTO "crime" ("type", "sentence_start_time", "sentence_end_time", "id_prisoner")
VALUES ('KIDNAP', TO_DATE('2007-06-14', 'yyyy/mm/dd'), TO_DATE('2017-06-14', 'yyyy/mm/dd'), 1);
INSERT INTO "crime" ("type", "sentence_start_time", "sentence_end_time", "id_prisoner")
VALUES ('ABUSE', TO_DATE('2017-06-14', 'yyyy/mm/dd'), TO_DATE('2041-06-14', 'yyyy/mm/dd'), 1);
INSERT INTO "crime" ("type", "sentence_start_time", "sentence_end_time", "id_prisoner")
VALUES ('ABUSE', TO_DATE('1938-05-01', 'yyyy/mm/dd'), TO_DATE('1962-05-01', 'yyyy/mm/dd'), 2);
INSERT INTO "crime" ("type", "sentence_start_time", "sentence_end_time", "id_prisoner")
VALUES ('THEFT', TO_DATE('1956-01-25', 'yyyy/mm/dd'), TO_DATE('1961-01-25', 'yyyy/mm/dd'), 3);
INSERT INTO "crime" ("type", "sentence_start_time", "sentence_end_time", "id_prisoner")
VALUES ('KIDNAP', TO_DATE('1961-01-25', 'yyyy/mm/dd'), TO_DATE('1971-01-25', 'yyyy/mm/dd'), 3);
INSERT INTO "crime" ("type", "sentence_start_time", "sentence_end_time", "id_prisoner")
VALUES ('MURDER ATTEMPT', TO_DATE('1955-08-22', 'yyyy/mm/dd'), TO_DATE('1964-08-22', 'yyyy/mm/dd'), 4);
INSERT INTO "crime" ("type", "sentence_start_time", "sentence_end_time", "id_prisoner")
VALUES ('CYBERCRIME', TO_DATE('1951-11-09', 'yyyy/mm/dd'), TO_DATE('1954-11-09', 'yyyy/mm/dd'), 5);
INSERT INTO "crime" ("type", "sentence_start_time", "sentence_end_time", "id_prisoner")
VALUES ('DRUG CRIME', TO_DATE('1954-11-09', 'yyyy/mm/dd'), TO_DATE('1958-11-09', 'yyyy/mm/dd'), 5);
INSERT INTO "crime" ("type", "sentence_start_time", "sentence_end_time", "id_prisoner")
VALUES ('TAX EVASION', TO_DATE('1958-11-09', 'yyyy/mm/dd'), TO_DATE('1965-11-09', 'yyyy/mm/dd'), 5);
INSERT INTO "crime" ("type", "sentence_start_time", "sentence_end_time", "id_prisoner")
VALUES ('DRUG CRIME', TO_DATE('1965-11-09', 'yyyy/mm/dd'), TO_DATE('1969-11-09', 'yyyy/mm/dd'), 5);

INSERT INTO "supervisor" ("id") VALUES (default);
INSERT INTO "supervisor" ("id") VALUES (default);
INSERT INTO "supervisor" ("id") VALUES (default);
INSERT INTO "supervisor" ("id") VALUES (default);
INSERT INTO "supervisor" ("id") VALUES (default);
INSERT INTO "supervisor" ("id") VALUES (default);

INSERT INTO "shift" ("start_time", "end_time") VALUES (TO_DSINTERVAL('0 06:00:00'), TO_DSINTERVAL('0 14:00:00'));
INSERT INTO "shift" ("start_time", "end_time") VALUES (TO_DSINTERVAL('0 14:00:00'), TO_DSINTERVAL('0 22:00:00'));
INSERT INTO "shift" ("start_time", "end_time") VALUES (TO_DSINTERVAL('0 22:00:00'), TO_DSINTERVAL('0 06:00:00'));

INSERT INTO "supervisor_on_shift" ("id_supervisor", "id_shift") VALUES (1, 1);
INSERT INTO "supervisor_on_shift" ("id_supervisor", "id_shift") VALUES (2, 1);
INSERT INTO "supervisor_on_shift" ("id_supervisor", "id_shift") VALUES (3, 2);
INSERT INTO "supervisor_on_shift" ("id_supervisor", "id_shift") VALUES (4, 2);
INSERT INTO "supervisor_on_shift" ("id_supervisor", "id_shift") VALUES (5, 3);
INSERT INTO "supervisor_on_shift" ("id_supervisor", "id_shift") VALUES (6, 3);

INSERT INTO "incident" ("type", "date", "id_shift") VALUES ('STAFF VIOLENCE', TO_DATE('1951-09-19', 'yyyy/mm/dd'), 1);
INSERT INTO "incident" ("type", "date", "id_shift") VALUES ('ESCAPE ATTEMPT', TO_DATE('1950-04-01', 'yyyy/mm/dd'), 3);

INSERT INTO "prisoner_causes_incident" ("id_incident", "id_prisoner") VALUES (1, 1);
INSERT INTO "prisoner_causes_incident" ("id_incident", "id_prisoner") VALUES (1, 2);
INSERT INTO "prisoner_causes_incident" ("id_incident", "id_prisoner") VALUES (1, 3);
INSERT INTO "prisoner_causes_incident" ("id_incident", "id_prisoner") VALUES (2, 4);

INSERT INTO "record_of_intolerance" ("start_of_increased_security", "end_of_increased_security", "id_prisoner_1", "id_prisoner_2", "id_incident")
VALUES (TO_DATE('1965-07-14', 'yyyy/mm/dd'), TO_DATE('1966-07-14', 'yyyy/mm/dd'), 1, 2, 1);
INSERT INTO "record_of_intolerance" ("start_of_increased_security", "end_of_increased_security", "id_prisoner_1", "id_prisoner_2", "id_incident")
VALUES (TO_DATE('1965-07-14', 'yyyy/mm/dd'), TO_DATE('1966-07-14', 'yyyy/mm/dd'), 1, 3, 1);
INSERT INTO "record_of_intolerance" ("start_of_increased_security", "end_of_increased_security", "id_prisoner_1", "id_prisoner_2", "id_incident")
VALUES (TO_DATE('1965-07-14', 'yyyy/mm/dd'), TO_DATE('1966-07-14', 'yyyy/mm/dd'), 2, 3, 1);

INSERT INTO "placement_to_solitary_confinement" ("start_time", "end_time", "id_incident", "id_solitary_confinement")
VALUES (TO_DATE('1951-09-19', 'yyyy/mm/dd'), TO_DATE('1951-09-29', 'yyyy/mm/dd'), 1, 2);


-- Konkrétně musí tento skript obsahovat alespoň dva dotazy využívající spojení dvou tabulek,
-- Ktere chovanky sedi za vrazdu?
SELECT "prisoner"."id"
FROM "crime",
     "prisoner"
WHERE "crime"."id_prisoner" = "prisoner"."id"
  and "crime"."type" = 'MURDER';

-- Ktere chovanky jsou v cele 3?
SELECT "prisoner"."id"
FROM "cell",
     "prisoner"
WHERE "cell"."id" = "prisoner"."id_cell"
  and "cell"."id" = 3;


-- jeden využívající spojení tří tabulek,
-- Ktere chovanky zpusobily incident 'STAFF VIOLENCE' a na jake smene?
SELECT "prisoner"."id", "incident"."id_shift"
FROM "prisoner",
     "incident",
     "prisoner_causes_incident"
WHERE "prisoner"."id" = "prisoner_causes_incident"."id_prisoner"
  and "incident"."id" = "prisoner_causes_incident"."id_incident"
  and "incident"."type" = 'STAFF VIOLENCE';


-- dva dotazy s klauzulí GROUP BY a agregační funkcí,
-- Kolik dozorkyn bylo na ruznych smenach a na jakych smenach?
SELECT "shift"."id" as "shift_id", COUNT("supervisor_on_shift"."id_supervisor")
FROM "shift",
     "supervisor_on_shift"
WHERE "shift"."id" = "supervisor_on_shift"."id_shift"
GROUP BY "shift"."id";


-- Kolik zaznamu o nesnasenlivosti vytvorily jednotlive incidenty?
SELECT "incident"."type", COUNT("record_of_intolerance"."id")
FROM "incident"
         LEFT JOIN "record_of_intolerance" ON "incident"."id" = "record_of_intolerance"."id_incident"
GROUP BY "incident"."type";


-- jeden dotaz obsahující predikát EXISTS a
-- Ktere chovanky nemaji zadny trest presahujici 10 let vcetne?
SELECT DISTINCT curr_prisoner."id"
FROM "prisoner" curr_prisoner,
     "crime"
WHERE curr_prisoner."id" = "crime"."id_prisoner"
  and NOT EXISTS(
        SELECT *
        FROM "prisoner",
             "crime"
        WHERE "prisoner"."id" = "crime"."id_prisoner"
          and "crime"."sentence_duration" > TO_DSINTERVAL('3650 00:00:00')
          and "prisoner"."id" = curr_prisoner."id");


-- jeden dotaz s predikátem IN s vnořeným selectem (nikoliv IN s množinou konstantních dat).
-- Jake trestne ciny spachali chovanky se stupnem ostrahy > 3
SELECT "crime"."type"
FROM "crime"
WHERE "crime"."id_prisoner" IN (SELECT "prisoner"."id" FROM "prisoner" WHERE "security_degree" > 3);


-- Predvedeni triggeru generate_id => tabulka cell bude mit generovane id od 1 do xx
SELECT * FROM "cell" ORDER BY "cell"."id";
-- Predvedeni, ze trigger funguje spravne
SELECT "sentence_duration" AS trigger_duration, "sentence_end_time"-"sentence_start_time" AS "substract_duration "FROM "crime";


-- Kolik % chovanek se zúčastnilo daného incidentu
CREATE OR REPLACE PROCEDURE prisoners_percent_caused_incident (my_type IN VARCHAR)
    AS
        all_prisoners_sum INT;
        prisoner_counter INT;
        caused_type_id "incident"."id"%TYPE;
        type_id "incident"."id"%TYPE;
        CURSOR caused_incidents_types IS SELECT "id_incident" FROM "prisoner_causes_incident";
    BEGIN
        SELECT COUNT("id") INTO all_prisoners_sum FROM "prisoner";
        prisoner_counter := 0;
        SELECT "id" INTO type_id FROM "incident" WHERE "type" = my_type;

        OPEN caused_incidents_types;
        LOOP
            FETCH caused_incidents_types INTO caused_type_id;
            EXIT WHEN caused_incidents_types%NOTFOUND;
            IF caused_type_id = type_id THEN
                prisoner_counter := prisoner_counter+1;
            END IF;
        END LOOP;
        CLOSE caused_incidents_types;
        DBMS_OUTPUT.PUT_LINE(my_type || ' was caused by ' || prisoner_counter/all_prisoners_sum*100 || ' % of all prisoners.');
    EXCEPTION WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE(my_type || ' was caused by 0% of all prisoners.');
        END;


CALL prisoners_percent_caused_incident('STAFF VIOLENCE');

-- Smaze vsechny tresne ciny, ktere spachala dana chovanka a vytiskne pocet trestnych cinu pred a po smazani a typy smazanych cinu
CREATE OR REPLACE PROCEDURE delete_crimes_caused_by_prisoner(id_prisoner INT)
    AS
        crimes_sum_before INT;
        crimes_sum_after INT;
        deleted_crime_id "crime"."id"%TYPE;
        deleted_crime_type "crime"."type"%TYPE;
        CURSOR delete_crimes IS SELECT "crime"."id", "crime"."type" FROM "crime" WHERE "id_prisoner" = id_prisoner;
    BEGIN
        SELECT COUNT("id") INTO crimes_sum_before FROM "crime";
        OPEN delete_crimes;
        LOOP
            FETCH delete_crimes INTO deleted_crime_id, deleted_crime_type;
            EXIT WHEN delete_crimes%NOTFOUND;
            DBMS_OUTPUT.PUT_LINE(deleted_crime_id || ': ' || deleted_crime_type || ' is being deleted.');
            DELETE FROM "crime" WHERE "id" = deleted_crime_id;
        END LOOP;
        CLOSE delete_crimes;
        SELECT COUNT("id") INTO crimes_sum_after FROM "crime";
        DBMS_OUTPUT.PUT_LINE('Before deleting in database was stored: ' || crimes_sum_before || ' crimes. And now there is only: ' || crimes_sum_after || '.');
    END;

CALL delete_crimes_caused_by_prisoner(3);


SELECT
    index_name,
    index_type,
    visibility,
    status
FROM
    all_indexes
WHERE
    table_name = 'crime';

-- Kolik chovanek spachalo trestny cin VRAZDU, KIDNAP NEBO ABUSE?

EXPLAIN PLAN FOR SELECT crime_type, COUNT(id_prisoner)
    FROM (
        SELECT "crime"."id_prisoner" as id_prisoner, "crime"."type" as crime_type
          FROM "crime"
                   INNER JOIN "prisoner" on "prisoner"."id" = "crime"."id_prisoner"
          GROUP BY "crime"."id_prisoner", "crime"."type"
        )
     WHERE crime_type = 'MURDER' OR crime_type = 'KIDNAP' OR crime_type = 'ABUSE'
           GROUP BY crime_type;

SELECT PLAN_TABLE_OUTPUT AS TABLE_WITHOUT_INDEX FROM TABLE(DBMS_XPLAN.DISPLAY());

CREATE INDEX crime_type_index ON "crime"("type");

EXPLAIN PLAN FOR SELECT crime_type, COUNT(id_prisoner)
    FROM (
        SELECT "crime"."id_prisoner" as id_prisoner, "crime"."type" as crime_type
          FROM "crime"
                   INNER JOIN "prisoner" on "prisoner"."id" = "crime"."id_prisoner"
          GROUP BY "crime"."id_prisoner", "crime"."type"
        )
     WHERE crime_type = 'MURDER' OR crime_type = 'KIDNAP' OR crime_type = 'ABUSE'
           GROUP BY crime_type;

SELECT PLAN_TABLE_OUTPUT AS TABLE_WITH_INDEX FROM TABLE(DBMS_XPLAN.DISPLAY());

-- Prava k tabulkam
GRANT ALL PRIVILEGES ON "supervisor_on_shift" TO XRADAC00;
GRANT ALL PRIVILEGES ON "prisoner_causes_incident" TO XRADAC00;
GRANT ALL PRIVILEGES ON "placement_to_solitary_confinement" TO XRADAC00;
GRANT ALL PRIVILEGES ON "supervisor" TO XRADAC00;
GRANT ALL PRIVILEGES ON "record_of_intolerance" TO XRADAC00;
GRANT ALL PRIVILEGES ON "incident" TO XRADAC00;
GRANT ALL PRIVILEGES ON "shift" TO XRADAC00;
GRANT ALL PRIVILEGES ON "crime" TO XRADAC00;
GRANT ALL PRIVILEGES ON "prisoner" TO XRADAC00;
GRANT ALL PRIVILEGES ON "cell" TO XRADAC00;
GRANT ALL PRIVILEGES ON "solitary_confinement" TO XRADAC00;
-- Prava k proceduram
GRANT EXECUTE ON delete_crimes_caused_by_prisoner TO XRADAC00;
GRANT EXECUTE ON prisoners_percent_caused_incident TO XRADAC00;


CREATE MATERIALIZED VIEW LOG ON XSNASE07."crime" WITH PRIMARY KEY, ROWID ("type", "id_prisoner");
CREATE MATERIALIZED VIEW LOG ON XSNASE07."prisoner" WITH PRIMARY KEY, ROWID ("security_degree");

CREATE MATERIALIZED VIEW crime_prisoner_view
    NOLOGGING
    CACHE
    BUILD IMMEDIATE
    REFRESH ON COMMIT
        ENABLE QUERY REWRITE AS
          SELECT prisoner."id" as prisoner_id, prisoner."security_degree" as security_degree, crime."type" as crime_type
          FROM XSNASE07."crime" crime, XSNASE07."prisoner" prisoner
          WHERE crime."id_prisoner" = prisoner."id";

GRANT ALL ON crime_prisoner_view TO XRADAC00;
SELECT * FROM crime_prisoner_view;
