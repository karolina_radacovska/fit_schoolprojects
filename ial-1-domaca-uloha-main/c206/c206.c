
/* ******************************* c206.c *********************************** */
/*  Předmět: Algoritmy (IAL) - FIT VUT v Brně                                 */
/*  Úkol: c206 - Dvousměrně vázaný lineární seznam                            */
/*  Návrh a referenční implementace: Bohuslav Křena, říjen 2001               */
/*  Vytvořil: Martin Tuček, říjen 2004                                        */
/*  Upravil: Kamil Jeřábek, září 2020                                         */
/*           Daniel Dolejška, září 2021                                       */
/* ************************************************************************** */
/*
** Implementujte abstraktní datový typ dvousměrně vázaný lineární seznam.
** Užitečným obsahem prvku seznamu je hodnota typu int. Seznam bude jako datová
** abstrakce reprezentován proměnnou typu DLList (DL znamená Doubly-Linked
** a slouží pro odlišení jmen konstant, typů a funkcí od jmen u jednosměrně
** vázaného lineárního seznamu). Definici konstant a typů naleznete
** v hlavičkovém souboru c206.h.
**
** Vaším úkolem je implementovat následující operace, které spolu s výše
** uvedenou datovou částí abstrakce tvoří abstraktní datový typ obousměrně
** vázaný lineární seznam:
**
**      DLL_Init ........... inicializace seznamu před prvním použitím,
**      DLL_Dispose ........ zrušení všech prvků seznamu,
**      DLL_InsertFirst .... vložení prvku na začátek seznamu,
**      DLL_InsertLast ..... vložení prvku na konec seznamu,
**      DLL_First .......... nastavení aktivity na první prvek,
**      DLL_Last ........... nastavení aktivity na poslední prvek,
**      DLL_GetFirst ....... vrací hodnotu prvního prvku,
**      DLL_GetLast ........ vrací hodnotu posledního prvku,
**      DLL_DeleteFirst .... zruší první prvek seznamu,
**      DLL_DeleteLast ..... zruší poslední prvek seznamu,
**      DLL_DeleteAfter .... ruší prvek za aktivním prvkem,
**      DLL_DeleteBefore ... ruší prvek před aktivním prvkem,
**      DLL_InsertAfter .... vloží nový prvek za aktivní prvek seznamu,
**      DLL_InsertBefore ... vloží nový prvek před aktivní prvek seznamu,
**      DLL_GetValue ....... vrací hodnotu aktivního prvku,
**      DLL_SetValue ....... přepíše obsah aktivního prvku novou hodnotou,
**      DLL_Previous ....... posune aktivitu na předchozí prvek seznamu,
**      DLL_Next ........... posune aktivitu na další prvek seznamu,
**      DLL_IsActive ....... zjišťuje aktivitu seznamu.
**
** Při implementaci jednotlivých funkcí nevolejte žádnou z funkcí
** implementovaných v rámci tohoto příkladu, není-li u funkce explicitně
 * uvedeno něco jiného.
**
** Nemusíte ošetřovat situaci, kdy místo legálního ukazatele na seznam
** předá někdo jako parametr hodnotu NULL.
**
** Svou implementaci vhodně komentujte!
**
** Terminologická poznámka: Jazyk C nepoužívá pojem procedura.
** Proto zde používáme pojem funkce i pro operace, které by byly
** v algoritmickém jazyce Pascalovského typu implemenovány jako procedury
** (v jazyce C procedurám odpovídají funkce vracející typ void).
**
**/

#include "c206.h"

int error_flag;
int solved;


/**
 * Vytiskne upozornění na to, že došlo k chybě.
 * Tato funkce bude volána z některých dále implementovaných operací.
 */
void DLL_Error() {
	printf("*ERROR* The program has performed an illegal operation.\n");
	error_flag = TRUE;
}

/**
 * Provede inicializaci seznamu list před jeho prvním použitím (tzn. žádná
 * z následujících funkcí nebude volána nad neinicializovaným seznamem).
 * Tato inicializace se nikdy nebude provádět nad již inicializovaným seznamem,
 * a proto tuto možnost neošetřujte.
 * Vždy předpokládejte, že neinicializované proměnné mají nedefinovanou hodnotu.
 *
 * @param list Ukazatel na strukturu dvousměrně vázaného seznamu
 */
void DLL_Init( DLList *list ) {
    // nastavenia na NULL zabrani spustenie kodu v nasledujucich funkciach 
    list->firstElement = NULL;
    list->lastElement = NULL;
    list->activeElement = NULL;    
}

/**
 * Zruší všechny prvky seznamu list a uvede seznam do stavu, v jakém se nacházel
 * po inicializaci.
 * Rušené prvky seznamu budou korektně uvolněny voláním operace free.
 *
 * @param list Ukazatel na inicializovanou strukturu dvousměrně vázaného seznamu
 */
void DLL_Dispose( DLList *list ) {

    DLLElementPtr currentElement = list->firstElement; // iterator
    
    // prechadzam celym zoznamom a mazem jeho jednotlive elementy
    while(currentElement != NULL){ 
        DLLElementPtr tmpNextElement = currentElement->nextElement; // novy elemeny pre odpamatanie nasledujuceho prvku
        free(currentElement); // uvolnim curr -> mozem, mam zapamatany dalsi prvok -> nestratim nit
        currentElement = tmpNextElement; // ++
    }
    // vratim vsetkym parametrom null
    list->firstElement = NULL;
    list->lastElement = NULL;
    list->activeElement = NULL;
    list = NULL;

    return;
}

/**
 * Vloží nový prvek na začátek seznamu list.
 * V případě, že není dostatek paměti pro nový prvek při operaci malloc,
 * volá funkci DLL_Error().
 *
 * @param list Ukazatel na inicializovanou strukturu dvousměrně vázaného seznamu
 * @param data Hodnota k vložení na začátek seznamu
 */
void DLL_InsertFirst( DLList *list, int data ) {
    
    // vytvarim strukturu elementu
    // naplnim jeho datovu a smernikovu cast
    DLLElementPtr newElement = calloc(1, sizeof(struct DLLElement));
    if(newElement == NULL){ // kontrola
        DLL_Error();
        return;
    }
    newElement->data = data;
    newElement->nextElement = NULL;
    newElement->previousElement = NULL; 

    DLLElementPtr oldFirstElement = list->firstElement; // drzim si smernik v pamati na povodny prvy element
    newElement->nextElement = list->firstElement; // pripojim novy element pred zvysok
    
    if(list->firstElement != NULL) // zoznam nesmie byt prazdny, aby mohol mat predchadzajuci prvok
        oldFirstElement->previousElement = newElement;
    
    list->firstElement = newElement; // v liste nastavim spravny smernik na prvy element
    
    if(list->lastElement == NULL) // ak je zoznam prazdny
        list->lastElement = newElement; // posledny element je novy element

    return;
}

/**
 * Vloží nový prvek na konec seznamu list (symetrická operace k DLL_InsertFirst).
 * V případě, že není dostatek paměti pro nový prvek při operaci malloc,
 * volá funkci DLL_Error().
 *
 * @param list Ukazatel na inicializovanou strukturu dvousměrně vázaného seznamu
 * @param data Hodnota k vložení na konec seznamu
 */
void DLL_InsertLast( DLList *list, int data ) {

    // vytvorim strukturu pre element
    // naplnim strukturu
    DLLElementPtr newElement = calloc(1, sizeof(struct DLLElement));    
    newElement->data = data;
    newElement->nextElement = NULL;
    newElement->previousElement = NULL;
    
    // prazdny zoznam znamena, ze prvy element = posledny element = novy element
    if(list->firstElement == NULL){ 
        list->firstElement = newElement; 
        list->lastElement = newElement;
        return;
    }

    // neprazdny zoznam    
    DLLElementPtr currentElement = list->firstElement; // iterator

    // dostanem sa na posledny element zoznamu
    while(currentElement->nextElement != NULL)
        currentElement = currentElement->nextElement; // ++

    // currElem je posledny, pridam za neho novy
    newElement->previousElement = currentElement; 
    currentElement->nextElement = newElement; // novy prvok je posledny prvok
    list->lastElement = newElement; 

    return;
}

/**
 * Nastaví první prvek seznamu list jako aktivní.
 * Funkci implementujte jako jediný příkaz (nepočítáme-li return),
 * aniž byste testovali, zda je seznam list prázdný.
 *
 * @param list Ukazatel na inicializovanou strukturu dvousměrně vázaného seznamu
 */
void DLL_First( DLList *list ) {

    list->activeElement = list->firstElement; // nastavim aktivny element ako prvy
    return;
}

/**
 * Nastaví poslední prvek seznamu list jako aktivní.
 * Funkci implementujte jako jediný příkaz (nepočítáme-li return),
 * aniž byste testovali, zda je seznam list prázdný.
 *
 * @param list Ukazatel na inicializovanou strukturu dvousměrně vázaného seznamu
 */
void DLL_Last( DLList *list ) {

    list->activeElement = list->lastElement; // nastavim aktivny element ako posledny
    return;
}

/**
 * Prostřednictvím parametru dataPtr vrátí hodnotu prvního prvku seznamu list.
 * Pokud je seznam list prázdný, volá funkci DLL_Error().
 *
 * @param list Ukazatel na inicializovanou strukturu dvousměrně vázaného seznamu
 * @param dataPtr Ukazatel na cílovou proměnnou
 */
void DLL_GetFirst( DLList *list, int *dataPtr ) {

    // osetrenie prazdneho zoznamu
    if(list->firstElement == NULL){
        DLL_Error();
        return;
    }

    *dataPtr = list->firstElement->data; // vratim datovy obsah prveho elementu
}

/**
 * Prostřednictvím parametru dataPtr vrátí hodnotu posledního prvku seznamu list.
 * Pokud je seznam list prázdný, volá funkci DLL_Error().
 *
 * @param list Ukazatel na inicializovanou strukturu dvousměrně vázaného seznamu
 * @param dataPtr Ukazatel na cílovou proměnnou
 */
void DLL_GetLast( DLList *list, int *dataPtr ) {

    // osetrenie prazdneho zoznamu
    if(list->firstElement == NULL){ 
        DLL_Error();
        return;
    }

    *dataPtr = list->lastElement->data; // vratim obsah posledneho elementu
    return;
}

/**
 * Zruší první prvek seznamu list.
 * Pokud byl první prvek aktivní, aktivita se ztrácí.
 * Pokud byl seznam list prázdný, nic se neděje.
 *
 * @param list Ukazatel na inicializovanou strukturu dvousměrně vázaného seznamu
 */
void DLL_DeleteFirst( DLList *list ) {

    if(list->firstElement == NULL) // ak je prazdny list -> koncim
        return;

    // mazany element je prvy, deaktivujem ho
    if((list->activeElement != NULL) && (list->firstElement == list->activeElement)){
        list->activeElement = NULL;
    }
    
    DLLElementPtr toBeDeleted = list->firstElement; // odpamatam si smernik na polozku, ktora ma byt mazana
    
    list->firstElement = list->firstElement->nextElement; // posuniem smernik

    // osetrenie predchadzajuceho smernika prveho elementu
    if(list->firstElement != NULL) 
        list->firstElement->previousElement = NULL; 

    free(toBeDeleted); // uvolnim pamat

    return;
}

/**
 * Zruší poslední prvek seznamu list.
 * Pokud byl poslední prvek aktivní, aktivita seznamu se ztrácí.
 * Pokud byl seznam list prázdný, nic se neděje.
 *
 * @param list Ukazatel na inicializovanou strukturu dvousměrně vázaného seznamu
 */
void DLL_DeleteLast( DLList *list ) {

    if(list->firstElement == NULL) // zoznam neexistuje
        return;

    // ak je aktivny posledny prvok, deaktivujem smernik
    if((list->activeElement != NULL)  && (list->activeElement == list->lastElement)){
        list->activeElement = NULL;
    }

    DLLElementPtr toBeDeleted = list->lastElement; // toto chcem zmazat

    if(list->lastElement != list->firstElement){ // v zozname je viac ako 1 prvok
        list->lastElement = list->lastElement->previousElement; // posunie lastElem na predposledny 
        // predposledny element je posledny
        // jeho next poslem na null, last element mam odchyteny v toBeDeleted
        list->lastElement->nextElement = NULL; 
    }
    else{ // v zozname je len jeden prvok
        list->lastElement = NULL;
        list->firstElement = NULL;
        list->activeElement = NULL;
    }

    free(toBeDeleted); 
    return;
}

/**
 * Zruší prvek seznamu list za aktivním prvkem.
 * Pokud je seznam list neaktivní nebo pokud je aktivní prvek
 * posledním prvkem seznamu, nic se neděje.
 *
 * @param list Ukazatel na inicializovanou strukturu dvousměrně vázaného seznamu
 */
void DLL_DeleteAfter( DLList *list ) {

    if(list->firstElement == NULL) // prazdny zoznam
        return;

    if(list->activeElement == list->lastElement) // ak je aktivny prvok posledny, skoncim
        return;

    if(list->activeElement == NULL) // ak neexistuje aktivny prvok, skoncim
        return;

    DLLElementPtr toBeDeleted = list->activeElement->nextElement; // zapamatam si prvok ZA aktivnym prvkom
    
    // posuniem smernik na element za prvkom, ktory chcem mazat
    list->activeElement->nextElement = toBeDeleted->nextElement; // 1 ---(preskocim 2)---> 3  
    
    // ak aktivny prvok nie je predposledny
    // posuniem smernik dalej o jeden element
    if(list->activeElement != list->lastElement->previousElement){
        toBeDeleted->nextElement->previousElement = toBeDeleted->previousElement;
        toBeDeleted->nextElement = NULL; // nil<----|2|---->nil
        toBeDeleted->previousElement = NULL; 
    }
    else{ 
        // inak je aktivny prvok posledny
        // mazem posledny prvok
        list->lastElement = list->activeElement;
    }
    free(toBeDeleted);

    return;
}

/**
 * Zruší prvek před aktivním prvkem seznamu list .
 * Pokud je seznam list neaktivní nebo pokud je aktivní prvek
 * prvním prvkem seznamu, nic se neděje.
 *
 * @param list Ukazatel na inicializovanou strukturu dvousměrně vázaného seznamu
 */
void DLL_DeleteBefore( DLList *list ) {

    if(list->firstElement == NULL) // kontrola prazdneho listu
        return;

    if(list->activeElement == list->firstElement) // kontola ci aktivny prvok nie je prvy
        return;

    if(list->activeElement == NULL) // osetrenie, ak aktivny prvok neexistuje
        return;

    DLLElementPtr toBeDeleted = list->activeElement->previousElement;

    // |nieco pred|<-------->(|na zmazanie|)<-------->|aktivny|<-------->|nieco za|
    // |nieco pred|<------|aktivny|
    list->activeElement->previousElement = toBeDeleted->previousElement;  
    // |nieco pred|------>|aktivny|
    if(toBeDeleted->previousElement != NULL)  // plati ak toBeDeleted != firstElement
        toBeDeleted->previousElement->nextElement = toBeDeleted->nextElement;

    // ak prvok, ktory mam vymazat je prvy element
    // urcim, ze prvy element je dalsi za mazanym elementom
    if(toBeDeleted == list->firstElement){ 
        list->firstElement = toBeDeleted->nextElement;
    }

    free(toBeDeleted);

    return;
}

/**
 * Vloží prvek za aktivní prvek seznamu list.
 * Pokud nebyl seznam list aktivní, nic se neděje.
 * V případě, že není dostatek paměti pro nový prvek při operaci malloc,
 * volá funkci DLL_Error().
 *
 * @param list Ukazatel na inicializovanou strukturu dvousměrně vázaného seznamu
 * @param data Hodnota k vložení do seznamu za právě aktivní prvek
 */
void DLL_InsertAfter( DLList *list, int data ) {
    if(list == NULL) // existuje zoznam?
        return;

    if(list->firstElement == NULL) // existuje element?
        return;

    if(!DLL_IsActive(list)) // neaktivny zoznam
        return;

    DLLElementPtr newElement = calloc(1, sizeof(struct DLLElement)); // vytvorim miesto pre novy element
     if(newElement == NULL){ // kontrola 
        DLL_Error();
        return;
    }

    // naplnim strukturu
    newElement->data = data;
    newElement->previousElement = list->activeElement; 
    newElement->nextElement = list->activeElement->nextElement;   

    if(list->activeElement == list->lastElement){ // ak aktivny prvok je posledny
        list->lastElement = newElement;
    }
    else { // aktivny prvok NIE JE posledny
        list->activeElement->nextElement->previousElement = newElement; // |new element|<-----
    }   

    list->activeElement->nextElement = newElement; // ------>|new element|

    return;
}

/**
 * Vloží prvek před aktivní prvek seznamu list.
 * Pokud nebyl seznam list aktivní, nic se neděje.
 * V případě, že není dostatek paměti pro nový prvek při operaci malloc,
 * volá funkci DLL_Error().
 *
 * @param list Ukazatel na inicializovanou strukturu dvousměrně vázaného seznamu
 * @param data Hodnota k vložení do seznamu před právě aktivní prvek
 */
void DLL_InsertBefore( DLList *list, int data ) {
    if(!DLL_IsActive(list)) // kontrola aktivneho prvku
        return;

    DLLElementPtr newElement = calloc(1, sizeof(struct DLLElement)); // vytvaram novy element
    if(newElement == NULL){ // kontrola
        DLL_Error();
        return;
    }
    newElement->data = data;
    newElement->nextElement = list->activeElement; // vlozim pred aktivny
    newElement->previousElement = list->activeElement->previousElement; // vlozim za byvaly prechadzajuci aktivneho

    if(list->activeElement == list->firstElement){ // aktivny prvok je prvy -> special case
        list->firstElement = newElement;
    }
    else{
        list->activeElement->previousElement->nextElement = newElement;
    }

    if(list->firstElement != list->lastElement){ // zoznam ma viac ako 1 prvok
        list->activeElement->previousElement = newElement;
    }

    return;
}

/**
 * Prostřednictvím parametru dataPtr vrátí hodnotu aktivního prvku seznamu list.
 * Pokud seznam list není aktivní, volá funkci DLL_Error ().
 *
 * @param list Ukazatel na inicializovanou strukturu dvousměrně vázaného seznamu
 * @param dataPtr Ukazatel na cílovou proměnnou
 */
void DLL_GetValue( DLList *list, int *dataPtr ) {

    if(list->firstElement == NULL){ // existuje zoznam?
        DLL_Error();
        return;
    }

    if(!DLL_IsActive(list)){ // existuje aktivny prvok?
        DLL_Error();
        return;
    }
    *dataPtr = list->activeElement->data;
    return;
}

/**
 * Přepíše obsah aktivního prvku seznamu list.
 * Pokud seznam list není aktivní, nedělá nic.
 *
 * @param list Ukazatel na inicializovanou strukturu dvousměrně vázaného seznamu
 * @param data Nová hodnota právě aktivního prvku
 */
void DLL_SetValue( DLList *list, int data ) {

    if(!DLL_IsActive(list)) // existuje aktivny prvok?
        return;

    list->activeElement->data = data;
    return;
}

/**
 * Posune aktivitu na následující prvek seznamu list.
 * Není-li seznam aktivní, nedělá nic.
 * Všimněte si, že při aktivitě na posledním prvku se seznam stane neaktivním.
 *
 * @param list Ukazatel na inicializovanou strukturu dvousměrně vázaného seznamu
 */
void DLL_Next( DLList *list ) {

    if(!DLL_IsActive(list)) // existuje aktivny prvok?
        return;

    // ak je aktivny prvok posledny, nastavim ho na null
    if(list->lastElement == list->activeElement){
        list->activeElement = NULL;
        return;
    }

    list->activeElement = list->activeElement->nextElement; // posuniem aktivny prvok na dalsi

    return;
}


/**
 * Posune aktivitu na předchozí prvek seznamu list.
 * Není-li seznam aktivní, nedělá nic.
 * Všimněte si, že při aktivitě na prvním prvku se seznam stane neaktivním.
 *
 * @param list Ukazatel na inicializovanou strukturu dvousměrně vázaného seznamu
 */
void DLL_Previous( DLList *list ) {

    if(!DLL_IsActive(list)) // existuje aktivny prvok?
        return;

    // overim, ci aktivny element nie je prave prvy elemenet
    if(list->activeElement == list->firstElement){
        list->activeElement = NULL;
        return;
    }

    list->activeElement = list->activeElement->previousElement; 

    return;    
}

/**
 * Je-li seznam list aktivní, vrací nenulovou hodnotu, jinak vrací 0.
 * Funkci je vhodné implementovat jedním příkazem return.
 *
 * @param list Ukazatel na inicializovanou strukturu dvousměrně vázaného seznamu
 *
 * @returns Nenulovou hodnotu v případě aktivity prvku seznamu, jinak nulu
 */
int DLL_IsActive( DLList *list ) {
    
    // ak ma list aktivny prvok, vratim smernik
    // inak null
    return list->activeElement != NULL;
}

/* Konec c206.c */
